from dolfin import *
from ShapeOpt import *

set_log_active(False)
set_log_level(PROGRESS)

Shape1 = ShapeOpt()

mesh = UnitSquareMesh(10, 10)

FS = FiniteElement("CG", mesh.ufl_cell(), 1)
FV = VectorElement("CG", mesh.ufl_cell(), 1)
FR = FiniteElement("R", mesh.ufl_cell(), 0)
TotalSpace = FunctionSpace(mesh, FV*FR)

Q0 = Function(TotalSpace)
(V0, lmb0) = Q0.split()
lmb0.rename("lmb0", "")
(V, lmb) = TestFunctions(TotalSpace)
(W, dlmb) = TrialFunctions(TotalSpace)

from ufl import checks
print checks.is_globally_constant(lmb0)
print checks.is_globally_constant(lmb)
print checks.is_globally_constant(dlmb)
#exit()

#Declare Lagrangian
#OneV = Expression("1.0", domain=mesh)
OneV = Constant(1.0)
OneV.rename("One", "")
OneR = Expression("1.0", element=FR, domain=mesh)
Vol0 = assemble(OneR*dx)
print "Initial Volume:", Vol0

L = OneV*ds(domain=mesh) + lmb0*(OneV*dx(domain=mesh))

N = Shape1.VolumeNormal(mesh)
N.rename("n", "")

SurfaceGradient = True

MeshSmooth = Constant(0.1)

CurvSmooth = 1e-2
kappa = Shape1.ComputeDivNVolume(mesh, CurvSmooth)

print "SHAPE DERIVATIVE:"
sd = ShapeDerivative(L, mesh, V, n=N, State=lmb0, StateDirection=lmb, kappa=kappa, Is_Normal=[V], Constant_In_Normal=[], GenerateSurface=SurfaceGradient)
#sd = ShapeDerivative(L, V, N, kappa=kappa, EnforceSymmetry = V)
print "SHAPE DERIVATIVE CREATED!"
print "SHAPE DERIVATIVE:"
print sd

sh = ShapeDerivative(sd, mesh, W, n=N, State=lmb0, StateDirection=dlmb, kappa=kappa, Is_Normal=[V,W], Constant_In_Normal=[], SymmetryDirection=V, IncludeNormalVariation=True, IncludeCurvatureVariation=True, GenerateSurface=SurfaceGradient)
print ""
print "SHAPE HESSIAN DONE:"
print sh
#exit()
#a2 = assemble(sh).array()
#print "a2", a2

#st1 = derivative(L, lmb0, lmb)
#st2 = derivative(st1, lmb0, dlmb)
st3 = derivative(sd, lmb0, dlmb)

#Define mesh deformation
#u = TrialFunction(FV)
#MeshDefo = inner(grad(u), grad(V))*dx + inner(u,V)*ds

g = Function(TotalSpace)

(V1, U1) = TrialFunctions(TotalSpace)
(V2, U2) = TestFunctions(TotalSpace)

StepLength = Constant(5e-3)
#StepLength = Constant(1e-5)
#KKT = inner(V1,V2)*dx + st3 + adjoint(st3)
KKT = inner(V1,V2)*dx + Constant(0.1)*inner(grad(V1),grad(V2))*dx + st3 + adjoint(st3)
#KKT = sh
#KKT = inner(V1,V2)*dx + Constant(0.1)*inner(grad(V1),grad(V2))*dx + 0.5*(sh + adjoint(sh))

Optimality = 1e9

FOut = File("output/SAD_Dido/Move.pvd")
FGrad = File("output/SAD_Dido/Gradient.pvd")

#convergence history
if MPI.rank(mpi_comm_world()) == 0:
    fHistory = open("output/SAD_Dido/"+"History.dat", "w")
    fHistory.write("Iter\tObjective\tOptimality\n")

iter = 0
while Optimality > 2e-3 and iter < 300:
    #print OptIter, "SOLVING KKT"
    CurVol = assemble(OneR*dx)
    #print "Current Volume", CurVol
    #update normal and curvature:
    N.assign(Shape1.VolumeNormal(mesh))
    kappa.assign(Shape1.ComputeDivNVolume(mesh, CurvSmooth))
  
    #When to switch to Newton?
    if True:#Optimality < 1e-0:
    #if iter > 250:
        #print "Newton-Step", iter
        StepLength= Constant(1.0)
        KKT = inner(V1,V2)*dx + MeshSmooth*inner(grad(V1),grad(V2))*dx
        KKT += inner(V1,V2)*ds + MeshSmooth*inner(grad(V1),grad(V2))*ds
        KKT += sh
        #(A,b) = assemble_system(KKT, -sd)
        #print A.array()
    #exit()
    
    (A,b) = assemble_system(KKT, -sd)

    #kill interior residual in gradient
    SurfGrad = Function(TotalSpace)
    SurfGrad.rename("Gradient", "")
    Tmp = Function(TotalSpace)
    Tmp.vector()[:] = b[:]
    KillInterior = DirichletBC(TotalSpace, Tmp, "on_boundary")
    KillInterior.apply(SurfGrad.vector())
    b = SurfGrad.vector()
    FGrad << SurfGrad
    #plot(SurfGrad.sub(0), interactive=True)

    #print the Hessian?
    #print A.array()
    b[len(b)-1] = Vol0-CurVol
    solve(A, g.vector(), b)
    #solve(KKT == -sd, g)
    (dDefo, dLambda) = g.split()

    """
    MDSpace = VectorFunctionSpace(mesh, "CG", 1)
    MDTest = TestFunction(MDSpace)
    MDTrial = TrialFunction(MDSpace)
    md = (inner(MDTest, MDTrial) + Constant(0.1)*inner(grad(MDTest), grad(MDTrial)))*dx
    md += (inner(MDTest, MDTrial))*ds #+ Constant(0.1)*inner(grad(MDTest), grad(MDTrial))*ds
    MDBC = DirichletBC(MDSpace, inner(dDefo,N)*N, "on_boundary")
    Defo2 = Function(MDSpace)
    Defo2.rename("MeshDefo", "")
    #solve(md == Constant(0.0)*MDTest[0]*dx(domain=mesh), Defo2, MDBC)
    solve(md == inner(inner(dDefo,N)*N,MDTest)*ds(domain=mesh), Defo2)
    dDefo = Defo2
    """

    FOut << dDefo
    plot(dDefo, interactive=False)
    #update state and adjoint:
    #lmb0.assign(project(lmb0+dLambda, FR))
    lmb0.assign(project(lmb0+dLambda, FunctionSpace(mesh, FR)))
    #lmb0 = lmb0+dLambda

    Optimality = norm(b)
    obj = assemble(OneV*ds(domain=mesh))
    print "Iter", iter, "Optimality:", Optimality, "Length:", norm(g), "Volume-offet:", Vol0-CurVol
    if MPI.rank(mpi_comm_world()) == 0:
        fHistory.write("%4d\t%e\t%e\n"%(iter, obj, Optimality))
    
    #move = project(StepLength*dDefo, FV)
    MyMove = project(StepLength*dDefo, FunctionSpace(mesh, FV))
    ALE.move(mesh, MyMove)
    iter = iter + 1
