from dolfin import *

from ShapeOpt import *
from SAD_IO import *

from subprocess import call

#For a detailed description please see the README or SAD_Navier_Reduced.py
#Solves the same problem as SAD_Navier_Reduced.py with using Newton's Method
#utilizing the weak form of the shape derivative generated automatically.

#set_log_active(False)
#set_log_level(PROGRESS)

Shape1 = ShapeOpt()
OutputFolder = "./output/SAD_Navier_Weak/"
RestartFolder = OutputFolder+"Restart/"
MeshFolder = "./mesh/Channel2D/"


#MeshName = "Channel1_Symm"
#RemeshLogic = "remesh"
MeshName = "Channel2_Symm"
RemeshLogic = "remesh_local"

mesh = Mesh(MeshFolder + "/" + MeshName+".xml")
boundary_parts = MeshFunction("size_t", mesh, MeshFolder + "/" + MeshName+"_facet_region.xml")
VolumeParts = MeshFunction("size_t", mesh, MeshFolder + "/" + MeshName + "_physical_region.xml")

#(mesh, boundary_parts, VolumeParts) = Shape1.LoadMeshH5("./output/SAD_Navier/Restart/Mesh_%05d.h5"%163, True, True)

VolumeConstraint = True
BarycenterConstraint = True
#BaryTol = 1e-5
SurfaceGradient = False

Mu = Constant(1.0/100.0)
Mu.rename("Mu", "")
Rho = Constant(1.0)
Rho.rename("Rho", "")

#OneR = project(Constant(1.0), FunctionSpace(Shape1.mesh, R0))
OneR = Constant(1.0)
OneR.rename("One", "")

#for mesh 2 with boundary Laplace mesh defo
StepLength1 = Constant(2.5e-1)
StepLength2 = Constant(1e-0)
StepSwap = 3
RemeshEvery = 1
RemeshStop = 40
OptEvery = 1
#MaxOptIter = 1000
CurvSmooth = 1e-1
ResStop = 1e-4
MaxIter = 1000

Shape1.mesh = mesh
Shape1.boundary_parts = boundary_parts
#plot(Shape1.boundary_parts, interactive=True)
MaxDim = Shape1.mesh.topology().dim()
if MaxDim == 2:
    MyZeroV = Constant((0.0,0.0))
    MyIn = Constant((1.0,0.0))
else:
    MyZeroV = Constant((0.0,0.0,0.0))
    MyIn = Constant((1.0,0.0,0.0))
#MyInflow = Expression("1.5*(x[1]+y0)*(x[1]-y0)/(-y0*y0)", y0=1.5)*MyIn
MyInflow = MyIn

Inlet = [11,12,31,32]#[11,12]
Noslip = []#[31,32]
Outlet = [21,22]
Design = [41,42]
Symm = [71,72]
AllMarkers = Inlet + Noslip + Outlet + Design + Symm

def MyResidual(u0, p0, V0, lu0, lp0, TotalSpace, Shape1):
    if V0 != None:
        VOff = project(V0, TotalSpace.sub(2).collapse())
    WallP = []
    WallV = []
    WallD = []
    for i in Noslip:
        if u0 != None:
            WallP.append(DirichletBC(TotalSpace.sub(0), project(u0, TotalSpace.sub(0).collapse()), Shape1.boundary_parts, i))
        if V0 != None:
            WallV.append(DirichletBC(TotalSpace.sub(2), VOff, Shape1.boundary_parts, i))
        if lu0 != None:
            WallD.append(DirichletBC(TotalSpace.sub(3), project(lu0, TotalSpace.sub(3).collapse()), Shape1.boundary_parts, i))
    ObstacleP = []
    ObstacleD = []
    for i in Design:
        if u0 != None:
            ObstacleP.append(DirichletBC(TotalSpace.sub(0), project(u0, TotalSpace.sub(0).collapse()), Shape1.boundary_parts, i))
        if lu0 != None:
            ObstacleD.append(DirichletBC(TotalSpace.sub(3), project(lu0, TotalSpace.sub(3).collapse()), Shape1.boundary_parts, i))
    InflowP = []
    InflowV = []
    InflowD = []
    for i in Inlet:
        if u0 != None:
            InflowP.append(DirichletBC(TotalSpace.sub(0), project(u0-MyInflow, TotalSpace.sub(0).collapse()), Shape1.boundary_parts, i))
        if V0 != None:
            InflowV.append(DirichletBC(TotalSpace.sub(2), VOff, Shape1.boundary_parts, i))
        if lu0 != None:
            InflowD.append(DirichletBC(TotalSpace.sub(3), project(lu0, TotalSpace.sub(3).collapse()), Shape1.boundary_parts, i))
    OutflowV = []
    for i in Outlet:
        if V0 != None:
            OutflowV.append(DirichletBC(TotalSpace.sub(2), VOff, Shape1.boundary_parts, i))
    return ObstacleP+WallP+InflowP+WallV+InflowV+OutflowV+ObstacleD+WallD+InflowD

def MyBCs(u0, p0, V0, lu0, lp0, TotalSpace, Shape1):
    WallP = []
    WallV = []
    WallD = []
    for i in Noslip:
        if u0 != None:
            WallP.append(DirichletBC(TotalSpace.sub(0), MyZeroV, Shape1.boundary_parts, i))
        if V0 != None:
            WallV.append(DirichletBC(TotalSpace.sub(2), MyZeroV, Shape1.boundary_parts, i))
        if lu0 != None:
            WallD.append(DirichletBC(TotalSpace.sub(3), MyZeroV, Shape1.boundary_parts, i))
    ObstacleP = []
    ObstacleD = []
    for i in Design:
        if u0 != None:
            ObstacleP.append(DirichletBC(TotalSpace.sub(0), MyZeroV, Shape1.boundary_parts, i))
        if lu0 != None:
            ObstacleD.append(DirichletBC(TotalSpace.sub(3), MyZeroV, Shape1.boundary_parts, i))
    InflowP = []
    InflowV = []
    InflowD = []
    for i in Inlet:
        if u0 != None:
            InflowP.append(DirichletBC(TotalSpace.sub(0), MyInflow, Shape1.boundary_parts, i))
        if V0 != None:
            InflowV.append(DirichletBC(TotalSpace.sub(2), MyZeroV, Shape1.boundary_parts, i))
        if lu0 != None:
            InflowD.append(DirichletBC(TotalSpace.sub(3), MyZeroV, Shape1.boundary_parts, i))
    OutflowV = []
    for i in Outlet:
        if V0 != None:
            OutflowV.append(DirichletBC(TotalSpace.sub(2), MyZeroV, Shape1.boundary_parts, i))
    return ObstacleP+WallP+InflowP+WallV+InflowV+OutflowV+ObstacleD+WallD+InflowD

#Declare Function Spaces
def InitTotalSpace(mesh):
    global V2
    global S1
    global V1
    global R0
    global ME
    global TotalSpace
    global OneR
    #global BarycenterConstraint
    #Space for velocity and adjoints
    V2 = VectorElement("CG", mesh.ufl_cell(), 2)
    #Space for pressure
    S1 = FiniteElement("CG", mesh.ufl_cell(), 1)
    #Space for deformation field
    V1 = VectorElement("CG", mesh.ufl_cell(), 1)
    #Space for scalar constraints
    R0 = FiniteElement("R", mesh.ufl_cell(), 0)
    Elements = [V2, S1, V1, V2, S1]
    if VolumeConstraint:
        Elements.append(R0)
    
    if BarycenterConstraint:
        MaxDim = mesh.topology().dim()
        for k in range(MaxDim):
            Elements.append(R0)
    
    ME = MixedElement(Elements)
    TotalSpace = FunctionSpace(mesh, ME)
    #OneR = project(Constant(1.0), FunctionSpace(Shape1.mesh, R0))
    #OneR.rename("One", "")

def InitTestFunctions():
    global du
    global dp
    global V
    global dLu
    global dLp
    global dLVol
    global VolumeConstraint
    global BarycenterConstraint, dLBary
    Tests = TestFunctions(TotalSpace)
    du = Tests[0]
    dp = Tests[1]
    V = Tests[2]
    dLu = Tests[3]
    dLp = Tests[4]
    
    if VolumeConstraint:
        dLVol = Tests[5]
    if BarycenterConstraint:
        MaxDim = mesh.topology().dim()
        dLBary = MaxDim*[0.0]
        for k in range(MaxDim):
            dLBary[k] = Tests[5+k+1]

def InitTrialFunctions():
    global DeltaU
    global DeltaP
    global W
    global DeltaLu
    global DeltaLp
    global DeltaLVol
    global VolumeConstraint
    Trials = TrialFunctions(TotalSpace)
    DeltaU = Trials[0]
    DeltaP = Trials[1]
    W = Trials[2]
    DeltaLu = Trials[3]
    DeltaLp = Trials[4]
    if VolumeConstraint:
        DeltaLVol = Trials[5]

def InitFunctions():
    global q
    global u0
    global p0
    global V0
    global lu0
    global lp0
    global lVol
    global VolumeConstraint
    global BarycenterConstraint, lBary
    q = Function(TotalSpace)
    q.rename("AllVar", "")
    
    if VolumeConstraint == False:
        (u0, p0, V0, lu0, lp0) = split(q)
    else:
        if BarycenterConstraint:
            MaxDim = mesh.topology().dim()
            if MaxDim == 2:
                (u0, p0, V0, lu0, lp0, lVol, lBary0, lBary1) = split(q)
                lBary = [lBary0, lBary1]
            if MaxDim == 3:
                (u0, p0, V0, lu0, lp0, lVol, lBary0, lBary1, lBary2) = split(q)
                lBary = [lBary0, lBary1, lBary2]
        else:
            (u0, p0, V0, lu0, lp0, lVol) = split(q)
    #print "Mesh #Nodes:", mesh.size_global(0)
    #print "Mesh #Triangles:", mesh.size_global(1)
    #print len(q.vector())

def InterpolateFunctions():
    global q
    global u0
    global p0
    global V0
    global lu0
    global lp0
    global lVol
    global BarycenterConstraint, lBary
    q_new = Function(TotalSpace)
    #q = q_new
    lp = LagrangeInterpolator()
    lp.interpolate(q_new, q)
    q = q_new
    if BarycenterConstraint:
        MaxDim = mesh.topology().dim()
        if MaxDim == 2:
            (u0, p0, V0, lu0, lp0, lVol, lBary0, lBary1) = split(q)
            lBary = [lBary0, lBary1]
        if MaxDim == 3:
            (u0, p0, V0, lu0, lp0, lVol, lBary0, lBary1, lBary2) = split(q)
            lBary = [lBary0, lBary1, lBary2]
    else:
        (u0, p0, V0, lu0, lp0, lVol) = split(q)
    #print "INTERPOLATED u"
    #plot(u0, interactive=True)

def MyMeshDefo(V,W):
    MeshSmooth = Constant(0.1)
    MeshSmooth.rename("MeshSmooth", "")
    MyDefo = inner(V,W)*dx + MeshSmooth*inner(grad(V),grad(W))*dx + inner(V,W)*ds
    MyDefo += MeshSmooth*inner(grad(V), grad(W))*ds
    return MyDefo

def BoundaryDefo(dDefo):
    MDSpace = VectorFunctionSpace(mesh, "CG", 1)
    MDTest = TestFunction(MDSpace)
    MDTrial = TrialFunction(MDSpace)
    md = (inner(MDTest, MDTrial) + Constant(0.1)*inner(grad(MDTest), grad(MDTrial)))*dx
    md += (inner(MDTest, MDTrial))*ds# + Constant(0.1)*inner(grad(MDTest), grad(MDTrial)))*ds
    #MDBC = DirichletBC(MDSpace, inner(dDefo,N)*N, "on_boundary")
    MDBC = []
    for k in Design:
        MDBC.append(DirichletBC(MDSpace, dDefo, boundary_parts, k))
    for k in Inlet + Noslip + Outlet:
        MDBC.append(DirichletBC(MDSpace, MyZeroV, boundary_parts, k))
    Defo2 = Function(MDSpace)
    Defo2.rename("MeshDefo", "")
    #Posed as essential boundary condition
    print "Solving boundary mesh deformation"
    solve(md == Constant(0.0)*MDTest[0]*dx(domain=mesh), Defo2, MDBC)
    #solve(md == inner(inner(dDefo,N)*N,MDTest)*ds(domain=mesh), Defo2)
    return Defo2

def NavierStokesResidual(MyRho, u0, p0, lu0, lp0):
    global Mu
    #Mu_eff = Mu#*(pow((inner(grad(u0), grad(u0))), 2.0)+1e0)
    NSR = (Mu*inner(grad(u0), grad(lu0)) + lp0*div(u0) - p0*div(lu0))*dx
    NSR += MyRho*inner(lu0, dot(grad(u0),u0))*dx
    return NSR

#Define Lagrangian
def MakeLagrangian(MyRho, u0, p0, lu0, lp0, lVol=None):
    global Mu
    global OneR
    global L_Objective
    
    Mu_eff = Mu#*(pow((inner(grad(u0), grad(u0))), 2.0)+1e0)
    #print "Defining Lagrangian:"
    L_Objective = Mu_eff*inner(grad(u0), grad(u0))*dx
    #L_Objective = OneR*dx(domain=mesh)
    #L_Objective = inner(grad(p0), grad(p0))*dx
    L_Functional = NavierStokesResidual(MyRho, u0, p0, lu0, lp0)
    
    #Scalar Constraints
    if lVol != None:
        #L_Scalar = (lVol*OneR)*dx
        MyOne = Constant(1.0)
        MyOne.rename("One", "")
        L_Scalar = (lVol*MyOne)*dx
        return (L_Objective, L_Functional, L_Scalar)
    #Barycenter-constraint cannot be postulated here, because the non-integral root node
    #would fail UFL and the S-AD differentiation
    #Add the derivative component of that directly to the KKT-RHS
    return (L_Objective, L_Functional)

def SolveState(Shape1):
    global V2
    global S1
    global Rho
    mesh = Shape1.mesh
    Elements = [V2, S1]
    ME = MixedElement(Elements)
    StateSpace = FunctionSpace(mesh, ME)
    q1 = Function(StateSpace)
    q1.rename("StateVarA", "")
    (u1, p1) = split(q1)
    (lu1, lp1) = TestFunctions(StateSpace)
    NSR = NavierStokesResidual(Rho,u1,p1,lu1,lp1)
    #J = ProcessState(NSR, q1, TrialFunction(StateSpace))
    J = derivative(NSR, q1, TrialFunction(StateSpace))
    print "Solving state equation"
    solve(NSR == 0, q1, bcs=MyBCs(u1, p1, None, None, None, StateSpace, Shape1), J=J)
    (u2, p2) = split(q1)
    #plot(u2, interactive = True)
    #plot(p2, interactive = True)
    return (u2, p2)

def SolveAdjoint(Shape1, u0, p0):
    global V2
    global S1
    global Rho
    mesh = Shape1.mesh
    Elements = [V2, S1]
    ME = MixedElement(Elements)
    StateSpace = FunctionSpace(mesh, ME)
    q1 = Function(StateSpace)
    q1.rename("StateVarB", "")
    u1 = project(u0, StateSpace.sub(0).collapse())
    p1 = project(p0, StateSpace.sub(1).collapse())
    assign(q1, [u1, p1])
    (u1, p1) = split(q1)
    
    q2 = Function(StateSpace)
    q2.rename("AdjVar", "")
    (lu1, lp1) = split(q2)
    if VolumeConstraint:
        (L_Objective, L_Functional, L_Scalar) = MakeLagrangian(Rho,u1, p1, lu1, lp1, lVol)
    else:
        (L_Objective, L_Functional) = MakeLagrangian(Rho,u1, p1, lu1, lp1)
    L = L_Objective + L_Functional
    if VolumeConstraint:
        L = L + L_Scalar
    #use dolfin standard differentiation
    #dL = derivative(L, q1, TestFunction(StateSpace))
    #use Shape1 differentiation
    #dL = ProcessState(L, q1, TestFunction(StateSpace))
    dL = derivative(L, q1, TestFunction(StateSpace))
    #bc = MyBoundaryConditions(u0, p0, None, None, None, StateSpace, Shape1)
    MyZero = Constant(MaxDim*[0.0])
    MyBC = []
    for k in Inlet+Noslip+Design:
        MyBC.append(DirichletBC(StateSpace.sub(0), MyZero, Shape1.boundary_parts, k))
    print "Solving adjoint equation"
    solve(dL == 0, q2, bcs=MyBC)
    return (lu1, lp1)

"""
def SolveOptimality(Shape1, u1, p1, lu1, lp1):
    global V1, R, Rho
    N = Shape1.VolumeNormal(Shape1.mesh)
    N.rename("n", "")
    kappa = Shape1.ComputeDivNVolume(Shape1.mesh, CurvSmooth)
    kappa.rename("Curvature", "")
    
    if VolumeConstraint:
        Elements = [V1, R0]
        if BarycenterConstraint:
            for k in range(MaxDim):
                Elements.append(R0)
        ME = MixedElement(Elements)
        VSpace = FunctionSpace(Shape1.mesh, ME)
        q1 = Function(VSpace)
        if BarycenterConstraint:
            if MaxDim == 2:
                (V0, lVol, lBary0, lBary1) = split(q1)
                (V, dlVol, dlBary0, dlBary1) = TestFunctions(VSpace)
                (W, DlVol, DlBary0, DlBary1) = TrialFunctions(VSpace)
                dlBary = (dlBary0, dlBary1)
                DlBary = (DlBary0, DlBary1)
            if MaxDim == 3:
                (V0, lVol, lBary0, lBary1, lBary2) = split(q1)
                (V, dlVol, dlBary0, dlBary1, dBary2) = TestFunctions(VSpace)
                (W, DlVol, DlBary0, DlBary1, DBary2) = TrialFunctions(VSpace)
                dlBary = (dlBary0, dlBary1, dlBary2)
                DlBary = (dlBary0, dlBary1, dlBary2)
        else:
            (V0, lVol) = split(q1)
            (V, dlVol) = TestFunctions(VSpace)
            (W, DlVol) = TrialFunctions(VSpace)
    else:
        VSpace = VectorFunctionSpace(Shape1.mesh, "CG", 1)
        V0 = Function(VSpace)
        V = TestFunction(VSpace)
        W = TrialFunction(VSpace)
    if VolumeConstraint:
        (L_Objective, L_Functional, L_Scalar) = MakeLagrangian(Rho, u1, p1, lu1, lp1, lVol)
        L = L_Objective + L_Functional + L_Scalar
    else:
        (L_Objective, L_Functional) = MakeLagrangian(Rho, u1, p1, lu1, lp1)
        L = L_Objective + L_Functional

    dL_Objective = ShapeDerivative(L_Objective, mesh, V, n=N, kappa=kappa, boundary_parts=Shape1.boundary_parts, Is_Normal=[V], Constant_In_Normal=[], SymmetryDirection=V, GenerateSurface=SurfaceGradient)
    dL_Functional = ShapeDerivative(L_Functional, mesh, V, n=N, kappa=kappa, boundary_parts=Shape1.boundary_parts, Is_Normal=[V], Constant_In_Normal=[], SymmetryDirection=V, GenerateSurface=SurfaceGradient)
    dL = dL_Objective + dL_Functional

    if SurfaceGradient == True:
        KKT = MyMeshDefo(V,W)
    else:
        KKT = Constant(1.0)*inner(V,W)*dx + Constant(1.0)*inner(V,W)*ds

    if VolumeConstraint:
        dL_Scalar = ShapeDerivative(L_Scalar, mesh, V, n=N, kappa=kappa, boundary_parts=Shape1.boundary_parts, Is_Normal=[V], Constant_In_Normal=[], SymmetryDirection=V, GenerateSurface=SurfaceGradient)
        KKT_Scalar = ShapeDerivative(dL_Scalar, mesh, W, n=N, kappa=kappa, boundary_parts=Shape1.boundary_parts, Is_Normal=[V,W], Constant_In_Normal=[], SymmetryDirection=V, IncludeNormalVariation=True, IncludeCurvatureVariation=True, GenerateSurface=SurfaceGradient)
        KKT += KKT_Scalar
        if BarycenterConstraint:
            print ""
            print "TEST: SOLVE OPTIMALITY BARY!"
            print ""
            exit()
            SDB = Shape1.SD_Bary(Shape1.mesh, V, N)
            for k in range(MaxDim):
                #TEST Bary SIGN!
                dL_Scalar += lBary[k]*SDB[k]*dx
                KKT += DlBary[k]*SDB[k]*dx + adjoint(DlBary[k]*SDB[k]*dx)
        dL += dL_Scalar
    bc = []
    if VolumeConstraint:
        for i in Inlet+Noslip+Outlet:
            bc.append(DirichletBC(VSpace.sub(0), MyZeroV, Shape1.boundary_parts, i))
    else:
        for i in Inlet+Noslip+Outlet:
            bc.append(DirichletBC(VSpace, MyZeroV, Shape1.boundary_parts, i))
    (A,b) = assemble_system(KKT, dL, bc)
    Grad = Function(VSpace)
    Grad.rename("ReducedGradient", "")
    solve(A, Grad.vector(), -b)
    File(OutputFolder+"Reduced_grad.pvd") << Grad.sub(0)
    plot(Grad.sub(0), interactive=True)
    exit()
    return Grad
"""

def InitProblem():
    global N
    global kappa
    global sd
    global KKT
    global OneR
    #global Mu
    global Rho
    
    N = Shape1.VolumeNormal(Shape1.mesh)
    N.rename("n", "")
    kappa = Shape1.ComputeDivNVolume(Shape1.mesh, CurvSmooth)
    kappa.rename("Curvature", "")

    if VolumeConstraint:
        (L_Objective, L_Functional, L_Scalar) = MakeLagrangian(Rho, u0, p0, lu0, lp0, lVol)
    else:
        (L_Objective, L_Functional) = MakeLagrangian(Rho, u0, p0, lu0, lp0)
    #with PDE Trace:
    #L = L_Shape + L_Functional
    #sd = ShapeDerivative(L, V, N, State=q,x StateDirection=TestFunction(TotalSpace), kappa=kappa, boundary_parts=Shape1.boundary_parts, Is_Normal=[V], Constant_In_Normal=[], GenerateSurface=GenerateSurface)
    #without PDE Trace: (missing + L_Functional)
    dObjective = ShapeDerivative(L_Objective + L_Functional, mesh, V, n=N, State=q, StateDirection=TestFunction(TotalSpace), kappa=kappa, boundary_parts=Shape1.boundary_parts, Is_Normal=[V], Constant_In_Normal=[], GenerateSurface=SurfaceGradient)
    #dFunctional = derivative(L_Objective + L_Functional, q, TestFunction(TotalSpace))
    sd = dObjective
    if VolumeConstraint:
        dScalar = ShapeDerivative(L_Scalar, mesh, V, n=N, State=q, StateDirection=TestFunction(TotalSpace), kappa=kappa, boundary_parts=Shape1.boundary_parts, Is_Normal=[V], Constant_In_Normal=[], GenerateSurface=SurfaceGradient)
        sd += dScalar
    
    #with PDE trace
    #KKT = ShapeDerivative(sd, W, N, State=q, StateDirection=TrialFunction(TotalSpace), kappa=kappa, boundary_parts=Shape1.boundary_parts, Is_Normal=[V,W], Constant_In_Normal=[], CurvatureSymmetry=V, IncludeNormalVariation=True, IncludeCurvatureVariation=True, GenerateSurface=GenerateSurface)
    #without PDE trace
    KKT_Objective = ShapeDerivative(sd, mesh, W, n=N, State=q, StateDirection=TrialFunction(TotalSpace), kappa=kappa, boundary_parts=Shape1.boundary_parts, Is_Normal=[V,W], Constant_In_Normal=[], SymmetryDirection=V, IncludeNormalVariation=True, IncludeCurvatureVariation=True, GenerateSurface=SurfaceGradient)
    #KKT_Functional = derivative(dFunctional + dObjective, q, TrialFunction(TotalSpace))
    #KKT_Objective CAUSES DIVERGENCE IN ADJOINT WHEN USED WITH NAVIER-STOKES!

    #print ""
    #print ""
    #print "KKT_Objective:"
    #print KKT_Objective
    #exit()
    KKT = KKT_Objective

    #KKT += derivative(dShape, q, TrialFunction(TotalSpace)) + adjoint(derivative(dShape, q, TrialFunction(TotalSpace)))
    
    #(du, dp, dv, dlu, dlp) = split(TestFunction(TotalSpace))
    #(Du, Dp, Dv, Dlu, Dlp) = split(TrialFunction(TotalSpace))
    #KKT += (inner(du,Du) + inner(dp, Dp))*dx# + inner(dvol, Dvol))*dx
    #KKT += MyMeshDefo(V,W) + inner(TrialFunction(TotalSpace), TestFunction(TotalSpace))*dx
    #KKT += inner(V,W)*dx
    if True:#SurfaceGradient == True:
        KKT += MyMeshDefo(V,W)
    #else:
    #    KKT += Constant(0.1)*inner(grad(V), grad(W))*ds(domain=mesh)
    #    KKT += inner(V,W)*ds(domain=mesh)
    ##sd += MyMeshDefo(V0,V)

    #Add stuff not meant to go through S-AD, e.g. the barycenter
    if BarycenterConstraint:
        #SD_BaryV = Shape1.SD_Bary(Shape1.mesh, V, N)
        #for k in range(MaxDim):
        #    #Test Bary Sign!
        #    sd += lBary[k]*SD_BaryV[k]*ds
        #SD_BaryW = Shape1.SD_Bary(Shape1.mesh, W, N)
        #for k in range(MaxDim):
        #    KKT += dLBary[k]*SD_BaryW[k]*ds + adjoint(dLBary[k]*SD_BaryW[k]*ds)
        #CurVol = assemble(OneR*dx(domain=mesh))
        OneByVol = Constant(1.0/Vol0)
        OneByVol.rename("1BV", "")
        #print ""
        #print "TEST: THE INIT VOLUME IS:", Vol0
        L_Bary = OneByVol*lBary[0]*MyId[0]*dx(domain=mesh)
        for k in range(1,MaxDim):
            L_Bary = L_Bary + OneByVol*lBary[k]*MyId[k]*dx(domain=mesh)
            print "BaryCenterAdjoint:", project(lBary[k], FunctionSpace(mesh, "R", 0)).vector()[0]
        SD_BaryV = ShapeDerivative(L_Bary, mesh, V, n=N, State=q, StateDirection=TestFunction(TotalSpace), kappa=kappa, boundary_parts=Shape1.boundary_parts, Is_Normal=[V,W], Constant_In_Normal=[], SymmetryDirection=V, IncludeNormalVariation=True, IncludeCurvatureVariation=True, GenerateSurface=True)
        #print "SD_BaryV:", SD_BaryV
        sd += SD_BaryV
        KKT_Bary = ShapeDerivative(SD_BaryV, mesh, W, n=N, State=q, StateDirection=TrialFunction(TotalSpace), kappa=kappa, boundary_parts=Shape1.boundary_parts, Is_Normal=[V,W], Constant_In_Normal=[], SymmetryDirection=V, IncludeNormalVariation=True, IncludeCurvatureVariation=True, GenerateSurface=True)
        #print "KKT_Bary:", KKT_Bary
        #print ""
        KKT += KKT_Bary
    #remove tangential motion:
    #sd -= inner(V, V0 - dot(V0,N)*N)*ds

InitTotalSpace(Shape1.mesh)
InitTestFunctions()
InitTrialFunctions()
InitFunctions()

#OneV = project(Constant(1.0), FS)
#OneV.rename("One", "")
if MaxDim == 2:
    MyZero = Constant((0.0,0.0))
    MyIn = Constant((1.0,0.0))
else:
    MyZero = Constant((0.0,0.0,0.0))
    MyIn = Constant((1.0,0.0,0.0))

Vol0 = assemble(OneR*dx(domain=mesh))
print "Initial Volume:", Vol0
if BarycenterConstraint:
    MyId = []
    for k in range(MaxDim):
        TmpSpace = FunctionSpace(mesh, "CG", 1)
        MyIdExpr1 = Expression("x[%d]"%k, element=TmpSpace.ufl_element())
        MyIdExpr2 = project(MyIdExpr1, TmpSpace)
        MyId.append(MyIdExpr2)
    InitBary = Shape1.ComputeBarycenter(Shape1.mesh)
    print "Initial Barycenter:", InitBary
#exit()

InitProblem()

FU = File(OutputFolder+"/Velocity.pvd", "compressed")
FP = File(OutputFolder+"/Pressure.pvd", "compressed")
FW = File(OutputFolder+"/Move.pvd", "compressed")
#FWB = File(OutputFolder+"/MoveBary.pvd", "compressed")
FLu = File(OutputFolder+"/Adj_Velo.pvd", "compressed")
FLp = File(OutputFolder+"/Adj_Pres.pvd", "compressed")
FKappa = File(OutputFolder+"/Kappa.pvd", "compressed")
FN = File(OutputFolder+"/Normal.pvd", "compressed")
FGrad = File(OutputFolder+"/Gradient.pvd", "compressed")

#FRU = File("./output/SAD_Navier/ResVelo.pvd", "compressed")

#convergence history
if MPI.rank(mpi_comm_world()) == 0:
    fHistory = open(OutputFolder+"/History.txt", "w")
    fHistory.write("Iter\tObjective\tResMove_Vol\tRHS_Volume\tResidual_U\tResidual_P\tResidual_V\tResidual_LU\tResidual_LP")
    if VolumeConstraint:
        fHistory.write("\tResidual_Vol")
    if BarycenterConstraint:
        for k in range(MaxDim):
            fHistory.write("\tResidual_Bary%d"%k)
    fHistory.write("\n")

print "Computing initial state and adjoint"
(u1, p1) = SolveState(Shape1)
(lu1, lp1) = SolveAdjoint(Shape1, u1, p1)
#SolveOptimality(Shape1, u1, p1, lu1, lp1)
u1 = project(u1, TotalSpace.sub(0).collapse())
p1 = project(p1, TotalSpace.sub(1).collapse())
#plot(u1, interactive=True)
#plot(p1, interactive=True)
#plot(lu1, interactive=True)
#plot(lp1, interactive=True)
VZero = project(MyZero, TotalSpace.sub(2).collapse())
lu1 = project(lu1, TotalSpace.sub(3).collapse())
lp1 = project(lp1, TotalSpace.sub(4).collapse())
if VolumeConstraint == False:
    assign(q, [u1, p1, VZero, lu1, lp1])
else:
    tmpVol = project(q.split()[5], TotalSpace.sub(5).collapse())
    if BarycenterConstraint == False:
        assign(q, [u1, p1, VZero, lu1, lp1, tmpVol])
    else:
        tmpBary = MaxDim*[0.0]
        for k in range(MaxDim):
            tmpBary[k] = project(q.split()[5+k+1], TotalSpace.sub(5+k+1).collapse())
        if MaxDim == 2:
            assign(q, [u1, p1, VZero, lu1, lp1, tmpVol, tmpBary[0], tmpBary[1]])
        else:
            assign(q, [u1, p1, VZero, lu1, lp1, tmpVol, tmpBary[0], tmpBary[1], tmpBary[2]])

Res1 = 1e+10
Res2 = 1e+10
i = 0
#for i in range(1,MaxOptIter):
while Res2 > ResStop and i < MaxIter:
    print "Total Number of unknowns (state, adjoint, design):", len(q.vector())
    if RestartFolder != None:
        StoreMesh(RestartFolder+"Mesh_%05d.h5"%i, Shape1.mesh, boundary_parts, VolumeParts)
    #update geometry
    N.assign(Shape1.VolumeNormal(Shape1.mesh))
    kappa.assign(Shape1.ComputeDivNVolume(Shape1.mesh, CurvSmooth))
    if BarycenterConstraint:
        for k in range(MaxDim):
            TmpSpace = FunctionSpace(mesh, "CG", 1)
            MyIdExpr1 = Expression("x[%d]"%k, element=TmpSpace.ufl_element())
            MyIdExpr2 = project(MyIdExpr1, FunctionSpace(mesh, "CG", 1))
            MyId[k].assign(MyIdExpr2)
    FN << N
    up = project(u0, TotalSpace.sub(0).collapse())
    up.rename("u", "")
    FU << up
    pp = project(p0, TotalSpace.sub(1).collapse())
    pp.rename("p", "")
    FP << pp
    lup = project(lu0, TotalSpace.sub(3).collapse())
    lup.rename("lu", "")
    FLu << lup
    lpp = project(lp0, TotalSpace.sub(4).collapse())
    lpp.rename("lpp", "")
    FLp << lpp
    #print "Plotting Adjoint"
    #plot(lu0, interactive=True)
    FKappa << kappa
    i = i+1
    if i < StepSwap:
        StepLength = StepLength1
        #Rho = Constant(0.0)
    else:
        StepLength = StepLength2
        #Rho = Constant(0.0)
    bcs = MyResidual(u0, p0, V0, lu0, lp0, TotalSpace, Shape1)
    (A,b) = assemble_system(KKT, sd, bcs)
    """
    A = assemble(KKT)
    b = assemble(sd)
    for bc in bcs:
        bc.apply(A)
        bc.apply(b)
    """

    if VolumeConstraint and not BarycenterConstraint:
        CurVol = assemble(OneR*dx(domain=mesh))
        Vol_offset = CurVol - Vol0
        #print ""
        #print "Test Vol without Bary: Vol_offset (neg = too small = hole too big)", Vol_offset
        #print "Current Volume:", CurVol
        #print "Vol0", Vol0
        #print ""
        VolID = len(b) - 1
        #Test Sign Vol
        b[VolID] = Vol_offset
    
    if BarycenterConstraint:
        CurVol = assemble(OneR*dx(domain=mesh))
        Vol_offset = CurVol - Vol0
        VolID = len(b) - (1+MaxDim)
        #Test Sign Vol
        b[VolID] = Vol_offset
        CurBary = Shape1.ComputeBarycenter(Shape1.mesh)
        BaryOff = MaxDim*[0.0]
        #print ""
        for k in range(MaxDim):
            BaryOff[k] = float(CurBary[k]) - float(InitBary[k])
            b[VolID + k + 1] = BaryOff[k]
            #print "CurBary", k, float(CurBary[k])
            #print "InitBary", k, float(InitBary[k])
            #print "BaryOff", k, BaryOff[k]
        #print ""
        #print "Bary offset:", BaryOff
        #exit()

    #only used for plotting the KKT right hand side
    RHS = Function(TotalSpace)
    RHS.vector()[:] = b
    RHS_V = RHS.split()[2]
    RHS_V.rename("Gradient", "")
    FGrad << RHS_V

    DeltaQ = Function(TotalSpace)
    DeltaQ.rename("KKT_Update", "")
    #list_krylov_solver_preconditioners()
    #exit()
    #solve(A, DeltaQ.vector(), b, "gmres", "hypre_euclid")
    #Standard/LU/Umfpack
    print "Solving full KKT system:"
    solve(A, DeltaQ.vector(), -b)

    #print "Plotting DeltaQ!"
    #plot(DeltaQ.sub(3), interactive=True)
    #plot(DeltaQ.sub(4), interactive=True)

    V_up = DeltaQ.split()[2]
    q.assign(q+DeltaQ)
    #(u0, p0, V0, lu0, lp0, lVol) = split(q)
    
    #set V to zero:
    AllFacets = FacetFunction("size_t", Shape1.mesh)
    AllFacets.set_all(0)
    #bc = DirichletBC(TotalSpace.sub(2), V_up, AllFacets, 0)
    bc = DirichletBC(TotalSpace.sub(2), MyZeroV, AllFacets, 0)
    bc.apply(q.vector())

    obj = assemble(L_Objective)
    MyMove = project(StepLength*V_up, FunctionSpace(Shape1.mesh, V1))
    #MyMove = project(StepLength*V0, FunctionSpace(Shape1.mesh, V1))
    MyMove.rename("Move", "")
    #FW << MyMove
    if SurfaceGradient:
        VPlot = project(V_up, FunctionSpace(Shape1.mesh, V1))
        VPlot.rename("W", "")
        FW << VPlot
        Res2 = norm(MyMove, "L2")
        MyMove2 = MyMove
    else:
        MyMove2 = BoundaryDefo(MyMove)
        MyMove2.rename("WVolSurf", "")
        FW << MyMove2
        Res2 = norm(MyMove2, "L2")

    Residual = Function(TotalSpace)
    Residual.vector()[:] = b
    Res1 = norm(Residual)
    ResU = norm(Residual.sub(0), "L2")
    #FRU << Residual.sub(0)
    ResP = norm(Residual.sub(1), "L2")
    ResV = norm(Residual.sub(2), "L2")
    ResLU = norm(Residual.sub(3), "L2")
    ResLP = norm(Residual.sub(4), "L2")
    if VolumeConstraint:
        #ResVol = norm(Residual.sub(5), "L2")
        ResVol = Vol_offset
    else:
        ResVol = 0.0
    if BarycenterConstraint == False:
        print i, obj, Res1, Res2, ResU, ResP, ResV, ResLU, ResLP, ResVol
    else:
        if MaxDim == 2:
            ResB0 = norm(Residual.sub(6), "L2")
            ResB1 = norm(Residual.sub(7), "L2")
            print i, obj, Res1, Res2, ResU, ResP, ResV, ResLU, ResLP, ResVol, ResB0, ResB1
        if MaxDim == 3:
            ResB0 = norm(Residual.sub(6), "L2")
            ResB1 = norm(Residual.sub(7), "L2")
            ResB2 = norm(Residual.sub(8), "L2")
            print i, obj, Res1, Res2, ResU, ResP, ResV, ResLU, ResLP, ResVol, ResB0, ResB1, ResB2
    if MPI.rank(mpi_comm_world()) == 0:
        fHistory.write("%4d\t%e\t%e\t%e"%(i, obj, Res2, Res1))
        EndIndex = 5
        if VolumeConstraint:
            EndIndex += 1
        if BarycenterConstraint:
            EndIndex = EndIndex + MaxDim
        for k in range(EndIndex):
            fHistory.write("\t%e"%(norm(Residual.sub(k), "L2")))
        fHistory.write("\n")
        fHistory.flush()

    #One Shot: Update Shape every n-th solution iteration
    if i%OptEvery == 0:
        ALE.move(Shape1.mesh, MyMove2)
    #remesh
    if i%RemeshEvery == 0 and i < RemeshStop:
        print "Remeshing"
        if VolumeConstraint:
            VolStore = float(project(q.split()[5], TotalSpace.sub(5).collapse()).vector()[0])
        #print "tmpVol", VolStore
        if BarycenterConstraint:
            BaryStore = MaxDim*[0.0]
            for k in range(MaxDim):
                BaryStore[k] = float(project(q.split()[5+k+1], TotalSpace.sub(5+k+1).collapse()).vector()[0])
                #print "tmpBary:", BaryStore[k]
        #exit()
        
        RemeshFolder = MeshFolder + "/remesh/"
        WriteMSH(Shape1.mesh, Shape1.boundary_parts, AllMarkers, MeshFolder + "/remesh/OldMesh.msh", VolumeParts)
        if MPI.rank(mpi_comm_world()) == 0:
            import platform
            if platform.system() == "Darwin":
                Gmsh_str = "/Applications/Gmsh.app/Contents/MacOS/gmsh"
            else:
                Gmsh_str = "gmsh"
            dolfin_convert_str = "dolfin-convert"
            call([Gmsh_str, "-v", "0", "-2", RemeshFolder+"/"+RemeshLogic+".geo"])
            call([dolfin_convert_str, RemeshFolder+RemeshLogic+".msh", RemeshFolder+MeshName+".xml"])
        #Read the new mesh
        mesh = Mesh(RemeshFolder + MeshName+".xml")
        boundary_parts = MeshFunction("size_t", mesh, RemeshFolder + MeshName+"_facet_region.xml")
        VolumeParts = MeshFunction("size_t", mesh, RemeshFolder + MeshName + "_physical_region.xml")
        Shape1.mesh = mesh
        Shape1.boundary_parts = boundary_parts
        
        #re-declare Function Spaces...
        InitTotalSpace(Shape1.mesh)
        InitTestFunctions()
        InitTrialFunctions()
        #VZero = project(q.split()[2], TotalSpace.sub(2).collapse())
        InitFunctions()
        #InterpolateFunctions()
        InitProblem()
        
        #Compute new exact solution
        print "Computing state and adjoint on new mesh"
        (u1, p1) = SolveState(Shape1)
        (lu1, lp1) = SolveAdjoint(Shape1, u1, p1)
        u1 = project(u1, TotalSpace.sub(0).collapse())
        p1 = project(p1, TotalSpace.sub(1).collapse())
        VZero = project(MyZero, TotalSpace.sub(2).collapse())
        lu1 = project(lu1, TotalSpace.sub(3).collapse())
        lp1 = project(lp1, TotalSpace.sub(4).collapse())
        #Copy adjoints of scalar constraints
        if VolumeConstraint:
            tmpVol = Function(TotalSpace.sub(5).collapse())
            tmpVol.vector()[0] = VolStore
        if VolumeConstraint == False and BarycenterConstraint == False:
            assign(q, [u1, p1, VZero, lu1, lp1])
        elif BarycenterConstraint == False:
            assign(q, [u1, p1, VZero, lu1, lp1, tmpVol])
        else:
            tmpBary = MaxDim*[Function(TotalSpace.sub(5+k+1).collapse())]
            for k in range(MaxDim):
                tmpBary[k].vector()[0] = BaryStore[k]
                #print "Retrieving Stored Bary Adjoint:", BaryStore[k]
            #print ""
            if MaxDim == 2:
                assign(q, [u1, p1, VZero, lu1, lp1, tmpVol, tmpBary[0], tmpBary[1]])
            else:
                assign(q, [u1, p1, VZero, lu1, lp1, tmpVol, tmpBary[0], tmpBary[1], tmpBary[2]])
#convergence history
if MPI.rank(mpi_comm_world()) == 0:
    fHistory.close()
