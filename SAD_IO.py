from dolfin import *
import SAD_MPI
import numpy

def StoreMesh(FileName, MyMesh, boundary_parts = None, volume_parts=None):
    orientation = numpy.asarray(MyMesh.cell_orientations(), dtype = 'float')
    s = FileName
    hdf = HDF5File(mpi_comm_world(), s,'w')
    hdf.write(MyMesh, "mesh")
    if len(orientation)>0:
        OSpace = FunctionSpace(MyMesh, 'DG', 0)
        ori = Function(OSpace)
        ori.vector().set_local(orientation)
        ori.vector().apply('')
        hdf.write(ori,"orientation")
    if boundary_parts != None:
        hdf.write(boundary_parts, "boundary_parts")
    if volume_parts != None:
        hdf.write(volume_parts, "volume_parts")
    hdf.close()

def ReadMesh(FileName):
    s = FileName
    hdf = HDF5File(mpi_comm_world(), s, 'r')
    MyMesh = Mesh()
    if hdf.has_dataset('mesh'):
        hdf.read(MyMesh, 'mesh', False)
    elif hdf.has_dataset('Mesh'):
        hdf.read(MyMesh, 'Mesh', False)
    else:
        print "Sorry, no mesh found in "+FileName
        exit(1)
    if hdf.has_dataset('orientation'):
        MyMesh.init_cell_orientations(Expression(('0.0','0.0','1.0'), degree=1))
        OSpace = FunctionSpace(MyMesh, 'DG', 0)
        ori = Function(OSpace)
        hdf.read(ori, 'orientation')
        orientation = ori.vector().array()
        orientation.astype(int)
        for i in range(MyMesh.num_cells()):
            MyMesh.cell_orientations()[i] = orientation[i]
    MyReturn = [MyMesh]
    if hdf.has_dataset("boundary_parts"):
        MaxDim = MyMesh.topology().dim()
        boundary_parts = MeshFunction('size_t', MyMesh, MaxDim-1)
        #FEniCS Hack: seems fenics is not correctly initializing mesh functions...
        for i in range(len(boundary_parts.array())):
            boundary_parts.array()[i] = 0
        hdf.read(boundary_parts, "boundary_parts")
        MyReturn.append(boundary_parts)
    if hdf.has_dataset("volume_parts"):
        volume_parts = MeshFunction('size_t', mesh, MaxDim)
        #FEniCS Hack: seems fenics is not correctly initializing mesh functions...
        for i in range(len(volume_parts.array())):
            volume_parts.array()[i] = 0
        hdf.read(volume_parts, "volume_parts")
        MyReturn.append(volume_parts)
    hdf.close()
    return MyReturn

def StoreFunctionH5(FileName, F = [], Key = []):
    MyFile = HDF5File(mpi_comm_world(), FileName, "w")
    for i in range(len(F)):
        MyFile.write(F[i], Key[i])
    MyFile.close()
    
def LoadFunctionH5(FileName, MyFunctionSpace = [], Key = []):
    MyFile = HDF5File(mpi_comm_world(), FileName, "r")
    F = []
    for i in range(len(MyFunctionSpace)):
        f = Function(MyFunctionSpace[i])
        f.rename(Key[i], "")
        F.append(f)
        MyFile.read(F[i], Key[i])
    MyFile.close()
    return F

def StoreMeshXML(FileName, mesh, boundary_parts):
    MyFileMesh = File(FileName+".xml")
    MyFileBP = File(FileName+"_facet_region.xml")
    MyFileMesh << mesh
    MyFileBP << boundary_parts

#Needs Boundary Mesh and boundary_boundaryparts as input...
def WriteSTL(gamma, boundary_parts, FacetNormal, RemeshFolder = "./remesh"):
    gamma.init()
    if MPI.size(mpi_comm_world()) > 1:
        print "WriteSTL not working in parallel"
        exit()
    MaxDim = gamma.topology().dim()
    if MaxDim == 1:
        print "Warning, I think STL files need to be 3D (Triangles in 3D Space)"
    AllMarkers = list(set(boundary_parts.array()))
    if MPI.rank(mpi_comm_world()) == 0:
        #STL files must be in the positive octant...
        x_min = [0.0, 0.0, 0.0]
        for v in vertices(gamma):
            for i in range(3):
                if v.x(i) < x_min[i]:
                    x_min[i] = v.x(i)
        if x_min[0] < 0.0 or x_min[1] < 0.0 or x_min[2] < 0.0:
            print "Write STL: Warning: Mesh not in first octant. Auto-translating..."
        import os
        for marker in AllMarkers:
            if not os.path.exists(RemeshFolder):#os.path.dirname(HistoryFileName)):
                os.makedirs(RemeshFolder)#os.path.dirname(HistoryFileName))
            STLFile = open(RemeshFolder+"/Boundary_%d.stl"%marker, "w")
            STLFile.write("solid Shape %d\n"%marker)
            for c in cells(gamma):
                if boundary_parts.array()[c.index()] == marker:
                    CD = FacetNormal.function_space().dofmap().cell_dofs(c.index())
                    N1 = Point(float(FacetNormal.vector()[CD[0]]), float(FacetNormal.vector()[CD[1]]), float(FacetNormal.vector()[CD[2]]))
                    orientation = c.orientation(N1)
                    STLFile.write("facet normal")
                    for i in range(MaxDim+1):
                        STLFile.write(" %e"%(FacetNormal.vector()[CD[i]]))
                    STLFile.write("\n")
                    STLFile.write("  outer loop\n")
                    MyCoords = numpy.zeros([3,3])
                    v_index = 0
                    for v in vertices(c):
                        for i in range(3):
                            MyCoords[v_index, i] = v.x(i)
                        v_index = v_index + 1
                    if orientation == 0:
                        for i in [0,1,2]:
                            STLFile.write("    vertex")
                            for j in range(3):
                                STLFile.write(" %e"%(MyCoords[i,j]+x_min[j]))
                            STLFile.write("\n")
                    else:
                        for i in [0,2,1]:
                            STLFile.write("    vertex")
                            for j in range(3):
                                STLFile.write(" %e"%(MyCoords[i,j]+x_min[j]))
                            STLFile.write("\n")
                    STLFile.write("  endloop\n")
                    STLFile.write("endfacet\n")
            STLFile.write("endsolid Shape\n")
            STLFile.close()

#MyMarkers is a list of boundary markers for which the remeshed geometry is based upon
def WriteMSH(mesh, boundary_parts, MyMarkers, MSHName, volume_parts = None):
    import os
    MaxDim = mesh.topology().dim()
    #AllMarkers = list(set(boundary_parts.array()))
    (CoordsGlobal, CellsGlobal, FacetsGlobal) = SAD_MPI.GlobalizeMesh(mesh)
    BP = SAD_MPI.GlobalizeMeshFunction(boundary_parts)
    if volume_parts != None:
        VP = SAD_MPI.GlobalizeMeshFunction(volume_parts)

    if MPI.rank(mpi_comm_world()) == 0:
        if not os.path.exists(os.path.dirname(MSHName)):
            os.makedirs(os.path.dirname(MSHName))
        MSHFile = open(MSHName, "w")
        MSHFile.write("$MeshFormat\n")
        MSHFile.write("2.2 0 8\n")
        MSHFile.write("$EndMeshFormat\n")
        MSHFile.write("$Nodes\n")
        MSHFile.write("%d\n"%len(CoordsGlobal))
        for i in range(len(CoordsGlobal)):
            MSHFile.write("%d"%(i+1))
            for j in range(MaxDim):
                MSHFile.write(" %e"%CoordsGlobal[i,j])
                #Add fake 3rd dimension
            if MaxDim == 2:
                MSHFile.write(" %e"%0.0)
            MSHFile.write("\n")
        MSHFile.write("$EndNodes\n")
        #determine joint surface edges
        FacetCounter = 0
        for i in range(len(BP)):
            if BP[i] in MyMarkers:
                FacetCounter += 1
        MSHFile.write("$Elements\n")
        #write all nodes again as point-elements so they are displayed in gmsh?
        WritePoints = False
        if WritePoints:
            MyOffset = len(CoordsGlobal)
            MSHFile.write("%d\n"%(MyOffset+len(CellsGlobal)+FacetCounter))
            for i in range(len(CoordsGlobal)):
                MSHFile.write("%d %d %d %d %d %d\n"%(i+1, 15, 2, 0, i+1, i+1))
        else:
            MyOffset = 0
            MSHFile.write("%d\n"%(len(CellsGlobal)+FacetCounter))
        if MaxDim == 2:
            if volume_parts == None:
                for c in range(len(CellsGlobal)):
                    MSHFile.write("%d %d %d %d %d"%(MyOffset+c+1, 2, 2, 0, 0))
                    for j in range(MaxDim+1):
                        MSHFile.write(" %d"%(CellsGlobal[c,j]+1))
                    MSHFile.write("\n")
            else:
                for c in range(len(CellsGlobal)):
                    MSHFile.write("%d %d %d %d %d"%(MyOffset+c+1, 2, 2, 0, VP[c]))
                    for j in range(MaxDim+1):
                        MSHFile.write(" %d"%(CellsGlobal[c,j]+1))
                    MSHFile.write("\n")
            MyOffset += len(CellsGlobal)
            MyCounter = 0
            for marker in MyMarkers:
                for i in range(len(BP)):
                    if BP[i] == marker:
                        MSHFile.write("%d %d %d %d %d"%(MyOffset+MyCounter+1, 1, 2, BP[i], BP[i]))
                        for j in range(MaxDim):
                            MSHFile.write(" %d"%(FacetsGlobal[i,j]+1))
                        MSHFile.write("\n")
                        MyCounter += 1
        else:
            print "WRITE .MSH ONLY FULLY IMPLEMENTED FOR 2D"
        MSHFile.write("$EndElements\n")
        MSHFile.close()
