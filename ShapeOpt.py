#Shape Optimization and Geometry Toolbox for FEniCS
#by Stephan Schmidt, 2016

# -*- coding: iso-8859-15 -*-
from dolfin import *

import SAD_MPI

from SAD_tangent_differentiation import *
#from utils import *

import numpy
import os
import math

ExprDegree = 2 #base degree for all expressions

def ShapeDerivative(form, mesh, V, n=None, State=None, StateDirection=None, kappa=None, Is_Normal=[], Constant_In_Normal=[], boundary_parts = None, SymmetryDirection = None, IncludeNormalVariation = True, IncludeCurvatureVariation=True, GenerateSurface = True):
    
    from ufl import algorithms
    from ufl import differentiation
    from ufl import checks

    from ufl.algorithms.map_integrands import map_integrand_dags

    from SAD_apply_derivatives import SAD_apply_derivatives

    import SAD_simplify
    #import SAD_tangent_differentiation
    from SAD_derivatives import SAD_material_derivative
    
    #mesh = n.function_space().mesh()
    mdx = dx(domain=mesh)
    if boundary_parts != None:
        mds = ds(domain=mesh, subdomain_data=boundary_parts)
    else:
        mds = ds(domain=mesh)

    from ufl import algorithms

    MyTerms = []
    for i in form.integrals():
        MySubDomain = i.subdomain_id()
        MyMeasure = i.integral_type()
        MyIntegrand = i.integrand()
        #immediatly throw out all constants, extra knowledge, etc...
        MyIntegrand = SAD_simplify.CreateStandardExpression(MyIntegrand)
        if kappa != None and n != None:
            MyIntegrand = SAD_simplify.ApplySimplifications(MyIntegrand, n, kappa, Is_Normal, Constant_In_Normal)
            if SymmetryDirection != None:
                MyIntegrand = SAD_simplify.IncorporateSymmetry(MyIntegrand, SymmetryDirection, V, n)
            #print "Shape Derivative: Simplified Integrand:"
            #print MyIntegrand

        if MyMeasure == "cell":
            MyIntegrand = SAD_simplify.CreateStandardExpression(MyIntegrand)
            #print "Shape Derivative: Processing (volume):", MyIntegrand
            #Volume / Weak Shape Derivative
            if not GenerateSurface:
                #print "Shape Derivative: Warning: Volume Subdomain Shape Derivatives only work in volume formulation for now. This can cause complications with second derivatives."
                #print "Volume Integral: Integrand:", MyIntegrand
                #print "Volume Integral: State:", State, StateDirection
                Expr = div(V)*MyIntegrand
                if State != None:
                    Material = derivative(MyIntegrand, State, StateDirection)
                    Material = SAD_material_derivative(Material, V)
                    Expr = Expr + Material
                Expr = SAD_simplify.CreateStandardExpression(Expr)
                """
                if SymmetryDirection != None:
                    Expr = SAD_simplify.IncorporateSymmetry(Expr, SymmetryDirection, V, n)
                    Expr = SAD_simplify.RemoveFromSForm(Expr, V, grad(div(SymmetryDirection)))
                """
                #print "Shape Derivative: Adding (Volume 1):", Expr
                SAD_simplify.MySumAppend(Expr, mdx(MySubDomain), MyTerms)
            #Boundary / Strong Shape Derivative
            else:
                alpha = inner(V, n)
                Expr = alpha*MyIntegrand
                Expr = SAD_simplify.CreateStandardExpression(Expr)
                #print "Shape Derivative: Adding (Volume 2):", Expr
                SAD_simplify.MySumAppend(Expr, mds, MyTerms)
                if State != None:
                    Local = derivative(MyIntegrand, State, StateDirection)
                    Local = SAD_simplify.CreateStandardExpression(Local)
                    SAD_simplify.MySumAppend(Local, mdx(MySubDomain), MyTerms)
        #processing facet integral (strong and weak form):
        elif MyMeasure == "exterior_facet" or MyMeasure == "interior_facet":
            if boundary_parts != None:
                if MyMeasure == "exterior_facet":
                    mds = ds(domain=mesh, subdomain_data=boundary_parts)
                else:
                    mds = dS(domain=mesh, subdomain_data=boundary_parts)
            else:
                if MyMeasure == "exterior_facet":
                    mds = ds(domain=mesh)
                else:
                    mds = dS(domain=mesh)

            #print "Shape Derivative: Processing (surface):", MyIntegrand
            if "grad(n)" in str(MyIntegrand) or "div(n)" in str(MyIntegrand):
                print "Shape Derivative: Differential operators on normal not supported, sorry..."
                exit()
            #Volume / Weak Shape Derivative
            if not GenerateSurface:#MySubDomain != "everywhere":
                #print "Surface Integral: Integrand:", MyIntegrand
                #print "Surface Integral: State:", State, StateDirection
                if V not in Constant_In_Normal:
                    if n == None:
                        print "Need to provide a normal to compute tangential divergence"
                        exit(1)
                    Expr = SAD_tangent_differentiation.div_tan(V, n)*MyIntegrand
                    #Expr = div(V)*MyIntegrand
                else:
                    Expr = div(V)*MyIntegrand
                if State != None:
                    Material = derivative(MyIntegrand, State, StateDirection)
                    Material = SAD_material_derivative(Material, V)
                    Expr = Expr + Material
                Expr = SAD_simplify.CreateStandardExpression(Expr)
                #print "Shape Derivative: Adding (Surface / weak):", Expr
                SAD_simplify.MySumAppend(Expr, mds(MySubDomain), MyTerms)
                #Test for dependency on normal:
                if n != None:
                    dn = Function(VectorFunctionSpace(mesh, "CG", 1))
                    dn.rename("SAD_dn", "")
                    NV0 = derivative(MyIntegrand, n, dn)
                    NV0 = SAD_simplify.CreateStandardExpression(NV0)
                    if NV0 != 0 and IncludeNormalVariation:
                        #print ""
                        #print "NORMAL (weak): Dependency detected"
                        #print "NV0>", NV0
                        D_tanV = grad_tan(V, n)
                        dn2 = dot(n,D_tanV) #same as -dot(transpose(D_tanV),n)
                        #do sign later...
                        NormalExpr = derivative(MyIntegrand, n, dn2)
                        NormalExpr = SAD_simplify.CreateStandardExpression(-NormalExpr)
                        SAD_simplify.MySumAppend(NormalExpr, mds(MySubDomain), MyTerms)
            #Surface/strong form
            else:
                if kappa == None:
                    print "Shape Derivative: Boundary Integrals Require Curvature. Please specify"
                    exit()
                alpha = inner(V, n)
                #use the well-known formula (V,n)*(dg/dn + kappa*g)
                #compute partial spatial derivative without normal (psd)
                isconstant = checks.is_globally_constant(MyIntegrand)
                if not isconstant:
                    psd1 = alpha*inner(grad(MyIntegrand), n)
                    #print "PSD1 Input:"
                    #print psd1
                    psd1 = SAD_simplify.CreateStandardExpression(psd1)
                    #print "PSD1 Standard Form:"
                    #print psd1
                    psd1 = SAD_simplify.ApplySimplifications(psd1, n, kappa, Is_Normal, Constant_In_Normal)
                    #print "PSD1 Simplification:"
                    #print psd1
                    if psd1 != as_ufl(0.0):
                        if SymmetryDirection != None:
                            psd1 = SAD_simplify.IncorporateSymmetry(psd1, SymmetryDirection, V, n)
                            #print "PSD1 After Incorporate Symmetry:"
                            #print psd1
                        #print "Shape Derivative: Adding (standard contribution 1):"
                        #print psd1
                        #print ""
                        if MyMeasure == "interior_facet":
                            psd1 = 2.0*avg(psd1)
                        SAD_simplify.MySumAppend(psd1, mds(MySubDomain), MyTerms)
                else:
                    #print "Omitting Standard 1 because it was zero"
                    pass
                psd2 = alpha*kappa*MyIntegrand
                psd2 = SAD_simplify.CreateStandardExpression(psd2)
                psd2 = SAD_simplify.ApplySimplifications(psd2, n, kappa, Is_Normal, Constant_In_Normal)
                if SymmetryDirection != None:
                    psd2 = SAD_simplify.IncorporateSymmetry(psd2, SymmetryDirection, V, n)
                #print "Shape Derivative: Adding (standard contribution 2):"
                #print psd2
                #print ""
                if MyMeasure == "interior_facet":
                    psd2 = 2.0*avg(psd2)
                SAD_simplify.MySumAppend(psd2, mds(MySubDomain), MyTerms)
                #Test for dependency on normal:
                if n != None:
                    dn = Function(VectorFunctionSpace(mesh, "CG", 1))
                    dn.rename("SAD_dn", "")
                    NV0 = derivative(MyIntegrand, n, dn)
                    NV0 = SAD_simplify.CreateStandardExpression(NV0)
                    #print "TEST:", MyIntegrand
                    if NV0 != 0 and IncludeNormalVariation:
                        #print ""
                        #print "NORMAL (strong form): Dependency detected"
                        #print "NV0:", NV0
                        if not V in Is_Normal:
                            #print "Shape Derivative: Expecting tangential motion in normal part"
                            D_tanV = grad(V) - outer(dot(grad(V),n),n)
                            dn2 = dot(n,D_tanV) #same as -dot(transpose(D_tanV),n)
                            #do sign later...
                            Expr = derivative(MyIntegrand, n, dn2)
                            Expr = SAD_simplify.CreateStandardExpression(-Expr)
                            SAD_simplify.MySumAppend(Expr, mds(MySubDomain), MyTerms)
                        else:
                            #print "Apply Tangent Stokes:"
                            NewExpr = SAD_simplify.ApplyTangentStokes(NV0, dn, V, n, kappa, Is_Normal+[n])
                            if NewExpr != as_ufl(0.0):
                                #print "NORMAL: Apply Simplification to Standard:"
                                NewExpr = SAD_simplify.CreateStandardExpression(NewExpr)
                                #print "NORMAL: After Simplification:", NewExpr
                                #print "NORMAL: Add Knowledge/Symmetry:"
                                NewExpr = SAD_simplify.ApplySimplifications(NewExpr, n, kappa, Is_Normal, Constant_In_Normal)
                                if SymmetryDirection != None:
                                    NewExpr = SAD_simplify.IncorporateSymmetry(NewExpr, SymmetryDirection, V, n)
                                #print "NORMAL: Adding (normal contribution):", NewExpr
                                SAD_simplify.MySumAppend(NewExpr, mds(MySubDomain), MyTerms)
                                #print ""
                            else:
                                #print "NORMAL (strong form): Tangent-Stokes has returned Zero..."
                                #print ""
                                pass
                #Add local derivative
                if State != None:
                    #print ""
                    #print "STATE: Dependecy Detected"
                    #print "Integrand:", MyIntegrand
                    #print "State:", State, StateDirection
                    Local = derivative(MyIntegrand, State, StateDirection)
                    Local = SAD_simplify.CreateStandardExpression(Local)
                    #print "STATE: Adding Local:", Local
                    #print ""
                    SAD_simplify.MySumAppend(Local, mds(MySubDomain), MyTerms)
            #Test for curvature dependency:
            if kappa != None:
                #print "TEST FOR CURVATURE:"
                NK = differentiation.VariableDerivative(MyIntegrand,kappa)
                NK = SAD_simplify.CreateStandardExpression(NK)
                if NK != 0 and IncludeCurvatureVariation:
                    if NK == as_ufl(1.0):
                        NK = Expression("1.0", domain=mesh, degree=ExprDegree)
                        NK.rename("1.0", "")
                    #print "NK:"
                    #print NK
                    if V in Is_Normal:
                        #print "CURVATURE USE 1"
                        OperatorTest = SAD_simplify.TestOperator()
                        beta = inner(SymmetryDirection, n)
                        def TreatCurvature(o):
                            MyType = OperatorTest(o)
                            if MyType == "SAD_SUM":
                                a, b = o.ufl_operands
                                Expr1 = TreatCurvature(a)
                                Expr2 = TreatCurvature(b)
                                return Expr1 + Expr2
                            CList = []
                            SAD_simplify.CollectProduct(o, CList)
                            if beta in CList:
                                CList.remove(beta)
                                if beta in CList:
                                    print "Shape Derivative: Curvature: Error: inner(V, n) not linear!"
                                    exit(1)
                                f = CList[0]
                                for i in range(1, len(CList)):
                                    f = f*CList[i]
                                Expr = f*inner(grad_tan(beta, n), grad_tan(alpha, n))
                                #Expr = Expr + alpha*beta*div_tan(grad_tan(f, n),n)
                                Expr = Expr + alpha*beta*laplace_tan(f, n, kappa)
                                #Expr = Expr - inner(grad_tan(alpha*beta, n), grad_tan(f, n))
                                return Expr
                            else:
                                #1x integration by parts
                                Expr = inner(grad(NK), dot(n, grad(V))) - inner(grad(NK),n)*inner(n, dot(n, grad(V)))
                                #2x integration by parts
                                #Expr = alpha*(div_tan(grad_tan(NK, n), n))
                            return Expr
                        Expr = TreatCurvature(NK)
                    else:
                        #print "CURVATURE USE 2"
                        Expr = inner(grad_tan(NK, n), grad_tan(alpha, n))
                    if Expr != None:
                        #print "Start Curvature Simplification"
                        #print "Step1:"
                        #print Expr
                        Expr = SAD_simplify.CreateStandardExpression(Expr)
                        Expr = SAD_simplify.ApplySimplifications(Expr, n, kappa, Is_Normal, Constant_In_Normal)
                        #print "Step2:"
                        #print Expr
                        if SymmetryDirection != None:
                            Expr = SAD_simplify.IncorporateSymmetry(Expr, SymmetryDirection, V, n)
                            #print "Step3:"
                            #print Expr
                        pass
                    #print "Shape Derivative: Adding (curvature contribution):"
                    #print Expr
                    SAD_simplify.MySumAppend(Expr, mds(MySubDomain), MyTerms)
        #neither surface nor volume integral
        else:
            raise ValueError, "Unknown integral type: {}.".format(MyMeasure)
    #create final expression
    ShapeDerivative = SAD_simplify.MyBuildSADForm(MyTerms, SymmetryDirection, V, n)
    return ShapeDerivative

"""
def ProcessState(form, State, StateDirection):
    print ""
    print "State: Processing:", form
    MyTerms = []
    MyInnerDerivative = derivative(form, State, StateDirection)
    #print "MyInnerDerivative:", MyInnerDerivative
    for StateTerm in MyInnerDerivative.integrals():
        if StateTerm.integral_type() == "cell":
            #print "State: Volume Integral"
            MyMeas = dx(StateTerm.subdomain_id())
        elif StateTerm.integral_type() == "exterior_facet":
            #print "State: Surface exterior Integral"
            MyMeas = ds(StateTerm.subdomain_id())
        elif StateTerm.integral_type() == "interior_facet":
            #print "State: Surface interior Integral"
            MyMeas = dS(StateTerm.subdomain_id())
        #print ""
        #print "Shape Derivative: Dependency on State detected"
        MyForm = StateTerm.integrand()
        print "Input to simplification:", MyForm
        MyForm = SAD_simplify.CreateStandardExpression(MyForm)
        print "State: Output of simplification:", MyForm
        if MyForm != as_ufl(0.0):
            print "State: Shape Derivative: State: Adding", MyForm
            MyTerms.append(MyForm*MyMeas)
        else:
            print "State: Simplification has resulted in zero"
        print ""
    ShapeDerivative = MyTerms[0]
    for i in range(1,len(MyTerms)):
        ShapeDerivative += MyTerms[i]
    return ShapeDerivative
"""

class ShapeOpt:
    def __init__(self, WorkDirectory="", Restart = False):
        self.verbose = 1
        self.OptIter = 0
        self.mesh = []
        self.boundary_parts = []
        self.SurfBoundaryParts = []
        self.MaxOrder = 1
        if self.MaxOrder < 1:
            self.MaxOrder = 1
        self.VariableBoundary = []
        self.Communicator = mpi_comm_world()
        self.WorkDirectory = WorkDirectory
        self.Restart = Restart
        if WorkDirectory != "":
            HistoryFileName = WorkDirectory + "/History.txt"
            if MPI.rank(self.Communicator) == 0:
                if not os.path.exists(os.path.dirname(HistoryFileName)):
                    os.makedirs(os.path.dirname(HistoryFileName))
            MPI.barrier(self.Communicator)
            if self.Restart == True:
                self.HistoryFile = open(HistoryFileName, "a")
            else:
                self.HistoryFile = open(HistoryFileName, "w")
            self.FlushOutput = True
            self.ReWriteMesh = True
            self.VisFunctions = {}
        self.UseLU = True
        #Expression to base element matching on, MUST ALWAYS BE MIRROR-SYMMETRIC
        #self.TestExpression = Expression("pow(x[0],2) + pow(x[1],4) + pow(x[2],6)")
        #self.TestExpression = Expression("fabs(x[0]) + fabs(x[1]) + fabs(x[2])")
        self.TestTol = 1e-14
        
    def MyProject(self, v, V, solver_type="default"):
        return project(v,V,solver_type=solver_type)

    def WriteHistory(self, Data):
        if MPI.rank(self.Communicator) == 0:
            self.HistoryFile.write("%4d" % self.OptIter)
            for i in Data:
                self.HistoryFile.write(" %+e"%i)
            self.HistoryFile.write("\n")
            self.HistoryFile.flush()

    def MakeSurfBoundaryParts(self, mesh, gamma, boundary_parts):
        MaxDim = mesh.topology().dim()
        mapa = gamma.entity_map(MaxDim-1)
        SurfBoundaryParts=MeshFunction('size_t', gamma, 1)
        for ver in entities(gamma, MaxDim-1):
            i = mapa[ver.index()]
            SurfBoundaryParts[ver.index()] = boundary_parts[i]
        return SurfBoundaryParts

    #Returns a DG 0 surface vector function with the face normal
    def SurfaceFaceNormal(self, mesh, reorient=True):
        gamma = BoundaryMesh(mesh, "exterior")
        dim = mesh.topology().dim()
        NSpace = VectorFunctionSpace(gamma, 'DG', 0, dim)
        N = Function(NSpace)
        dof_min, dof_max = N.vector().local_range()
        ll = dof_max - dof_min
        values = numpy.empty(ll)
        GlobalValues = numpy.zeros(len(N.vector()))
        for MyFace in entities(gamma, dim-1):
            FaceIndex = MyFace.index()
            MyVertex = MyFace.entities(0)
            ver0 = numpy.zeros(dim)
            ver1 = numpy.zeros(dim)
            ver2 = numpy.zeros(dim)
            if dim == 3:
                for i in range(0,3):
                    ver0[i] = Vertex(gamma, MyVertex[0]).x(i)
                    ver1[i] = Vertex(gamma, MyVertex[1]).x(i)
                    ver2[i] = Vertex(gamma, MyVertex[2]).x(i)
                #1st tangent
                u = [ver1[0]-ver0[0], ver1[1]-ver0[1], ver1[2]-ver0[2]]
                #2nd tangent
                v = [ver2[0]-ver0[0], ver2[1]-ver0[1], ver2[2]-ver0[2]]
                #normal
                n = [u[1]*v[2] - u[2]*v[1], u[2]*v[0] - u[0]*v[2], u[0]*v[1] - u[1]*v[0]]
            else:
                for i in range(0,2):
                    ver0[i] = Vertex(gamma, MyVertex[0]).x(i)
                    ver1[i] = Vertex(gamma, MyVertex[1]).x(i)
                #1st tangent
                u = [ver1[0]-ver0[0], ver1[1]-ver0[1]]
                #normal
                n = [ver1[1]-ver0[1], -(ver1[0]-ver0[0])]
            norm = numpy.linalg.norm(n)
            n = n/norm
            #check orientation
            if reorient==True:
                mapa = gamma.entity_map(dim-1)
                ParentFace = Facet(mesh, mapa[FaceIndex])
                PTetrahedron = ParentFace.entities(dim)
                PTetrahedron = Cell(mesh, PTetrahedron[0])
                PMid = [0.0]*dim
                for i in PTetrahedron.entities(0):
                    Mid = Vertex(mesh, i)
                    for j in range(dim):
                        PMid[j] = PMid[j] + (Mid.x(j)/(dim+1))
                PRef = numpy.zeros(dim)
                for i in range(dim):
                    PRef[i] = PMid[i]-ver0[i]
                d = 0.0
                for i in range(dim):
                    d += n[i]*PRef[i]
                if d > 0.0 and reorient==True:
                    n = -n
            IndexGlobal = MyFace.global_index()
            for j in range(dim):
                GlobalValues[dim*IndexGlobal + j] = n[j]
        SAD_MPI.SyncSum(GlobalValues)
        for i in range(dof_min, dof_max):
            values[i-dof_min] = GlobalValues[i]
        N.vector().set_local(values)
        N.vector().apply('')
        N.rename("n", "")
        return N

    #return a CG-1 function in the volume that is the vertex normal on the boundary and zero in the volume
    def VolumeNormal(self, mesh, Symm = [], boundary_parts = None, InteriorIDs = None):
        dim = mesh.topology().dim()
        NumVertex = mesh.size_global(0)
        MyNormal = numpy.zeros([NumVertex, dim])
        NumAvg = numpy.zeros(NumVertex)
        if InteriorIDs == None:
            for f in facets(mesh):
                if f.exterior() and f.is_ghost() == False:
                    for v in vertices(f):
                        vID = v.global_index()
                        for j in range(dim):
                            MyNormal[vID, j] += f.normal(j)
                        NumAvg[vID] += 1
        else:
            ct = mesh.type()
            for c in cells(mesh):
                fcount = 0
                for f in facets(c):
                    if f.is_ghost() == False and (f.exterior() or boundary_parts[f.index()] in InteriorIDs):
                        fnormal = ct.normal(c, fcount)
                        for v in vertices(f):
                            vID = v.global_index()
                            for j in range(dim):
                                MyNormal[vID, j] += f.normal(j)
                            NumAvg[vID] += 1
                    fcount += 1
        SAD_MPI.SyncSum(MyNormal)
        SAD_MPI.SyncSum(NumAvg)
        SSpace = FunctionSpace(mesh, "CG", 1)
        V2D = vertex_to_dof_map(SSpace)
        N = Function(VectorFunctionSpace(mesh, "CG", 1, dim))
        LocValues = numpy.zeros(N.vector().local_size())
        OwnerRange = SSpace.dofmap().ownership_range()
        for v in vertices(mesh):
            vID = v.index()
            vIDG = v.global_index()
            DDof = V2D[v.index()]
            IsOwned = OwnerRange[0] <= SSpace.dofmap().local_to_global_index(DDof) and SSpace.dofmap().local_to_global_index(DDof) < OwnerRange[1]
            if IsOwned and NumAvg[vIDG] > 0.0:
                for j in range(dim):
                    LocValues[dim*DDof+j] = MyNormal[vIDG, j]/NumAvg[vIDG]
                #Apply Symmetry Plane
                for i in Symm:
                    if near(v.x(i), 0.0, DOLFIN_EPS):
                        LocValues[dim*DDof+i] = 0.0
                #Normalize
                MyValue = 0.0
                for j in range(dim):
                    MyValue += LocValues[dim*DDof+j]*LocValues[dim*DDof+j]
                MyValue = sqrt(MyValue)
                if MyValue > 1e-6:
                    for j in range(dim):
                        LocValues[dim*DDof+j] /= MyValue
                else:
                    for j in range(dim):
                        LocValues[dim*DDof+j] = 0.0
        N.vector().set_local(LocValues)
        N.vector().apply("")
        N.rename("SAD_VOLUME_NORMAL", "dummy")
        return N

    def NormalFromSignedDistance(self, SignedDistance, mesh, boundary_parts=None, InteriorIDs=None):
        VSpace = VectorFunctionSpace(mesh, "CG", 1)
        v = TestFunction(VSpace)
        w = TrialFunction(VSpace)
        n = Function(VSpace)
        n.rename("normal", "")
        a = inner(v,w)*(dx+ds)
        l = inner(v,-grad(SignedDistance))*ds
        if InteriorIDs != None:
            dS = dolfin.dS(domain=mesh, subdomain_data=boundary_parts)
            for i in InteriorIDs:
                a += avg(inner(v,w))*dS(i)
                l += avg(inner(v,-grad(SignedDistance)))*dS(i)
        solve(a==l, n)
        #plot(n, interactive=True)
        return n

    #Average a per face vector field to a vertex CG1 vector field
    def AverageFaceQuantity(self, FaceNormal, boundary_parts = [], markers = []):
        NSpace = FaceNormal.function_space()
        gamma = NSpace.mesh()
        MaxDim = NSpace.element().value_dimension(0)
        DofMapFace = NSpace.dofmap()
        #local storage for three vector components per vertex of the whole mesh
        LocValues = numpy.zeros(len(FaceNormal.vector()))
        LocNumAverages = numpy.zeros(len(FaceNormal.vector())/MaxDim)
        GlobalFaceNormal = self.GlobalizeFunction(FaceNormal)
        for v in vertices(gamma):
            Index = v.index()
            #self.pprint("Processing Vertex Index %d"%Index)
            #PARALLEL: Turn local mesh index into global mesh index
            #IndexGlobal = GT.global_indices(0)[Index]
            IndexGlobal = v.global_index()
            Faces = cells(v)#v.entities(MaxDim-1)
            FNormalSum = numpy.zeros(MaxDim)
            MyNumFaces = 0.0
            for f in Faces:
                i = f.index()
                GID = f.global_index()
                if markers != []:
                    FaceMarker = boundary_parts.array()[i]
                    IsInMarker = FaceMarker in markers
                else:
                    IsInMarker = True
                if IsInMarker:
                    MyNumFaces = MyNumFaces + 1.0
                    FaceDofs = DofMapFace.cell_dofs(i)#Local-to-global mapping of dofs on a cell
                    for j in range(MaxDim):
                        ID1 = DofMapFace.local_to_global_index((FaceDofs[j]))
                        value = GlobalFaceNormal[ID1]#FaceNormal.vector()[ID1]
                        FNormalSum[j] = FNormalSum[j] + value
            #LocNumAverages[IndexGlobal] = len(Faces)
            LocNumAverages[IndexGlobal] = MyNumFaces
            for j in range(MaxDim):
                LocValues[MaxDim*IndexGlobal + j] = FNormalSum[j]
        LocValues = self.SyncSum(LocValues)
        LocNumAverages = self.SyncSum(LocNumAverages)
        #from now on, LocValues actually contains the globally summed face normals into vertex normals
        MoveSpaceS = FunctionSpace(gamma, "CG", 1)
        MoveSpaceV = VectorFunctionSpace(gamma, "CG", 1, MaxDim)
        VertexNormal = Function(MoveSpaceV)
        dof_min, dof_max = VertexNormal.vector().local_range()
        dof_max = int(dof_max/MaxDim)
        dof_min = int(dof_min/MaxDim)
        ll = dof_max - dof_min
        MyValues = numpy.empty(MaxDim*ll)
        for i in range(ll):
            GDof = i#dof_min + i
            vert = dof_to_vertex_map(MoveSpaceS)[GDof]
            MyVert = MeshEntity(gamma, 0, vert)
            IndexGlobal = MyVert.global_index()
            #IndexGlobal = GT.global_indices(0)[vert]
            quotient = LocNumAverages[IndexGlobal]
            if quotient > DOLFIN_EPS:
                norm = 0.0
                for j in range(MaxDim):
                    NormalJ = LocValues[MaxDim*IndexGlobal + j]/quotient
                    MyValues[MaxDim*i+j] = NormalJ
                    norm = norm + NormalJ*NormalJ
                norm = sqrt(norm)
                for j in range(MaxDim):
                    MyValues[MaxDim*i+j] = MyValues[MaxDim*i+j]/norm
        VertexNormal.vector().set_local(MyValues)
        VertexNormal.vector().apply('')
        VertexNormal.rename(FaceNormal.name(), "dummy")
        return VertexNormal

    #Only works for 3D Vectorfield of type "CG" 1
    #and only for a vectorfield over the internal mesh
    def NormalizeVectorField(self, V, fixed=[]):
        mesh = V.function_space().mesh()
        coords = mesh.coordinates()
        MaxDim = V.function_space().element().value_dimension(0)
        #dof_min, dof_max = V.vector().local_range()
        #dof_min = int(dof_min/MaxDim)
        #dof_max = int(dof_max/MaxDim)
        #ll = dof_max - dof_min
        d2v = dof_to_vertex_map(FunctionSpace(mesh, "CG", 1))
        NewValues = numpy.zeros(V.vector().local_size())
        for i in range(V.vector().local_size()/MaxDim):
            #v = MeshEntity(mesh, 0, d2v[i])
            x = numpy.array(coords[d2v[i]])
            norm = 0.0
            for j in range(MaxDim):
                if not (j in fixed and near(x[j], 0.0, self.TestTol)):
                    value = V.vector()[MaxDim*i + j]
                    norm = norm + value*value
            norm = sqrt(float(norm))
            #do not normalize if quantity is zero...
            if norm > DOLFIN_EPS:
                for j in range(MaxDim):
                    if j in fixed and near(x[j], 0.0, self.TestTol):
                        NewValues[MaxDim*i + j] = 0.0
                    else:
                        NewValues[MaxDim*i + j] = V.vector()[MaxDim*i + j]/norm
        V.vector().set_local(NewValues)
        V.vector().apply('')

    #Take a CG1 Vector Function, aka the normal on a subsurface, and make
    #it zero except for precisely one node of global index GID
    #and scale with MyEps
    #Helps with finite differences...
    def ReduceVectorFieldToNode(self, V, GID, MyEps = 1.0):
        mesh = V.function_space().mesh()
        MaxDim = V.function_space().element().value_dimension(0)
        LocSize = V.vector().local_size()
        LocValues = numpy.zeros(LocSize)
        V2D = vertex_to_dof_map(FunctionSpace(mesh, "CG", 1))
        V_new = Function(VectorFunctionSpace(mesh, "CG", 1, MaxDim))
        for v in vertices(mesh):
            if v.global_index() == GID:
                MyDof = V2D[v.index()]
                for i in range(MaxDim):
                    LocValues[MaxDim*MyDof+i] = V.vector()[MaxDim*MyDof+i]
        V_new.vector().set_local(LocValues)
        V_new.vector().apply("")
        V_new.rename("n", "dummy")
        return V_new

    def VertexOnBoundary(self, mesh, boundary_parts, markers, InteriorOnly = False, Symmetry=[]):
        OnSubsurface = numpy.zeros(mesh.size_global(0), dtype=numpy.int)
        #print "Interior Only:", InteriorOnly
        if InteriorOnly == False:
            for f in facets(mesh):
                if f.exterior() and not f.is_ghost():
                    MyMarker = boundary_parts[f.index()]
                    if MyMarker in markers:
                        for v in vertices(f):
                            OnSubsurface[v.global_index()] = 1
            self.SyncSum(OnSubsurface)
            OnSubsurface[numpy.nonzero(OnSubsurface)] = 1
        else:
            #print "Looking for", markers
            for v in vertices(mesh):
                OneWrong = False
                OneIncluded = False
                for f in facets(v):
                    if f.exterior() and not f.is_ghost():
                        MyMarker = boundary_parts.array()[f.index()]
                        if MyMarker in markers:
                            OneIncluded = True
                        if MyMarker not in markers and MyMarker not in Symmetry:
                            OneWrong = True
                if OneIncluded == True and OneWrong == False:
                    OnSubsurface[v.global_index()] = 1
                else:
                    OnSubsurface[v.global_index()] = -1
            AllInfo = self.SyncAllGather(OnSubsurface)
            for i in range(len(OnSubsurface)):
                MyMin = numpy.min(AllInfo[i, :])
                MyMax = numpy.max(AllInfo[i, :])
                if MyMin < 0:
                    OnSubsurface[i] = 0
                elif MyMax > 0:
                    OnSubsurface[i] = 1
                else:
                    OnSubsurface[i] = 0
                #OnSubsurface[i] = numpy.sum(AllInfo[i, :])
        #mf = MeshFunction("int", mesh, 0)
        #for i in range(len(mf.array())):
        #    mf.array()[i] = 0
        #for v in vertices(mesh):
        #    mf.array()[v.index()] = OnSubsurface[v.global_index()]
        #plot(mf, interactive=True)
        #File("Tmp/TestBoundary.pvd", "compressed") << mf
        #File("Tmp/Mesh.pvd", "compressed") << mesh
        return OnSubsurface
        
    #Take a 3D vector CG1-Function V and set it to zero everywhere execpt on faces of marker "marker"
    def InjectOnSubSurface(self, V, mesh, boundary_parts, marker):
        MaxDim = mesh.topology().dim()
        gamma = BoundaryMesh(mesh, "exterior")
        SpaceS = FunctionSpace(gamma, "CG", 1)
        SpaceV = VectorFunctionSpace(gamma, "CG", 1, MaxDim)
        V2 = Function(SpaceV)

        dof_min, dof_max = V2.vector().local_range()
        dof_min = int(dof_min/MaxDim)
        dof_max = int(dof_max/MaxDim)
        ll = dof_max - dof_min
        LocValues = numpy.zeros(MaxDim*ll)

        MyCorrectBoundary = self.VertexOnBoundary(mesh, boundary_parts, marker)
        
        mapa = gamma.entity_map(0)
        DofToVertex = dof_to_vertex_map(SpaceS) #maps to local/processor dofs
        for i in range(dof_min, dof_max):
            VertexID = int(DofToVertex[i-dof_min])
            #Vert = MeshEntity(gamma, 0, VertexID)
            #GlobalID = Vert.global_index()
            GlobalID = MeshEntity(mesh, 0, mapa[VertexID]).global_index()
            if MyCorrectBoundary[GlobalID] > 0:
                for j in range(0, MaxDim):
                    value = V.vector()[MaxDim*i+j]
                    LocValues[MaxDim*(i-dof_min)+j] = V.vector()[MaxDim*i+j]
        V2.vector().set_local(LocValues)
        V2.vector().apply('')
        return V2

    def MakeV(self, mesh, boundary_parts):
        gamma = BoundaryMesh(mesh, "exterior")
        V = self.SurfaceFaceNormal(mesh)
        V = self.AverageFaceQuantity(V)
        #fix x-movement
        #self.NormalizeVectorField(V, 0)
        #all degrees of freedom
        self.NormalizeVectorField(V)
        V = self.InjectOnSubSurface(V, mesh, boundary_parts, self.VariableBoundary)
        V.rename("V", "Dummy")
        return V

    #returns surface vector...
    def MultiplySurfaceWithVn(self, SGrad, StepLength, V, mesh, boundary_parts):
        if not self.MaxOrder == 1:
            print "ERROR multiply (V,n) only works for 1st order"
            exit()
        #mesh = V.function_space().mesh()
        #boundary_parts = self.ReduceMeshFunctionToSurface(mesh, boundary_parts)
        MaxDim = mesh.topology().dim()
        gamma = BoundaryMesh(mesh, "exterior")
        SpaceS = FunctionSpace(gamma, "CG", 1)
        SpaceV = VectorFunctionSpace(gamma, "CG", 1, MaxDim)
        SGradV = Function(SpaceV)
            
        dof_min, dof_max = SGradV.vector().local_range()
        dof_min = int(dof_min/MaxDim)
        dof_max = int(dof_max/MaxDim)
        ll = dof_max - dof_min
        LocValues = numpy.zeros(MaxDim*ll)
        
        N = self.SurfaceFaceNormal(mesh)
        N = self.AverageFaceQuantity(N)
        self.NormalizeVectorField(N)
            
        #exclude boundary
        gamma.init()
        mapa = gamma.entity_map(MaxDim-1)
        mesh.init()
        LocalToGlobal = gamma.topology().global_indices(0)
        MyCorrectBoundary = self.VertexOnBoundary(mesh, boundary_parts, self.VariableBoundary)

        DofToVertex = dof_to_vertex_map(SpaceS) #maps to local/processor dofs
        for i in range(dof_min, dof_max):
            VertexID = DofToVertex[i-dof_min]
            Vert = MeshEntity(gamma, 0, VertexID)
            GlobalIndex = Vert.global_index()
            if MyCorrectBoundary[GlobalIndex] > 0.0:
                GradValue = SGrad.vector()[i]
                factor = 0.0
                for j in range(0, MaxDim):
                    LocN = N.vector()[MaxDim*i+j]
                    LocV = V.vector()[MaxDim*i+j]
                    factor = factor + StepLength*LocN*LocV
                for j in range(0, MaxDim):
                    LocV = V.vector()[MaxDim*i+j]
                    LocValues[MaxDim*(i-dof_min)+j] = factor*GradValue*LocV
        SGradV.vector().set_local(LocValues)
        SGradV.vector().apply('')
        return SGradV

    def AddVectors(self, V1, V2, f1=1.0, f2=1.0):
        FunctionSpace = V1.function_space()
        Sum = Function(FunctionSpace)
        
        aa = Sum.vector().local_size()
        (a,b) = Sum.vector().local_range()
        #self.dprint("aa %d"%(aa))
        #self.dprint("a %d, b %d, b-a %d"%(a,b,b-a))
        
        #Sanity check:
        (a1,b1) = V1.vector().local_range()
        (a2,b2) = V2.vector().local_range()
        if not (a == a1 and a1 == a2 and b == b1 and b1 == b2):
            self.dprint("Error using AddVectors. Data locality no equal")
            self.dprint("a %d, b %d"%(a,b))
            self.dprint("a1 %d, b1 %d, a2 %d, b2 %d"%(a1, b1, a2, b2))
            exit()
        LocValues = numpy.zeros(aa)
        #Modification for Dolfin 1.5 (with Ghost Layers)
        for i in range(aa):
        #for i in range(a,b):    
            LocValues[i] = f1*V1.vector()[i]+f2*V2.vector()[i]
            #LocValues[i-a] = f1*V1.vector()[i]+f2*V2.vector()[i]
        Sum.vector().set_local(LocValues)
        Sum.vector().apply("")
        Sum.rename("Sum", "Dummy")
        return Sum

    #set a vector component in parallel
    def SetVectorEntry(self, V, MyIndex, MyValue):
        aa = V.vector().local_size()
        (a,b) = V.vector().local_range()
        self.dprint(aa)
        self.dprint(a)
        self.dprint(b)
        exit()

    def RescaleVector(self, MyScalar, MyFunction, MyVector):
        MaxDim = MyVector.function_space().element().value_dimension(0)
        if MyFunction.vector().local_size()*MaxDim != MyVector.vector().local_size():
            self.dprint("ERROR: RescaleVector: SIZE MISMATCH")
            exit(1)
        MySize = MyFunction.vector().local_size()
        LocValues = numpy.zeros(MaxDim*MySize)
        for i in range(MySize):
            value = MyScalar*MyFunction.vector()[i]
            for j in range(MaxDim):
                LocValues[MaxDim*i+j] = value*MyVector.vector()[MaxDim*i+j]
        MyMesh = MyVector.function_space().mesh()
        MyVector2 = Function(VectorFunctionSpace(MyMesh, "CG", 1, MaxDim))
        MyVector2.vector().set_local(LocValues)
        MyVector2.vector().apply("")
        MyVector2.rename(MyVector.name(), "dummy")
        return MyVector2

    #OLD VERSION!
    #def MultiplySurfaceWithV(self, SGrad, StepLength, V):
    #    gamma = BoundaryMesh(self.mesh, "exterior")
    #    #NN = VectorFunctionSpace(gamma, 'CG', self.MaxOrder)
    #    #V = interpolate(V, NN)
    #    #Multiply with V
    #    SGradV = StepLength*SGrad*V
    #    SGradSpaceV = VectorFunctionSpace(gamma, "CG", self.MaxOrder)
    #    SGradV = project(SGradV, SGradSpaceV)
    #    return SGradV
    
    def ReduceMeshFunctionToSurface(self, mesh, boundary_parts):
        mesh.init()
        MaxDim = mesh.topology().dim()
        VariableSurface = BoundaryMesh(mesh, "exterior")
        mapa = VariableSurface.entity_map(MaxDim-1)
        VariableBoundary_parts = MeshFunction('size_t', VariableSurface, MaxDim-1)
        #VariableBoundary_parts = CellFunction('size_t', VariableSurface)
        for ver in entities(VariableSurface, MaxDim-1):
            i = mapa[ver.index()]
            VariableBoundary_parts[ver.index()] = boundary_parts[i]
        return VariableBoundary_parts

    def ReduceToSurface(self, VolumeGradient, SpaceType = "CG", SpaceOrder = 1):
        MaxDim = 1#VolumeGradient.function_space().dofmap().global_dimension()
        VolSpace = VolumeGradient.function_space()
        mesh = VolSpace.mesh()
        mesh.init()
        mesh.order()
        gamma = BoundaryMesh(mesh, "exterior")
        gamma.init()
        gamma.order()
        SurfSpace = FunctionSpace(gamma, SpaceType, SpaceOrder)
        SurfGrad = Function(SurfSpace)
        (sdmin, sdmax) = SurfGrad.vector().local_range()
        sdmin = int(sdmin/MaxDim)
        sdmax = int(sdmax/MaxDim)
        LocValues = numpy.zeros(MaxDim*(sdmax-sdmin))
        if SpaceType == "CG":
            VGlobal = numpy.zeros(len(VolumeGradient.vector()))
            (vdmin, vdmax) = VolumeGradient.vector().local_range()
            DofToVert = dof_to_vertex_map(VolSpace)
            for i in range(vdmax-vdmin):
                Vert = MeshEntity(VolumeGradient.function_space().mesh(), 0, DofToVert[i])
                GlobalIndex = Vert.global_index()
                value = VolumeGradient.vector()[i+vdmin]
                VGlobal[GlobalIndex] = value
            VGlobal = self.SyncSum(VGlobal)
            mapa = gamma.entity_map(0)
            DofToVert = dof_to_vertex_map(FunctionSpace(gamma, SpaceType, SpaceOrder))
            for i in range(sdmax-sdmin):
                VolVert = MeshEntity(mesh, 0, mapa[int(DofToVert[i])])
                GlobalIndex = VolVert.global_index()
                for j in range(MaxDim):
                    value = VGlobal[MaxDim*GlobalIndex+j]
                    LocValues[MaxDim*i+j] = value
        elif SpaceType == "DG":
            map = gamma.entity_map(2)
            if SpaceOrder == 0:
                for f in entities(gamma, 2):
                    f_vol = MeshEntity(mesh, 2, map[f.index()])
                    #Because we are looping physical boundary facets, there can be only one
                    #volume element attached to this boundary facet:
                    e_vol = entities(f_vol, 3).next()
                    CellDofsSurf = SurfSpace.dofmap().cell_dofs(f.index())
                    CellDofsVol = VolSpace.dofmap().cell_dofs(e_vol.index())
                    LocValues[CellDofsSurf[0]] = VolumeGradient.vector()[CellDofsVol[0]]
            elif SpaceOrder == 1:
                TestSurf = self.MyProject(self.TestExpression, SurfSpace, "lu")
                TestVol = self.MyProject(self.TestExpression, VolSpace, "lu")
                #Error1 = errornorm(self.TestExpression, TestSurf)
                #Error2 = errornorm(self.TestExpression, TestVol)
                #print "Test Function Error norms:", Error1, Error2
                for f in entities(gamma, 2):
                    f_vol = MeshEntity(mesh, 2, map[f.index()])
                    #Because we are looping physical boundary facets, there can be only one
                    #volume element attached to this boundary facet:
                    MyNumCells = 0
                    ParCells = cells(f_vol)
                    for e_vol in ParCells:
                        MyNumCells = MyNumCells + 1
                        #for v2 in vertices(e_vol):
                        #    print "v2.x(0)", v2.x(0), "v2.x(1)", v2.x(1), "v2.x(2)", v2.x(2), "value", abs(v2.x(0)) + abs(v2.x(1)) + abs(v2.x(2))
                        CellDofsSurf = SurfSpace.dofmap().cell_dofs(f.index())
                        CellDofsVol = VolSpace.dofmap().cell_dofs(e_vol.index())
                        for i in CellDofsSurf:
                            TValueS = TestSurf.vector()[i]
                            Found = False
                            #MinOffset = 1e10
                            #MinIndex = 1000
                            for j in CellDofsVol:
                                TValueVol = TestVol.vector()[j]
                                offset = abs(TValueVol - TValueS)
                                if offset < self.TestTol:
                                    Found = True
                                    LocValues[i] = VolumeGradient.vector()[j]
                                    break
                                #if offset < MinOffset:
                                #    MinOffset = offset
                                #    MinIndex = j
                            if Found == False:
                                print "WARNING: ReduceToSurfaceDG: DOF NOT FOUND"
                                print "i:",i,"TValueS =", TValueS
                                for j in CellDofsVol:
                                    TValueVol = TestVol.vector()[j]
                                    print "j:", j, "TValueVol", TValueVol, "diff:", abs(TValueVol - TValueS)
                                        #LocValues[i] = VolumeGradient.vector()[MinIndex]
                                        #print "Using closet index", MinIndex
                                    for v1 in vertices(f):
                                        print "v1.x(0)", v1.x(0), "v1.x(1)", v1.x(1), "v1.x(2)", v1.x(2), "value", abs(v1.x(0)) + abs(v1.x(1)) + abs(v1.x(2))
                                    for v2 in vertices(e_vol):
                                        print "v2.x(0)", v2.x(0), "v2.x(1)", v2.x(1), "v2.x(2)", v2.x(2), "value", abs(v2.x(0)) + abs(v2.x(1)) + abs(v2.x(2))
                                #FDebug = File(self.WorkDirectory+"/Debug.pvd", "compressed")
                                #TestSurf.rename("TestExpression", "dummy")
                                #TestVol.rename("TestExpression", "dummy")
                                #FDebug << (TestSurf, 0.0)
                                #FDebug << (TestVol, 1.0)
                                exit(1)
                    if MyNumCells != 1:
                        print "ERROR: Number of Parent Cells not as expected:", MyNumCells
                        exit(1)
            elif SpaceOrder > 1:
                print "ERROR: ReduceToSurfaceDG: WORKS ONLY FOR SpaceOrder <= 1"
                exit()
        SurfGrad.vector().set_local(LocValues)
        SurfGrad.vector().apply('')
        SurfGrad.rename("Surface", "dummy")
        return SurfGrad
    
    def ReduceVectorToSurface(self, VolumeGradient, SpaceType="CG", SpaceOrder=1):
        mesh = VolumeGradient.function_space().mesh()
        MaxDim = mesh.topology().dim()
        gamma = BoundaryMesh(mesh, "exterior")
        SGradSpace = VectorFunctionSpace(gamma, SpaceType, SpaceOrder, MaxDim)
        SGrad = Function(SGradSpace)
        (sdmin, sdmax) = SGrad.vector().local_range()
        sdmin = int(sdmin/MaxDim)
        sdmax = int(sdmax/MaxDim)
        LocValues = numpy.zeros(MaxDim*(sdmax-sdmin))
        if SpaceType == "CG":
            VGlobal = numpy.zeros(len(VolumeGradient.vector()))
            (vdmin, vdmax) = VolumeGradient.vector().local_range()
            vdmin = int(vdmin/MaxDim)
            vdmax = int(vdmax/MaxDim)
            DofToVert = dof_to_vertex_map(FunctionSpace(mesh, "CG", 1))
            for i in range(vdmax-vdmin):
                Vert = MeshEntity(mesh, 0, DofToVert[i])
                GlobalIndex = Vert.global_index()
                for j in range(MaxDim):
                    value = VolumeGradient.vector()[MaxDim*(i+vdmin)+j]
                    VGlobal[MaxDim*GlobalIndex+j] = value
            VGlobal = self.SyncSum(VGlobal)
            mapa = gamma.entity_map(0)
            DofToVert = dof_to_vertex_map(FunctionSpace(gamma, SpaceType, SpaceOrder))
            for i in range(sdmax-sdmin):
                VolVert = MeshEntity(mesh, 0, mapa[int(DofToVert[i])])
                GlobalIndex = VolVert.global_index()
                for j in range(MaxDim):
                    value = VGlobal[MaxDim*GlobalIndex+j]
                    LocValues[MaxDim*i+j] = value
        else:
            print "ReduceVectorToSurface: ONLY CG 1 SUPPORTED"
            exit(1)
        SGrad.vector().set_local(LocValues)
        SGrad.vector().apply('')
        return SGrad

    #take a CG1 vector f defined on a surface mesh and return a volume vector with same values on boundary but zero in volume
    def InjectVectorFromSurface(self, f, mesh):
        SpaceOrder = 1
        SpaceType = "CG"
        MaxDim = 3
        #gamma = f.function_space().mesh()
        gamma = BoundaryMesh(mesh, "exterior")
        SpaceS = FunctionSpace(mesh, SpaceType, SpaceOrder)
        SpaceV = VectorFunctionSpace(mesh, SpaceType, SpaceOrder, MaxDim)
        F = Function(SpaceV)
        LocValues = numpy.zeros(F.vector().local_size())
        map = gamma.entity_map(0)
        d2v = dof_to_vertex_map(FunctionSpace(gamma, SpaceType, SpaceOrder))
        v2d = vertex_to_dof_map(SpaceS)
        for i in range(f.vector().local_size()/MaxDim):
            GVertID = Vertex(gamma, d2v[i]).index()
            PVertID = map[GVertID]
            PDof = v2d[PVertID]
            for j in range(MaxDim):
                value = f.vector()[MaxDim*i+j]
                LocValues[PDof*MaxDim+j] = value
        F.vector().set_local(LocValues)
        F.vector().apply("")
        return F
    
    def CalculateScalarL2NormBoundary(self, SurGrad, mesh, boundary_parts):
        (VariableSurface, VariableBoundary_parts) = self.ExtractSubsurface(self.VariableBoundary, mesh, boundary_parts)
        SmoothSpace = FunctionSpace(VariableSurface, "CG", self.MaxOrder)

        SurGrad = interpolate(SurGrad, SmoothSpace)

        a = (SurGrad*SurGrad)*dx
        a = assemble(a)
        return sqrt(a)

    def CalculateVectorL2NormBoundary(self, SGV, mesh, boundary_parts):
        (VariableSurface, VariableBoundary_parts) = self.ExtractSubsurface(self.VariableBoundary, mesh, boundary_parts)
        SmoothSpace = VectorFunctionSpace(VariableSurface, "CG", self.MaxOrder)

        SGV = interpolate(SGV, SmoothSpace)
        a = (SGV[0]*SGV[0]+SGV[1]*SGV[1]+SGV[2]*SGV[2])*dx
        a = assemble(a)
        return sqrt(a)

    def CalculateArea(self, mesh, Marker=[], boundary_parts=[]):
        Area = 0.0
        temp = project(Constant(1.0), FunctionSpace(mesh, 'CG', 1))
        if Marker == []:
            Area = assemble(temp*ds)
        else:
            dss = ds(domain=mesh, subdomain_data=boundary_parts)
            for i in Marker:
                Area += assemble(temp*dss(i))
        return Area
    
    def CalculateVolume(self, mesh):
        Volume = project(Constant(1.0), FunctionSpace(mesh, "CG", 1))*dx
        return assemble(Volume)
    
    def CalculateCentroid(self, mesh, Marker):
        MaxDim = mesh.topology().dim()
        dss = Measure("ds")[self.boundary_parts]
        Vol = self.CalculateArea(mesh, self.VariableBoundary)
        self.pprint("Vol = %e"%Vol)
        Space = FunctionSpace(mesh, "CG", 1)
        X = []
        for j in range(MaxDim):
            x = project(Expression("x[j]", j=j, degree=ExprDegree), Space)
            c = (1.0/Vol)*assemble(x*dss(Marker))
            X.append(c)
        return X
    
    def BisectVolume(self, mesh, TargetVolume, TargetCentroid = [], InitField = [], Quadrants = [], tol = 2e-3, MinDist = 1.25e-2):
        gamma = BoundaryMesh(mesh, "exterior")
        N = self.MakeV(mesh, gamma)
        ll = 0.0
        lr = 0.5
        eps = 1e-5
        alpha = 0.0
        SpaceV = VectorFunctionSpace(gamma, "CG", 1)
        while lr - ll > eps:
            alpha = 0.5*(ll+lr)
            MeshCopy = Mesh(mesh)
            GammaCopy = BoundaryMesh(MeshCopy, "exterior")
            if InitField != []:
                Field = project(InitField + Constant(-alpha)*N, SpaceV)
            else:
                Field = project(Constant(-alpha)*N, SpaceV)
            if Quadrants != []:
                Field = project(Field + self.FixQuadrant(gamma, mesh.topology().dim(), Quadrants, tol, MinDist, InitField), SpaceV)
            GammaCopy.move(Field)
            MeshCopy.move(GammaCopy)
            Vol = self.CalculateVolume(MeshCopy)
            if Vol > TargetVolume: ll = alpha
            elif Vol < TargetVolume: lr = alpha
        if TargetCentroid != []:
            MaxDim = mesh.topology().dim()
            Centroid = self.CalculateCentroid(mesh, self.VariableBoundary)
            CDiff = [0.0]*MaxDim
            for j in range(MaxDim):
                CDiff[j] = Centroid[j] - TargetCentroid[j]
            self.pprint("Centroid offset: %s"%CDiff)
            if MaxDim == 2:
                CDiff = project(Expression(("a", "b"), a = CDiff[0], b=CDiff[1], element=SpaceV.ufl_element()), SpaceV)
            if MaxDim == 3:
                CDiff = project(Expression(("a", "b", "c"), a = CDiff[0], b=CDiff[1], c=CDiff[2], element=SpaceV.ufl_element()), SpaceV)
            CDiff = self.InjectOnSubSurface(CDiff, mesh, self.VariableBoundary)
            Field = project(Field - CDiff, SpaceV)
        return Field
    
    def FixQuadrant(self, mesh, MaxDim, Quadrants, tol = 2e-3, MinDist = 1.25e-2, InitField = []):
        SpaceV = VectorFunctionSpace(mesh, "CG", 1, MaxDim)
        SpaceS = FunctionSpace(mesh, "CG", 1)
        Field = Function(SpaceV)
        (dmin, dmax) = Field.vector().local_range()
        dmin = int(dmin/MaxDim)
        dmax = int(dmax/MaxDim)
        LocValues = numpy.zeros(MaxDim*(dmax-dmin))
        DtoV = dof_to_vertex_map(SpaceS)
        for i in range(dmin, dmax):
            v = Vertex(mesh, DtoV[i-dmin])
            for k in Quadrants:
                x_k = v.x(k)
                #tolerace when to accept a point that is on the plane anyway
                #and should stay there...
                if abs(x_k) > tol:
                    if InitField != []:
                        x_knew = x_k + InitField.vector()[MaxDim*i + k]
                    else:
                        x_knew = x_k
                    #check if x_knew is too close to the boundary or has transgressed already:
                    TooFar = 0.0
                    if (abs(x_knew) < MinDist):
                        TooFar = MinDist - abs(x_knew)
                    elif (math.copysign(1.0, x_knew) != math.copysign(1.0, x_k)):
                        TooFar = abs(x_knew) + MinDist
                    LocValues[MaxDim*(i-dmin) + k] = math.copysign(TooFar, x_k)
                #force point perfectly onto symmetry
                else:
                    LocValues[MaxDim*(i-dmin) + k] = -1.0*x_k
        Field.vector().set_local(LocValues)
        Field.vector().apply("")
        return Field
    
    def FixSymmetryPlane(self, boundary_parts, VectorFunction, XIDs = [], YIDs = [], ZIDs = []):
        Space = VectorFunction.function_space()
        for i in XIDs:
            BC = DirichletBC(Space.sub(0), Constant(0.0), boundary_parts, i)
            BC.apply(VectorFunction.vector())
        for i in YIDs:
            BC = DirichletBC(Space.sub(1), Constant(0.0), boundary_parts, i)
            BC.apply(VectorFunction.vector())
        for i in ZIDs:
            BC = DirichletBC(Space.sub(2), Constant(0.0), boundary_parts, i)
            BC.apply(VectorFunction.vector())
        return VectorFunction

    def BLaplaceSmooth(self, SurGrad, boundary_parts, epsilon, NumSmooth, closed=False):
        mesh = SurGrad.function_space().mesh()
        (VariableSurface, VariableBoundary_parts) = self.ExtractSubsurface(self.VariableBoundary, mesh, boundary_parts)
        SmoothSpace = FunctionSpace(VariableSurface, "CG", 1)
        
        #u_old = interpolate(SurGrad, SmoothSpace)
        gamma = BoundaryMesh(mesh, "exterior")
        u_old = self.ReduceFunctionToSubsurface(SurGrad, gamma, VariableSurface, "CG", 1)
        
        v = TestFunction(SmoothSpace)
        w = TrialFunction(SmoothSpace)
        
        a = (epsilon*inner(grad(v), grad(w)) + v*w)*dx
        A = assemble(a)
        l = v*u_old*dx
        
        u_0 = Constant(0.0)
        bc = DirichletBC(SmoothSpace, u_0, "on_boundary")
        
        u_next = Function(SmoothSpace)
        for i in range(0, NumSmooth):
            self.pprint("Smoothing: %d"%i)
            b = assemble(l)
            if closed == False:
                bc.apply(A,b)
            solve(A, u_next.vector(), b)
            u_old.assign(u_next)
        u_old = self.ExpandFunctionFromSubsurface(u_old, gamma, VariableSurface, "CG", 1)
        return u_old

    def BLaplaceSmoothPeriodic(self, SurGrad, epsilon, NumSmooth):
        class PeriodicBoundary(SubDomain):
            def inside(self, x, on_boundary):
                #return true if on top or right
                return bool((near(x[1], 0.0) and x[2] > 0.0) or (near(x[2], 0.0) and x[1] > 0.0))
            def map(self, x, y):
                if near(x[1], 0.0):
                    y[0] = x[0]
                    y[1] = x[1]
                    y[2] = -x[2]
                elif near(x[2], 0.0):
                    y[0] = x[0]
                    y[1] = -x[1]
                    y[2] = x[2]
                #not clear why this is needed but dolfin crashes unless you assign all points.
                #result needs to be outside the boundary...
                else:
                    y[0] = -10000
                    y[1] = -10000
                    y[2] = -10000

        (VariableSurface, VariableBoundary_parts) = self.ExtractSubsurface(self.VariableBoundary)
        BoundarySpace = FunctionSpace(VariableSurface, "CG", self.MaxOrder)
        SmoothSpace = FunctionSpace(VariableSurface, "CG", self.MaxOrder, constrained_domain=PeriodicBoundary())

        u_old = interpolate(SurGrad, BoundarySpace)

        v = TestFunction(SmoothSpace)
        w = TrialFunction(SmoothSpace)

        a = (epsilon*inner(grad(v), grad(w)) + v*w)*dx
        A = assemble(a)
        l = v*u_old*dx

        u_0 = Constant(0.0)
        bc = DirichletBC(SmoothSpace, u_0, "on_boundary")

        u_next = Function(SmoothSpace)
        for i in range(0, NumSmooth):
            print "Smoothing:", i
            b = assemble(l)
            bc.apply(A,b)
            solve(A, u_next.vector(), b)
            u_old.assign(u_next)

        return u_old

    def MakeV_old(mesh, boundary_parts):
        gamma = BoundaryMesh(mesh, "exterior")
        #boundary_parts=boundary_parts
        dss=ds[boundary_parts]
        N = VectorFunctionSpace(mesh, 'CG', 1)
        n = FacetNormal(mesh)
        v = TestFunctions(N)
        w = TrialFunctions(N)
        a = (inner(v[0],w[0]) + inner(v[1],w[1]) + inner(v[2],w[2]))*dx
        a += (inner(v[0],w[0]) + inner(v[1],w[1]) + inner(v[2],w[2]))*dss
        l = (inner(n[0],v[0]) + inner(n[1],v[1]) + inner(n[2],v[2]))*(dss(1)+dss(4))
        #l+= 2.0*(inner(n[0],v[0]) + inner(n[1],v[1]) + inner(0.0,v[2]))*dss(5)
        #l+= 2.0*(inner(n[0],v[0]) + inner(n[1],v[1]) + inner(n[2],v[2]))*dss(5)
        #A = assemble(a, exterior_facet_domains=boundary_parts)
        #b = assemble(l, exterior_facet_domains=boundary_parts)
        V = Function(N)

        def Boundary1(x):
            return abs(x[2]) < 1e-8#DOLFIN_EPS
        def Boundary2(x):
            return abs(x[1]) < 1e-8#DOLFIN_EPS

        B1  = DirichletBC(N.sub(2), 0.0, Boundary1)
        B2  = DirichletBC(N.sub(1), 0.0, Boundary2)
        #B1 = DirichletBC(N, (0.0,0.0,0.0), Boundary1)
        #B2 = DirichletBC(N, (0.0,0.0,0.0), Boundary2)
        #solve(A, V.vector(), b)
        solve(a==l, V)

        #combined_subdomains = boundary_parts
        #for ver in entities(mesh, 2):
        #  if boundary_parts[ver.index()] == 1 or boundary_parts[ver.index()] == 4:
        #   combined_subdomains[ver.index()] = 500
        #(surf14, BParts14) = ExtractSubsurface(mesh, combined_subdomains, 500)
        NN = VectorFunctionSpace(gamma, 'CG', 1)
        V = interpolate(V, NN)
        #normalize(V.vector())

        #normalize
        #V2 = Function(N)
        #for ver in entities(mesh, 0):
            ##print 'test'
            ##i=mapb[ver.index()]
            #i = ver.index()
            #nx = V.vector()[i]
            #ny = V.vector()[i+mesh.num_entities(0)]
            #nz = V.vector()[i+2*mesh.num_entities(0)]
            #length = sqrt(nx*nx + ny*ny + nz*nz)
            #l1 = length
            #if length > 0.5:
                #l1 = 1.0/length
            #else:
                #l1 = 0.0
            ##l1 = 1.0
            #V2.vector()[i] = l1*V.vector()[i]
            #V2.vector()[i+mesh.num_entities(0)] = l1*V.vector()[i+mesh.num_entities(0)]
            #V2.vector()[i+2*mesh.num_entities(0)] = l1*V.vector()[i+2*mesh.num_entities(0)]


        #NN = VectorFunctionSpace(gamma, 'CG', 1)
        #V = interpolate(V, NN)
        #V = interpolate(V, NN)
        #V = interpolate(V2, NN)
        #V = compute_normal_field(mesh, boundary_parts)
        #FTest = File('./output/V_test.pvd')
        #FTest<<(V,0.0)
        #mesh.move(project(V, N))

    #bildet den durchschnitt aller normalen
    def vertex_normal(mesh, boundary_parts, i, label):
        ver = Vertex(mesh, i)
        n = Point(0, 0, 0)
        div = 0.0
        for fac in entities(ver, 2):
            f = Facet(mesh, fac.index())
            if f.exterior()==True:
                n+=f.normal()
                div+=1
        if div !=0:
            n/=div
        return n

    #omega=mesh
    #mf =boundary parts marker
    #Computes Vertex Normals
    def compute_normal_field(omega, mf):
        print 'Test from compute normal field'
        #Boundary Mesh
        gamma = BoundaryMesh(omega, "exterior")
        mapa = gamma.entity_map(0)


        V = FunctionSpace(gamma, "CG", 1)
        N = VectorFunctionSpace(gamma, "CG", 1)
        normal_field = Function(N)

        normal_x = Function(V)
        normal_y = Function(V)
        normal_z = Function(V)

        for ver in entities(gamma, 0):
            i = mapa[ver.index()]
            point = vertex_normal(omega, mf, i, ([1]))
            normal_x.vector()[ver.index()] = -1.0*point[0]
            normal_y.vector()[ver.index()] = -1.0*point[1]
            normal_z.vector()[ver.index()] = -1.0*point[2]

        (x, y, z) = TestFunctions(N)
        solve(inner(normal_field[0], x)*dx + inner(normal_field[1], y)*dx + inner(normal_field[2], z)*dx -\
        inner(normal_x, x)*dx -inner(normal_y, y)*dx -inner(normal_z, z)*dx == 0, normal_field)


        #Dirichlet Boundary must be initialized by Function in Vector Space over Omega (only on Gamma fails)
        V_vec = VectorFunctionSpace(omega, "CG", 1)
        normal_fieldV = Function(V_vec)
        for ver in entities(gamma, 0):
            i = mapa[ver.index()]
            normal_fieldV.vector()[i] = normal_field.vector()[ver.index()]
            normal_fieldV.vector()[i+omega.num_vertices()] = normal_field.vector()[ver.index()+gamma.num_vertices()]
            normal_fieldV.vector()[i+2*omega.num_vertices()] = normal_field.vector()[ver.index()+2*gamma.num_vertices()]
            #0 im Gebiet
            #am Rand 3D Normalenkomponenten

        deform= project(normal_fieldV,V_vec)
        return deform

        """
        deform = TrialFunction(V_vec)
        v = TestFunction(V_vec)
        a = 0.01*inner(nabla_grad(deform), nabla_grad(v))*dx + inner(deform,v)*ds(4)
        L = inner(Constant((0.0,0.0,0.0)),v)*dx
        bc1 = DirichletBC(V_vec, normal_fieldV, mf, 4)
        bc2 = DirichletBC(V_vec, Constant((0,0,0)), mf, 2)
        bc3 = DirichletBC(V_vec, Constant((0,0,0)), mf, 3)
        bc4 = DirichletBC(V_vec, Constant((0,0,0)), mf, 5)
        bc = [bc1, bc2, bc3, bc4]
        deform = Function(V_vec)
        solve(a==L, deform, bcs=bc)


        return deform
        """

    def compute_subsurface_vertex_normal(omega, boundary_parts, marker,nbb):
        #Extract Subsurface with ID marker
        gamma = BoundaryMesh(omega, "exterior")
        mapa = gamma.entity_map(2)
        boundaryboundary_parts=MeshFunction('size_t',gamma,2)
        for ver in entities(gamma, 2):
            i = mapa[ver.index()]
            boundaryboundary_parts[ver.index()] = boundary_parts[i]
        gammagamma = SubMesh(gamma, boundaryboundary_parts, marker)

        #Construct face normales for all boundaries
        NB = VectorFunctionSpace(gamma, 'DG', 0, 3)
        nb = Function(NB)
        NumFacets = gamma.num_entities(2)
        for ver in entities(gamma,2):
            k = mapa[ver.index()]
            fac = Facet(omega, k)
            n_fac = fac.normal()
            nb.vector()[ver.index()] = -1.0*n_fac[0]
            nb.vector()[ver.index()+NumFacets] = -1.0*n_fac[1]
            nb.vector()[ver.index()+2*NumFacets] = -1.0*n_fac[2]

        #Reduce face normals from all boundaries to boundary labeled "marker"
        NBB = VectorFunctionSpace(gammagamma, 'DG', 0, 3)
        mapb = gammagamma.data().mesh_function("parent_cell_indices")
        #nbb = Function(NBB)

        for ver in entities(gammagamma, 2):
            i=mapb[ver.index()]
            nbb.vector()[ver.index()] = nb.vector()[i]
            nbb.vector()[ver.index()+gammagamma.num_entities(2)] = nb.vector()[i+gamma.num_entities(2)]
            nbb.vector()[ver.index()+2*gammagamma.num_entities(2)] = nb.vector()[i+2*gamma.num_entities(2)]
        return nbb

    def ComputeVolume(self, mesh):
        dxx = dx(domain=mesh)
        vol = assemble(Constant(1.0)*dxx)
        return vol
    
    def ComputeBarycenter(self, mesh, subdomain=None):
        MaxDim = mesh.topology().dim()
        dxx = dx(domain=mesh)
        Barycenter = MaxDim*[0.0]
        vol = self.ComputeVolume(mesh)
        for i in range(MaxDim):
            MyBC = 1.0/vol*assemble(Expression("x[%d]"%i, degree=2)*dxx(subdomain))
            Barycenter[i] = MyBC
        return Barycenter
    
    def SD_Bary(self, mesh, V, N):
        MaxDim = mesh.topology().dim()
        #dss = ds(domain=mesh)
        vol = self.ComputeVolume(mesh)
        #print "SD_BARY: VOL", vol
        Bary = self.ComputeBarycenter(mesh)
        #print "SD_Bary: Bary:", Bary[0], Bary[1]
        SDB = MaxDim*[0.0]
        for k in range(MaxDim):
            MyXi = Expression("x[%d]"%k, degree=2)
            MyXi.rename("x_%d"%k, "")
            volb1 = Constant(1.0/vol)
            volb1.rename("1/vol", "")
            Baryk = Constant(Bary[k])
            Baryk.rename("Bary_%d"%k, "")
            SDB[k] = volb1*dot(V,N)*(MyXi - Baryk)
        return SDB

    def SplitVectorFunction(h, omega, degree):
        H = VectorFunctionSpace(omega, 'DG', degree)
        H0 = FunctionSpace(omega, 'DG', degree)
        h0 = Function(H0)
        h1 = Function(H0)
        h2 = Function(H0)
        x = TestFunction(H0)
        solve(inner(h[0], x)*dx - inner(h0, x)*dx == 0, h0)
        h0 = project(h0, H0)
        solve(inner(h[1], x)*dx - inner(h1, x)*dx == 0, h1)
        h1 = project(h1, H0)
        solve(inner(h[2], x)*dx - inner(h2, x)*dx == 0, h2)
        h2 = project(h2, H0)
        return h0, h1, h2

    def CombineVectorFunction(h0, h1, h2, omega, degree):
        N = VectorFunctionSpace(omega, "DG", degree)
        H = Function(N)
        (x, y, z) = TestFunctions(N)
        solve(inner(H[0], x)*dx + inner(H[1], y)*dx + inner(H[2], z)*dx -\
        inner(h0, x)*dx -inner(h1, y)*dx -inner(h2, z)*dx == 0, H)
        return H
    
    def ExtractSubsurface_old(self, marker, mesh, boundary_parts):
        gamma = BoundaryMesh(mesh, "exterior")
        mapa = gamma.entity_map(2)
        boundaryboundary_parts=MeshFunction('size_t',gamma,2)
        for ver in entities(gamma, 2):
            i = mapa[ver.index()]
            if boundary_parts[i] in marker:
                boundaryboundary_parts[ver.index()] = 1
            else:
                boundaryboundary_parts[ver.index()] = 0
        gammagamma = SubMesh(gamma, boundaryboundary_parts, 1)
        return (gammagamma, boundaryboundary_parts)
    
    #Extract Subsurface with ID marker and return global Surface Cell (= facet) to source global volume cell (= element)
    def ExtractSubsurface(self, marker, mesh, boundary_parts):
        mesh.init()
        mesh.order()
        code="""
            #include <dolfin/mesh/MeshPartitioning.h>
            
            namespace dolfin {
            void build_distributed_mesh(std::shared_ptr<Mesh> mesh)
            {
            MeshPartitioning::build_distributed_mesh(*mesh);
            }
            };
            """
        build_distributed_mesh = compile_extension_module(code).build_distributed_mesh
        
        MaxDim = mesh.topology().dim()
        GBP = self.GlobalizeMeshFunction(boundary_parts)
        #combined_subdomains = MeshFunction('size_t', gamma, MaxDim-1)
        #for i in marker:
        #    combined_subdomains.array()[boundary_parts.array()==i] = 1
        #plot(combined_subdomains, interactive=True)
        NumVertexGlobal = mesh.size_global(0)
        #NumCellsGlobal = mesh.size_global(MaxDim)
        coords = mesh.coordinates()
        CoordsGlobal = numpy.zeros([NumVertexGlobal, MaxDim])
        NumProcGlobal = numpy.zeros(NumVertexGlobal)
        for v in vertices(mesh):
            x = numpy.array(coords[v.index()])
            CoordsGlobal[v.global_index(),:] = x
            NumProcGlobal[v.global_index()] = 1.0
        self.SyncSum(CoordsGlobal)
        self.SyncSum(NumProcGlobal)
        FacetsOnBoundary = numpy.zeros(1, dtype=numpy.uintp)
        for f in facets(mesh):
            if f.is_ghost() == False:
                if MPI.size(mpi_comm_world()) == 1:
                    ID = f.index()
                else:
                    ID = f.global_index()
                if GBP[ID] in marker:
                    FacetsOnBoundary[0] += 1
        FacetsOnBoundary = self.SyncAllGather(FacetsOnBoundary)
        NumFacets = numpy.sum(numpy.sum(FacetsOnBoundary))
        MyStartIndex = 0
        for i in range(MPI.rank(mpi_comm_world())):
            MyStartIndex += FacetsOnBoundary[0,i]
        #Array [Global Facet ID, global Vertices full]
        BVertexID = numpy.zeros([NumFacets, MaxDim], dtype=numpy.uintp)
        #The return map global Surface Facet to global Volume Cell
        SFacet2VCell = numpy.zeros(NumFacets, dtype=numpy.uintp)
        SFacetNormal = numpy.zeros([NumFacets, MaxDim])
        #For new boundary_markers
        SFacetBP = numpy.zeros(NumFacets, numpy.uintp)
        MyAdded = 0
        MyStartIndex = int(MyStartIndex)
        MyAdded = int(MyAdded)
        for f in facets(mesh):
            if MPI.size(mpi_comm_world()) == 1:
                ID = f.index()
            else:
                ID = f.global_index()
            if GBP[ID] in marker and f.is_ghost() == False:
                #For new boundary_markers
                SFacetBP[MyStartIndex + MyAdded] = GBP[ID]
                VID = 0
                for v in vertices(f):
                    BVertexID[MyStartIndex + MyAdded, VID] = v.global_index()
                    VID += 1
                #Only for the SFacet2VCell Map:
                for j in range(MaxDim):
                    SFacetNormal[MyStartIndex + MyAdded, j] = f.normal(j)
                for c in cells(f):
                    SFacet2VCell[MyStartIndex + MyAdded] = c.global_index()
                    break
                MyAdded += 1
        self.SyncSum(BVertexID)
        self.SyncSum(SFacet2VCell)
        self.SyncSum(SFacetBP)
        #renumber boundary vertices consistently
        TMP = numpy.zeros(mesh.size_global(0), dtype=numpy.uintp)
        for i in range(NumFacets):
            for j in range(MaxDim):
                GID = BVertexID[i,j]
                TMP[GID] = 1
        BVertex2VVertex = numpy.zeros(numpy.sum(TMP), dtype=numpy.uintp)
        VVertex2BVertex = numpy.zeros(mesh.size_global(0), dtype=numpy.uintp)
        NumBVertices = 0
        for i in range(len(TMP)):
            if TMP[i] > 0:
                BVertex2VVertex[NumBVertices] = i
                VVertex2BVertex[i] = NumBVertices
                NumBVertices += 1
        MyBoundaryMesh = Mesh()
        editor = MeshEditor()
        editor.open(MyBoundaryMesh, MaxDim-1, MaxDim)
        editor.init_vertices_global(NumBVertices, NumBVertices)
        editor.init_cells_global(NumFacets, NumFacets)
        #Parallel bug in FEniCS 1.7dev:
        #BEGIN SERIAL PART: Build mesh on node 0 only!
        if MPI.rank(mpi_comm_world()) == 0:
            for i in range(len(BVertex2VVertex)):
                j = BVertex2VVertex[i]
                x = CoordsGlobal[j,:]/NumProcGlobal[j]
                editor.add_vertex_global(i, i, x)
            for i in range(len(BVertexID)):
                VertIndex = VVertex2BVertex[BVertexID[i, :]]
                #print "Adding Cell", i, VertIndex, BVertexID[i, :]
                editor.add_cell(i, VertIndex)
        editor.close()
        build_distributed_mesh(MyBoundaryMesh)
        MyBoundaryMesh.init()
        MyBoundaryMesh.order()
        
        #Constract Facet Normal
        self.SyncSum(SFacetNormal)
        NormalSpace = VectorFunctionSpace(MyBoundaryMesh, "DG", 0, MaxDim)
        FacetNormal = Function(NormalSpace)
        #print "size", FacetNormal.vector().local_size()
        LocValues = numpy.zeros(FacetNormal.vector().local_size())
        for c in cells(MyBoundaryMesh):
            GID = c.global_index()
            i = 0
            for dof in NormalSpace.dofmap().cell_dofs(c.index()):
                value = SFacetNormal[GID][i]
                LocValues[dof] = value
                i += 1
        FacetNormal.vector().set_local(LocValues)
        FacetNormal.vector().apply("")
        FacetNormal.rename("Normal", "dummy")

        BBP = MeshFunction("size_t", MyBoundaryMesh, MaxDim-1)
        for f in cells(MyBoundaryMesh):
            BBP.array()[f.index()] = SFacetBP[f.global_index()]
        return MyBoundaryMesh, BBP, FacetNormal, SFacet2VCell

    def ReduceFunctionToSubsurface(self, h, gammagamma, type, degree):
        vertex_indices = gammagamma.data().array("parent_vertex_indices", 0)
        SubSpace = FunctionSpace(gammagamma, type, degree)
        ParSpace = h.function_space()
        hb = Function(SubSpace)
        LocValues = numpy.zeros(hb.vector().local_size())
        if type == "CG":
            d2v = dof_to_vertex_map(SubSpace)
            v2d = vertex_to_dof_map(ParSpace)
            for i in range(hb.vector().local_size()):
                ver = d2v[i]
                SubIndex = Vertex(gammagamma, ver).index()
                ParIndex = vertex_indices[SubIndex]
                ParDof = v2d[ParIndex]
                value = h.vector()[ParDof]
                LocValues[i] = value
        elif type == "DG":
            cell_indices = gammagamma.data().array("parent_cell_indices", 2)
            if degree == 0:
                for f in entities(gammagamma, 2):
                    CIndex = f.index()
                    PIndex = cell_indices[CIndex]
                    CCellDofs = SubSpace.dofmap().cell_dofs(CIndex)
                    PCellDofs = ParSpace.dofmap().cell_dofs(PIndex)
                    LocValues[CCellDofs[0]] = h.vector()[PCellDofs[0]]
            elif degree == 1:
                TestC = interpolate(self.TestExpression, SubSpace)
                TestP = interpolate(self.TestExpression, ParSpace)
                for f in entities(gammagamma, 2):
                    CIndex = f.index()
                    PIndex = cell_indices[CIndex]
                    CCellDofs = SubSpace.dofmap().cell_dofs(CIndex)
                    PCellDofs = ParSpace.dofmap().cell_dofs(PIndex)
                    for i in CCellDofs:
                        CValue = TestC.vector()[i]
                        Found = False
                        for j in PCellDofs:
                            PValue = TestP.vector()[j]
                            if abs(CValue - PValue) < self.TestTol:
                                Found = True
                                LocValues[i] = h.vector()[j]
                                break
                        if Found == False:
                            print "ReduceFunctionToSubsurface DG 1: ERROR: DOF NOT FOUND"
                            exit(1)
            else:
                print "ReduceFunctionToSubsurface: DG DEGREE TOO HIGH!"
                exit(1)
        else:
            print "ReduceFunctionToSubsurface: Unrecognized Type!"
            exit()
        hb.vector().set_local(LocValues)
        hb.vector().apply("")
        #HB = FunctionSpace(gammagamma, type, degree, 3)
        #hb = interpolate(h, HB)
        #hb.name = h.name
        return hb

    def ReduceVectorToSubsurface(self, h, gammagamma, type, degree, dim):
        vertex_indices = gammagamma.data().array("parent_vertex_indices", 0)
        SubSpace = VectorFunctionSpace(gammagamma, type, degree, dim)
        ParSpace = h.function_space()
        mesh = ParSpace.mesh()
        #plot(mesh, interactive=True)
        hb = Function(SubSpace)
        LocValues = numpy.zeros(hb.vector().local_size())
        if type == "CG":
            d2v = dof_to_vertex_map(FunctionSpace(gammagamma, type, degree))
            v2d = vertex_to_dof_map(FunctionSpace(mesh, type, degree))
            for i in range(hb.vector().local_size()/dim):
                ver = d2v[i]
                SubIndex = Vertex(gammagamma, ver).index()
                ParIndex = vertex_indices[SubIndex]
                ParDof = v2d[ParIndex]
                for j in range(dim):
                    value = h.vector()[ParDof*dim+j]
                    LocValues[i*dim+j] = value
        hb.vector().set_local(LocValues)
        hb.vector().apply("")
        #HB = FunctionSpace(gammagamma, type, degree, 3)
        #hb = interpolate(h, HB)
        #hb.name = h.name
        return hb
    
    def ExpandFunctionFromSubsurface(self, hb, omega, gammagamma, type, degree):
        vertex_indices = gammagamma.data().array("parent_vertex_indices", 0)
        ParSpace = FunctionSpace(omega, type, degree)
        SubSpace = hb.function_space()
        d2v = dof_to_vertex_map(SubSpace)
        v2d = vertex_to_dof_map(ParSpace)
        h = Function(ParSpace)
        LocValues = numpy.zeros(h.vector().local_size())
        for i in range(hb.vector().local_size()):
            value = hb.vector()[i]
            ver = d2v[i]
            SubIndex = Vertex(gammagamma, ver).index()
            ParIndex = vertex_indices[SubIndex]
            ParDof = v2d[ParIndex]
            LocValues[ParDof] = value
        h.vector().set_local(LocValues)
        h.vector().apply("")
        #HB = FunctionSpace(gammagamma, type, degree, 3)
        #hb = interpolate(h, HB)
        #hb.name = h.name
        return h

    def ExpandVectorFromSubsurface(self, hb, omega, gammagamma, type, degree, dim):
        vertex_indices = gammagamma.data().array("parent_vertex_indices", 0)
        ParSpace = VectorFunctionSpace(omega, type, degree, dim)
        SubSpace = VectorFunctionSpace(gammagamma, type, degree, dim)
        d2v = dof_to_vertex_map(FunctionSpace(gammagamma, type, degree))
        v2d = vertex_to_dof_map(FunctionSpace(omega, type, degree))
        h = Function(ParSpace)
        LocValues = numpy.zeros(h.vector().local_size())
        for i in range((hb.vector().local_size())/dim):
            ver = d2v[i]
            SubIndex = Vertex(gammagamma, ver).index()
            ParIndex = vertex_indices[SubIndex]
            ParDof = v2d[ParIndex]
            for j in range(dim):
                value = hb.vector()[dim*i+j]
                LocValues[dim*ParDof+j] = value
        h.vector().set_local(LocValues)
        h.vector().apply("")
        #HB = FunctionSpace(gammagamma, type, degree, 3)
        #hb = interpolate(h, HB)
        #hb.name = h.name
        return h

    def ReduceVectorFunctionToSubsurface(self, h, omega, gammagamma, type, degree):
        #Extract Subsurface with ID marker
        #gamma = BoundaryMesh(omega, "exterior")
        #mapa = gamma.entity_map(2)
        #boundaryboundary_parts=MeshFunction('size_t',gamma,2)
        #for ver in entities(gamma, 2):
        #   i = mapa[ver.index()]
        #   boundaryboundary_parts[ver.index()] = boundary_parts[i]
        #gammagamma = SubMesh(gamma, boundaryboundary_parts, marker)
        #print 'REDUCE FUNCTION: DEGREE=', degree
        HB = VectorFunctionSpace(gammagamma, type, degree, 3)
        hb = interpolate(h, HB)
        hb.name = h.name
        #H=VectorFunctionSpace(omega, 'DG', degree,3)
        #psi=TestFunction(HB)
        #solve(inner(h-hb,psi)*self.dss(4) == 0, hb)
        return hb

    def ReduceTensorFunctionToSubsurface(h, omega, boundary_parts, marker, degree):
        #Extract Subsurface with ID marker
        gamma = BoundaryMesh(omega, "exterior")
        mapa = gamma.entity_map(2)
        boundaryboundary_parts=MeshFunction('size_t',gamma,2)
        for ver in entities(gamma, 2):
            i = mapa[ver.index()]
            boundaryboundary_parts[ver.index()] = boundary_parts[i]
        gammagamma = SubMesh(gamma, boundaryboundary_parts, marker)

        HB = TensorFunctionSpace(gammagamma, 'DG', degree, ((3,3)))
        #hb = Function(HB)
        hb = interpolate(h, HB)
        #print "HB INNER= ", HB
        #mapb = gammagamma.data().mesh_function("parent_cell_indices")
        #NumFacets = gammagamma.num_entities(2)
        #NumFacets2 = omega.num_entities(2)
        #Size1 = HB.vector().size()
        #Size2 = H.vector().size()
        #print 'Size1 = ', Size1
        #print 'Size2 = ', Size2
        #Size1 = Size1/9
        #Size2 = Size2/9
        #for ver in entities(gammagamma, 2):
        #           i = mapb[ver.index()]
        #           k = mapa[i]
        #           hb.vector()[ver.index()+0*NumFacets] = H.vector()[k+0*NumFacets2]
        #           hb.vector()[ver.index()+1*NumFacets] = H.vector()[k+1*NumFacets2]
        #           hb.vector()[ver.index()+2*NumFacets] = H.vector()[k+2*NumFacets2]
        #           hb.vector()[ver.index()+3*NumFacets] = H.vector()[k+3*NumFacets2]
        #           hb.vector()[ver.index()+4*NumFacets] = H.vector()[k+4*NumFacets2]
        #           hb.vector()[ver.index()+5*NumFacets] = H.vector()[k+5*NumFacets2]
        #           hb.vector()[ver.index()+6*NumFacets] = H.vector()[k+6*NumFacets2]
        #           hb.vector()[ver.index()+7*NumFacets] = H.vector()[k+7*NumFacets2]
        #           hb.vector()[ver.index()+8*NumFacets] = H.vector()[k+8*NumFacets2]
        #
        return hb

    def InterpolateExpressionOnSubsurface(expression, omega, boundary_parts, marker, type, degree):
        print "Start InterpolateExpressionOnSubsurface"
        #Extract Subsurface with ID marker
        gamma = BoundaryMesh(omega, "exterior")
        mapa = gamma.entity_map(2)
        boundaryboundary_parts=MeshFunction('size_t',gamma,2)
        for ver in entities(gamma, 2):
            i = mapa[ver.index()]
            boundaryboundary_parts[ver.index()] = boundary_parts[i]
        gammagamma = SubMesh(gamma, boundaryboundary_parts, marker)

        N=FunctionSpace(gammagamma, type, degree)
        F=Function(N)
        T=TestFunction(N)
        solve(T*expression*dx - T*F*dx == 0, F)
        return F

    def InterpolateVectorExpressionOnSubsurface(expression, omega, boundary_parts, marker, type, degree):
        print "Start InterpolateVectorExpressionOnSubsurface"
        #Extract Subsurface with ID marker
        gamma = BoundaryMesh(omega, "exterior")
        mapa = gamma.entity_map(2)
        boundaryboundary_parts=MeshFunction('size_t',gamma,2)
        for ver in entities(gamma, 2):
            i = mapa[ver.index()]
            boundaryboundary_parts[ver.index()] = boundary_parts[i]
        gammagamma = SubMesh(gamma, boundaryboundary_parts, marker)

        N=VectorFunctionSpace(gammagamma, type, degree, 3)
        F=Function(N)
        T=TestFunctions(N)
        solve(inner(T, expression)*dx - inner(T,F)*dx == 0, F)
        return F

    def GlobalizeMesh(self, mesh):
        TopDim = mesh.topology().dim()
        GeoDim = mesh.geometry().dim()
        NumVertexGlobal = mesh.size_global(0)
        NumCellsGlobal = mesh.size_global(TopDim)
        coords = mesh.coordinates()
        CoordsGlobal = numpy.zeros([NumVertexGlobal, GeoDim])
        NumProcGlobal = numpy.zeros(NumVertexGlobal)
        for v in vertices(mesh):
            x = numpy.array(coords[v.index()])
            CoordsGlobal[v.global_index(),:] = x
            NumProcGlobal[v.global_index()] = 1.0
        self.SyncSum(CoordsGlobal)
        self.SyncSum(NumProcGlobal)
        if min(NumProcGlobal==0):
            self.dprint("ERROR: THERE IS A VERTEX NOT BELONGING TO ANY PROCESSOR")
            exit(1)
        for IndexGlobal in range(NumVertexGlobal):
            CoordsGlobal[IndexGlobal,:] = CoordsGlobal[IndexGlobal,:]/NumProcGlobal[IndexGlobal]
        CellsGlobal = numpy.zeros([NumCellsGlobal, TopDim+1], dtype=numpy.uintp)
        for c in cells(mesh):
            LocVertexID = 0
            for v in vertices(c):
                CellsGlobal[c.global_index(), LocVertexID] = v.global_index()
                LocVertexID += 1
        self.SyncSum(CellsGlobal)
        #New: Also globalize Facets:
        NumFacetsGlobal = mesh.size_global(TopDim-1)
        NumProcFacets = numpy.zeros(NumFacetsGlobal)
        FacetsGlobal = numpy.zeros([NumFacetsGlobal, TopDim], dtype=numpy.uintp)
        for f in facets(mesh):
            LocVertexID = 0
            #For some VERY strange reason, faces in SymmetricMesh segfault upon global_index() call
            #And ONLY on 1 processor...
            if MPI.size(mpi_comm_world()) == 1:
                f_index = f.index()
            else:
                f_index = f.global_index()
            for v in vertices(f):
                FacetsGlobal[f_index, LocVertexID] = v.global_index()
                LocVertexID += 1
            NumProcFacets[f_index] = 1.0
        self.SyncSum(FacetsGlobal)
        self.SyncSum(NumProcFacets)
        for IndexGlobal in range(NumFacetsGlobal):
            FacetsGlobal[IndexGlobal,:] = FacetsGlobal[IndexGlobal,:]/NumProcFacets[IndexGlobal]
        return (CoordsGlobal, CellsGlobal, FacetsGlobal)
    
    def Mirror(self, mesh, plane, Offset = 0.0):
        code="""
            #include <dolfin/mesh/MeshPartitioning.h>
            
            namespace dolfin {
            void build_distributed_mesh(std::shared_ptr<Mesh> mesh)
            {
            MeshPartitioning::build_distributed_mesh(*mesh);
            }
            };
            """
        build_distributed_mesh = compile_extension_module(code).build_distributed_mesh
    
        MaxDim = mesh.topology().dim()
        #self.dprint("num_entities: %s"%mesh.size(0))
        #self.dprint("num_global_entities: %s"%mesh.size_global(0))
        mesh.init()
        mesh.order()
        (CoordsGlobal, CellsGlobal) = self.GlobalizeMesh(mesh)
        OnSymmetryGlobal = 0
        #Determine vertices on mirror plane (global)
        for i in range(mesh.size_global(0)):
            #if OnInterfaceGlobal[i] > 0.0:
            if(bool(near(CoordsGlobal[i, plane], Offset, self.TestTol))):
                OnSymmetryGlobal += 1
        #Global Number of Vertices on Symmetry
        #self.dprint("Vertices on Symmetry Plane (global) %d"%OnSymmetryGlobal)
        NumVertexGlobal = mesh.size_global(0)
        NumSVertexGlobal = 2*NumVertexGlobal - OnSymmetryGlobal
        NumCellsGlobal = mesh.size_global(MaxDim)
        
        SymmetricMesh = Mesh()
        editor = MeshEditor()
        editor.open(SymmetricMesh, MaxDim, MaxDim)
        editor.init_vertices_global(NumSVertexGlobal, NumSVertexGlobal)
        editor.init_cells_global(2*NumCellsGlobal, 2*NumCellsGlobal)
        #MirrorVertexMap: points from global id of original vertex to global id of mirrored vertex
        #identity if point on mirror plane
        MirrorVertexMap = numpy.zeros(NumVertexGlobal, dtype=numpy.uintp)
        MirrorVertexMapInv = numpy.zeros(NumSVertexGlobal, dtype=numpy.uintp)
        #Parallel bug in FEniCS 1.7dev:
        #BEGIN SERIAL PART: Build mesh on node 0 only!
        if MPI.rank(mpi_comm_world()) == 0:
            #compute new global indices for the new points on the mirrored half:
            SIndexGlobal = numpy.zeros(NumVertexGlobal, dtype="P")
            OffsetIndex = 0
            #self.pprint("MIRROR: Computing Indices for Points on Mirror")
            for i in range(mesh.size_global(0)):
                if(bool(near(CoordsGlobal[i, plane], Offset, self.TestTol))):
                    OffsetIndex = OffsetIndex + 1
                    SIndexGlobal[i] = OffsetIndex
            #self.pprint("MIRROR: Adding Vertices")
            NumAdded = 0
            for IndexGlobal in range(NumVertexGlobal):
                x = CoordsGlobal[IndexGlobal,:]
                editor.add_vertex_global(IndexGlobal, IndexGlobal, x)
                MirrorVertexMap[IndexGlobal] = IndexGlobal
                MirrorVertexMapInv[IndexGlobal] = IndexGlobal
                if not bool(near(x[plane], Offset, self.TestTol)):
                    #Compute Mirror coordinates:
                    x_new = numpy.empty(MaxDim)
                    for i in range(MaxDim):
                        x_new[i] = x[i]
                    x_new[plane] = -(x_new[plane]-Offset)+Offset
                    editor.add_vertex_global(NumVertexGlobal + NumAdded, NumVertexGlobal + NumAdded, x_new)
                    MirrorVertexMap[IndexGlobal] = NumVertexGlobal + NumAdded
                    MirrorVertexMapInv[NumVertexGlobal + NumAdded] = IndexGlobal
                    NumAdded = NumAdded + 1
            #self.pprint("MIRROR: Adding Elements")
            for Index in range(NumCellsGlobal):
                VertIndex = CellsGlobal[Index, :]
                editor.add_cell(Index, VertIndex)
                editor.add_cell(Index+NumCellsGlobal, MirrorVertexMap[VertIndex])
        #END SERIAL PART
        editor.close()
        self.SyncSum(MirrorVertexMap)
        self.SyncSum(MirrorVertexMapInv)
        #self.dprint("Building distributed mesh")
        build_distributed_mesh(SymmetricMesh)
        SymmetricMesh.init()
        SymmetricMesh.order()
        #self.dprint("Mesh distributed. Local vertices: %d"%SymmetricMesh.size(0))
        #File("MeshTest.pvd") << SymmetricMesh
        #plot(SymmetricMesh, interactive=True)
        return (SymmetricMesh, MirrorVertexMapInv)
    
    #Return map "local facet ID Symmetric -> global facet ID original"
    def MatchMirrorFaces(self, mesh, SymmetricMesh, MirrorVertexMap):
        MaxDim = mesh.topology().dim()
        #Make an array: [Global Cell, Global Facets, Global Vertices]
        NumCellsGlobal = mesh.size_global(MaxDim)
        FacetsGlobal = numpy.zeros([NumCellsGlobal, MaxDim+1, MaxDim], dtype=numpy.uintp)
        FacetsGlobalIDs = numpy.zeros([NumCellsGlobal, MaxDim+1], dtype=numpy.uintp)
        for c in cells(mesh):
            LocFacetID = 0
            for f in facets(c):
                if f.is_ghost() == False:
                    LocVertexID = 0
                    MyVertex = numpy.zeros(f.num_entities(0), dtype=numpy.uintp)
                    for v in vertices(f):
                        MyVertex[LocVertexID] = v.global_index()
                        LocVertexID = LocVertexID+1
                    FacetsGlobal[c.global_index(), LocFacetID,:] = MyVertex
                    #For some VERY strange reason, faces in SymmetricMesh segfault upon global_index() call
                    #And ONLY on 1 processor...
                    if MPI.size(mpi_comm_world()) == 1:
                        FacetsGlobalIDs[c.global_index(), LocFacetID] = f.index()
                    else:
                        FacetsGlobalIDs[c.global_index(), LocFacetID] = f.global_index()
                    LocFacetID += 1
        #no division needed, each cell is only part of one processor!
        self.SyncSum(FacetsGlobal)
        self.SyncSum(FacetsGlobalIDs)
        #CopyFacetMap = MeshFunction("size_t", mesh, MaxDim-1)
        MirrorFacetMap = MeshFunction("size_t", SymmetricMesh, MaxDim-1)
        for i in range(len(MirrorFacetMap.array())):
            MirrorFacetMap.array()[i] = 0
        for f in facets(SymmetricMesh):
            if f.is_ghost() == True:
                continue
            #CFound = False
            MFound = False
            #MyVertexC = numpy.zeros(f.num_entities(0), dtype=numpy.uintp)
            MyVertexM = numpy.zeros(f.num_entities(0), dtype=numpy.uintp)
            i = 0
            for v in vertices(f):
                #MyVertexC[i] = v.global_index()
                MyVertexM[i] = MirrorVertexMap[v.global_index()]
                i = i+1
            #MyVertexC = sorted(MyVertexC)
            MyVertexM = sorted(MyVertexM)
            for e in cells(f):
                GID_C = e.global_index()
                if GID_C < mesh.size_global(MaxDim):
                    GID_M = GID_C
                else:
                    GID_M = GID_C - mesh.size_global(MaxDim)
                for j in range(e.num_entities(MaxDim-1)):
                    MM = sorted(FacetsGlobal[GID_M, j, :])
                    if MyVertexM == MM:
                        MirrorFacetMap.array()[f.index()] = FacetsGlobalIDs[GID_M, j]
                        MFound = True
                        #self.dprint("j=%d"%j)
                        break
                break
            #if CFound == False:
            #    self.dprint("MatchMirrorFaces: Copy Facet not found!")
            #    exit(1)
            if MFound == False:
                self.dprint("MatchMirrorFaces: Mirror Facet not found!")
                for e in cells(f):
                    GID_C = e.global_index()
                    if GID_C < mesh.size_global(MaxDim):
                        GID_M = GID_C
                    else:
                        GID_M = GID_C - mesh.size_global(MaxDim)
                    for j in range(e.num_entities(MaxDim-1)):
                        MM = sorted(FacetsGlobal[GID_M, j, :])
                        print "Matching", MyVertexM, MM
                exit(1)
        return MirrorFacetMap

    def VisualizeMeshH5(self, FileName, mesh):
        FSpace = FunctionSpace(mesh, "DG", 0)
        Z = project(Constant(0.0), FSpace)
        MyFile = XDMFFile(mpi_comm_world(), FileName)
        MyFile << mesh

    def StoreAppendFunction(self, MyFolder, MyIter, MyFunction):
        FName = MyFunction.name()
        if FName not in self.VisFunctions:
            #print "Adding", FName
            MyXFile = XDMFFile(self.Communicator, MyFolder+"/"+FName+".xdmf")
            MyXFile.parameters["rewrite_function_mesh"] = self.ReWriteMesh
            MyXFile.parameters["flush_output"] = self.FlushOutput
            self.VisFunctions.update({FName:MyXFile})
            self.VisFunctions[FName] << (MyFunction, float(MyIter))
            #update restart file of stored functions:
            if MPI.rank(mpi_comm_world()) == 0 and self.Restart == False:
                NameFile = open(MyFolder+"/VisFileNames.txt", "a")
                    #for k in self.VisFunctions.keys():
                NameFile.write(FName + " " + str(MyFunction.function_space().element().value_dimension(0))+" "+MyFunction.function_space().element().signature()+"\n")
                NameFile.close()
        else:
            self.VisFunctions[FName] << (MyFunction, float(MyIter))
        MyFile = HDF5File(mpi_comm_world(), MyFolder+"/RAW/"+FName+"_"+str(MyIter)+".h5", "w")
        MyFile.write(MyFunction, FName)
        MyFile.close()

    #Recreate the morph visualization after a restart
    def ReconstructVisualization(self, MyFolder, MyIter):
        NameFile = open(MyFolder+"/RAW/VisFileNames.txt", "r")
        for line in NameFile:
            MyLine = line.split()
            FName = MyLine[0]
            MaxDim = int(MyLine[1])
            self.pprint("Reconstructing "+FName)
            #Get the FEniCS Element Type Descriptor
            FEIdentifier = MyLine[2]
            MeshDim = 3
            start = FEIdentifier.find("'")+1
            end = start+FEIdentifier[start:].find("'")
            FEType = FEIdentifier[start:end]
            FECell = MyLine[3]
            if "triangle" in FECell:
                OrderIndex = 5
                IsSurf= True
            else:
                OrderIndex = 4
                IsSurf = False
            FEOrder = MyLine[OrderIndex]
            FEOrder = MyLine[OrderIndex][MyLine[OrderIndex].rfind(")")-1]
            FEOrder = int(FEOrder)
            self.pprint("Reconstructing first %d iterations of Function "%MyIter+FName+" of type "+FEType+" of order %d %d"%(FEOrder, MaxDim))
            MyXFile = XDMFFile(self.Communicator, MyFolder+"/"+FName+".xdmf")
            MyXFile.parameters["rewrite_function_mesh"] = True#self.ReWriteMesh
            MyXFile.parameters["flush_output"] = True#self.FlushOutput
            self.VisFunctions.update({FName:MyXFile})
            for i in range(MyIter):
                MyName = MyFolder+"/../"+"OptIter%d"%i
                #self.pprint("Opening Mesh "+MyName)
                if IsSurf == False:
                    (TMesh, TBP) = self.LoadMeshH5(MyName+"/Mesh.h5")
                #else:
                #    TMesh = self.LoadMeshH5(MyName+"/Gamma.h5", False)
                    if MaxDim > 0:
                        MySpace = VectorFunctionSpace(TMesh, FEType, FEOrder, int(MaxDim))
                    else:
                        MySpace = FunctionSpace(TMesh, FEType, FEOrder)
                    MyF = Function(MySpace)
                    MyFile = HDF5File(mpi_comm_world(), MyFolder+"/RAW/"+FName+"_"+str(i)+".h5", "r")
                    MyFile.read(MyF, FName)
                    MyFile.close()
                    MyF.rename(FName, "dummy")
                    self.VisFunctions[FName] << (MyF, float(i))
        NameFile.close()
    
    def StoreMirrorMeshHDD(self, FileName, Mesh, boundary_parts, SymmetricMesh, SymmetricBoundaryParts, CFM, MFM):
        File = HDF5File(mpi_comm_world(), FileName, "w")
        File.write(Mesh, "Mesh")
        File.write(boundary_parts, "boundary_parts")
        File.write(SymmetricMesh, "SymmetricMesh")
        File.write(SymmetricBoundaryParts, "SymmetricBoundaryParts")
        File.write(CFM, "CopyFacetMap")
        File.write(MFM, "MirrorFacetMap")
        #File.close()
        
    def LoadMirrorMeshHDD(self, FileName):
        File = HDF5File(mpi_comm_world(), FileName, "r")
        mesh = Mesh()
        File.read(mesh, "Mesh", False)
        MaxDim = mesh.topology().dim()
        self.pprint("MaxDim %d"%MaxDim)
        boundary_parts = MeshFunction('size_t', mesh, MaxDim-1)
        File.read(boundary_parts, "boundary_parts")
        SymmetricMesh = Mesh()
        File.read(SymmetricMesh, "SymmetricMesh", False)
        SymmetricBoundaryParts = MeshFunction('size_t', SymmetricMesh, MaxDim-1)
        File.read(SymmetricBoundaryParts, "SymmetricBoundaryParts")
        CFM = MeshFunction('size_t', mesh, MaxDim-1)
        File.read(CFM, "CopyFacetMap")
        MFM = MeshFunction('size_t', mesh, MaxDim-1)
        File.read(MFM, "MirrorFacetMap")
        #File.close()
        return (mesh, boundary_parts, SymmetricMesh, SymmetricBoundaryParts, CFM, MFM)

    def GlobalizeMeshFunction(self, mf):
        MaxDim = mf.dim()
        mesh = mf.mesh()
        GlobalValues = numpy.zeros(mesh.size_global(MaxDim), dtype=numpy.uintp)
        GlobalNumProc = numpy.zeros(mesh.size_global(MaxDim), dtype=numpy.uintp)
        for e in entities(mesh, MaxDim):
            IDL = e.index()
            #strange bug in mesh generator meshes...
            if MPI.size(mpi_comm_world()) == 1 and MaxDim == 2:
                IDG = IDL
            else:
                IDG = e.global_index()
            GlobalValues[IDG] = mf.array()[IDL]
            GlobalNumProc[IDG] += 1
        self.SyncSum(GlobalValues)
        self.SyncSum(GlobalNumProc)
        return GlobalValues/GlobalNumProc

    def MirrorMeshFunction(self, SymmetricMesh, boundary_parts, MirrorFacetMap, MarkerToRemove = -1):
        MaxDim = boundary_parts.dim()
        BP_global = self.GlobalizeMeshFunction(boundary_parts)
        SymmetryBoundaryMap = MeshFunction('size_t', SymmetricMesh, MaxDim)
        for i in range(len(SymmetryBoundaryMap.array())):
            SymmetryBoundaryMap.array()[i] = 0
        for f in facets(SymmetricMesh):
            if f.is_ghost() == True:
                continue
            LID = f.index()
            #strange fenics bug:
            #if MPI.size(mpi_comm_world()) == 1:
            #    GID = f.index()
            #else:
            #    GID = f.global_index()
            value = BP_global[MirrorFacetMap.array()[LID]]
            if value in MarkerToRemove:
                value = 0
            SymmetryBoundaryMap.array()[LID] = value
        return SymmetryBoundaryMap

    #return an array A[global_cell_id] = global_dofs
    def GlobalizeDofMap(self, SymmetricMesh, SpaceType, SpaceOrder):
        MaxDim = SymmetricMesh.topology().dim()
        TestFunctionSpaceM = FunctionSpace(SymmetricMesh, SpaceType, SpaceOrder)
        MyDofMap = TestFunctionSpaceM.dofmap()
        #print "max_cell_dimension", MyDofMap.max_cell_dimension()
        #print "max_element_dofs()", MyDofMap.max_element_dofs()
        DofsPerElement = MyDofMap.max_cell_dimension()
        GlobalDimension = MyDofMap.global_dimension()
        CellDofsGlobal = numpy.zeros([SymmetricMesh.size_global(MaxDim), DofsPerElement], dtype=numpy.uintp)
        for c in cells(SymmetricMesh):
            CellDofs = MyDofMap.cell_dofs(c.index())
            for j in range(DofsPerElement):
                CellDofsGlobal[c.global_index(), j] = MyDofMap.local_to_global_index(CellDofs[j])
        self.SyncSum(CellDofsGlobal)
        return CellDofsGlobal
    
    def GlobalizeFunction(self, f):
        MyDofMap = f.function_space().dofmap()
        NumDofsGlobal = MyDofMap.global_dimension()
        GlobalValues = numpy.zeros(NumDofsGlobal)
        GlobalProcs = numpy.zeros(NumDofsGlobal)
        (sdmin, sdmax) = f.vector().local_range()
        #sdmin = int(sdmin/MaxDim)
        #sdmax = int(sdmax/MaxDim)
        for i in range(sdmax-sdmin):
            value = f.vector()[i]
            gindex = MyDofMap.local_to_global_index(i)
            GlobalValues[gindex] = value
            GlobalProcs[gindex] = 1.0
        self.SyncSum(GlobalValues)
        self.SyncSum(GlobalProcs)
        return GlobalValues/GlobalProcs
    
    #Make DofMapping for copying from SourceMesh to DestinationMesh
    #so mapping points from DestinationMesh to SourceMesh
    def MakeInterMeshDofMap(self, SourceMesh, DestinationMesh, SpaceType, SpaceOrder, CellMap = None):
        #transmit symmetric mesh
        #main iterate over original mesh
        #but return mapping from DestinationMesh mesh (global) to SourceMesh mesh (global)...
        CellDofsGlobal = self.GlobalizeDofMap(SourceMesh, SpaceType, SpaceOrder)
        DimSource = SourceMesh.topology().dim()
        DimDesination = DestinationMesh.topology().dim()
        TestFunctionSpaceS = FunctionSpace(SourceMesh, SpaceType, SpaceOrder)
        TestFunctionSpaceD = FunctionSpace(DestinationMesh, SpaceType, SpaceOrder)
        TestXS = interpolate(self.TestExpression, TestFunctionSpaceS)
        #plot(TestXS, interactive=True)
        TestXS = self.GlobalizeFunction(TestXS)
        TestXD = interpolate(self.TestExpression, TestFunctionSpaceD)
        OwnerRange = TestFunctionSpaceD.dofmap().ownership_range()
        #InvMap = numpy.zeros([2,TestFunctionSpaceD.dofmap().global_dimension()], dtype=numpy.uintp)
        InvMap = numpy.zeros([2,TestFunctionSpaceD.dofmap().local_dimension("owned")], dtype=numpy.uintp)
        #InvMap = numpy.zeros([2,TestFunctionSpaceD.dofmap().local_dimension("all")], dtype=numpy.uintp)
        for e in cells(DestinationMesh):
            LID_Destination = e.index()
            GID_Destination = e.global_index()
            
            IsMirrored = 0
            #no map, assume we want to fill the mirror mesh
            if CellMap == None:
                if GID_Destination() < mesh.size_global(MaxDim):
                    GID_Source = GID_Destination
                else:
                    GID_Source = GID_Destination - SourceMesh.size_global(DimSource)
                    IsMirrored = 1
            else:
                GID_Source = CellMap[GID_Destination]
        
            SourceDofs = CellDofsGlobal[GID_Source,:]
            DestinationDofs = TestFunctionSpaceD.dofmap().cell_dofs(LID_Destination)
            for DDof in DestinationDofs:
                IsOwned = OwnerRange[0] <= TestFunctionSpaceD.dofmap().local_to_global_index(DDof) and TestFunctionSpaceD.dofmap().local_to_global_index(DDof) < OwnerRange[1]
                #print "DDof", DDof, IsOwned
                if IsOwned:
                    if InvMap[0, DDof] != 0:
                        continue
                    DValue = TestXD.vector()[DDof]
                    DDofFound = False
                    for SDof in SourceDofs:
                        SValue = TestXS[SDof]
                        if abs(DValue-SValue) < self.TestTol or SpaceOrder == 0:
                            #InvMap[0][DDof] = TestFunctionSpaceS.dofmap().local_to_global_index(SDof)
                            InvMap[0][DDof] = SDof
                            InvMap[1][DDof] = IsMirrored
                            DDofFound = True
                            break
                    if DDofFound == False:
                        self.dprint("ERROR: MakeInterMeshDofMap: Destination DOF not found!")
                        self.dprint("Processing: %d %d"%(LID_Destination, GID_Source))
                        self.dprint("Looking for valueP: %e"%DValue)
                        for SDof in SourceDofs:
                            SValue = TestXS[SDof]
                            self.dprint("Found valueC: %e"%SValue)
                        exit(1)
        #self.SyncSum(InvMap)
        return InvMap

    #This is a bit complicated...
    #Boundary Mesh Face -> Full Mesh Face -> Mirror Mesh Face -> Mirror Boundary Mesh Face
    def MakeMirrorFaceDofMap(self, mesh, SymmetricMesh, CopyFaceMap, MirrorFaceMap, SpaceType, SpaceOrder, marker):
        gamma = BoundaryMesh(mesh, "exterior")
        gammaM = BoundaryMesh(SymmetricMesh, "exterior")
        MapaM = gammaM.entity_map(2)
        MapaMInv = MeshFunction('size_t', SymmetricMesh, 2)
        for i in range(len(MapaMInv.array())):
            MapaMInv[i] = 0
        for i in range(len(MapaM)):
            #print i, MapaM[i]
            MapaMInv[MapaM[i]] = i
        #exit()
        TestFunctionSpace = FunctionSpace(gamma, SpaceType, SpaceOrder)
        TestFunctionSpaceM = FunctionSpace(gammaM, SpaceType, SpaceOrder)
        TestExpression = self.TestExpression
        TestX = interpolate(TestExpression, TestFunctionSpace)
        TestXM = interpolate(TestExpression, TestFunctionSpaceM)
        dm = TestFunctionSpace.dofmap()
        dmMirror = TestFunctionSpaceM.dofmap()
        mapa = gamma.entity_map(2)
        CopyFaceDofMap = range(len(TestX.vector()))
        MirrorFaceDofMap = range(len(TestX.vector()))
        for i in range(len(TestX.vector())):
            CopyFaceDofMap[i] = -1
            MirrorFaceDofMap[i] = -1
        for MyFace in entities(gamma, 2):
            FaceIndex = MyFace.index()
            i = mapa[FaceIndex]
            if self.boundary_parts[i] == marker:
                CopyID = MapaMInv[CopyFaceMap[i]]
                MirrorID = MapaMInv[MirrorFaceMap[i]]
                #check vertices
                CopyFace = Face(gammaM, CopyID)
                MyVertex = MyFace.entities(0)
                #CopyVertex = CopyFace.entities(0)
                #print "MyVertex", MyVertex, "CopyVertex", CopyVertex
                dofs = dm.cell_dofs(FaceIndex)
                CopyDofs = dmMirror.cell_dofs(CopyID)
                MirrorDofs = dmMirror.cell_dofs(MirrorID)
                for j in range(len(dofs)):
                    value = TestX.vector()[dofs[j]]
                    FoundCopy = False
                    for k in range(len(dofs)):
                        ValueCopy = TestXM.vector()[CopyDofs[k]]
                        if value == ValueCopy:
                            CopyFaceDofMap[dofs[j]] = CopyDofs[k]
                            FoundCopy = True
                            break
                    if FoundCopy == False:
                        print "COPY FACE DOF NOT FOUND!"
                        exit()
                    FoundMirror = False
                    for k in range(len(dofs)):
                        ValueMirror = TestXM.vector()[MirrorDofs[k]]
                        if value == ValueMirror:
                            MirrorFaceDofMap[dofs[j]] = MirrorDofs[k]
                            FoundMirror = True
                            break
                    if FoundMirror == False:
                        print "MIRROR FACE DOF NOT FOUND!"
                        exit()
        return CopyFaceDofMap, MirrorFaceDofMap

    #OrigDof is mapping from local destination dof to global source dof
    #Push function from SourceMesh to DestinationMesh
    def PushInterMesh(self, SourceMesh, DestinationMesh, OrigDof, fSource, Type, Deg, plane=-1):
        MyName = fSource.name()
        dim = fSource.function_space().element().value_dimension(0)
        #print fSource.function_space().element().signature()
        fSource = self.GlobalizeFunction(fSource)
        if dim == 1:
            DestinationSpace = FunctionSpace(DestinationMesh, Type, Deg)
        else:
            DestinationSpace = VectorFunctionSpace(DestinationMesh, Type, Deg, dim)
        fDestination = Function(DestinationSpace)
        #VecEnd = len(fDestination.vector())/dim
        #VecEnd = DestinationSpace.dofmap().local_dimension("owned")/dim
        VecEnd = fDestination.vector().local_size()/dim
        if not numpy.shape(OrigDof)[1] == VecEnd:
            print "DestinationFunction: Problem with number of DOF"
            print "len(OrigDof) =", numpy.shape(OrigDof), "but VecEnd =", VecEnd
            exit(1)
        LocValues = numpy.zeros(dim*VecEnd)
        for LID in range(VecEnd):
            #GID = DestinationSpace.dofmap().local_to_global_index(dim*i)/dim
            IndexOrig = OrigDof[0][LID]
            for j in range(dim):
                #print "access:", IndexOrig, "VecEnd", VecEnd
                value = fSource[dim*IndexOrig+j]
                LocValues[dim*LID+j] = value
                if j == plane and OrigDof[1][LID] > 0:
                    LocValues[dim*LID+j] = -value
        fDestination.vector().set_local(LocValues)
        fDestination.vector().apply("")
        fDestination.rename(MyName, "dummy")
        return fDestination

    #OrigDof is mapping from local destination dof to global source dof
    #Pull function from DestinationMesh to SourceMesh
    #WARNING: Problems with DG > 0
    def PullInterMesh(self, SourceMesh, DestinationMesh, OrigDof, fOrig, Type, Deg, plane=-1):
        dim = fOrig.function_space().element().value_dimension(0)
        #print fOrig.function_space().element().signature()
        VecEnd = fOrig.vector().local_size()/dim
        #fOrig = self.GlobalizeFunction(fOrig)
        if dim == 1:
            SourceSpace = FunctionSpace(SourceMesh, Type, Deg)
        else:
            SourceSpace = VectorFunctionSpace(SourceMesh, Type, Deg, dim)
        fSource = Function(SourceSpace)
        
        if not numpy.shape(OrigDof)[1] == VecEnd:
            print "DestinationFunction: Problem with number of DOF"
            print "len(OrigDof) =", numpy.shape(OrigDof), "but VecEnd =", VecEnd
            exit(1)
        GlobalValues = numpy.zeros(len(fSource.vector()))
        for LID in range(VecEnd):
            #GID = DestinationSpace.dofmap().local_to_global_index(dim*i)/dim
            IndexOrig = OrigDof[0][LID]
            for j in range(dim):
                #print "access:", IndexOrig, "VecEnd", VecEnd
                value = fOrig.vector()[dim*LID+j]
                GlobalValues[dim*IndexOrig+j] = value
                if j == plane and OrigDof[1][LID] > 0:
                    GlobalValues[dim*IndexOrig+j] = -value
        self.SyncSum(GlobalValues)
        LocValues = numpy.zeros(fSource.vector().local_size())
        for i in range(fSource.vector().local_size()):
            GID = SourceSpace.dofmap().local_to_global_index(i)
            LocValues[i] = GlobalValues[GID]
        fSource.vector().set_local(LocValues)
        fSource.vector().apply("")
        fSource.rename(fOrig.name(), "dummy")
        return fSource

    #insert value from the copy cell, not the mirror cell
    def ReduceFunctionFromMirror(self, CopyDof, MirrorDof, f, fMirror, dim):
        #VecEnd = len(f.vector())/dim
        VecEnd = f.vector().local_size()/dim
        if not len(CopyDof) == VecEnd:
            print "ReduceFunctionFromMirror: Problem with number of DOF1"
            print "Length of CopyDof:", len(CopyDof)
            print "Length of vector:", VecEnd
            exit()
        LocValues = numpy.zeros(dim*VecEnd)
        for i in range(VecEnd):
            IndexCopy = CopyDof[i]
            if IndexCopy >-1:
                for j in range(dim):
                    value = fMirror.vector()[dim*IndexCopy+j]
                    LocValues[dim*i+j] = value
        f.vector().set_local(LocValues)
        f.vector().apply("")

    def ReduceFunction(self, mesh, SymmetricMesh, CopyDof, MirrorDof, f, Type, Deg, dim):
        if dim == 1:
            OriginalSpace = FunctionSpace(mesh, Type, Deg)
        else:
            OriginalSpace = VectorFunctionSpace(mesh, Type, Deg)
        fCopy = Function(OriginalSpace)
        self.ReduceFunctionFromMirror(CopyDof, MirrorDof, fCopy, f, dim)
        return fCopy

    def ReduceSurfaceFunction(self, mesh, SymmetricMesh, CopyDof, MirrorDof, f, Type, Deg, dim):
        gamma = BoundaryMesh(mesh, "exterior")
        #gammaM = BoundaryMesh(SymmetricMesh, "exterior")
        if dim == 1:
            OriginalSpace = FunctionSpace(gamma, Type, Deg)
        else:
            OriginalSpace = VectorFunctionSpace(gamma, Type, Deg, dim)
        fCopy = Function(OriginalSpace)
        self.ReduceFunctionFromMirror(CopyDof, MirrorDof, fCopy, f, dim)
        return fCopy

    #FEniCS sometimes shows weird behavior when interpolating a surface function into the volume
    #In particular the case of missing values after the ComponentLaplaceDefo
    #input surface function V, output same function on surface but zeros in the volume
    #return the dofs on the mirror mesh based on the dofs of the original mesh
    #Warning: THIS COULD THEORETICAL FAIL ON VERY SPECIAL MESHES...
    def MakeSurfaceToVolumeDofMapOLD(self, mesh, gamma, SpaceType, SpaceOrder):
        TestFunctionSpaceV = FunctionSpace(mesh, SpaceType, SpaceOrder)
        TestFunctionSpaceS = FunctionSpace(gamma, SpaceType, SpaceOrder)
        TestExpression = self.TestExpression
        TestXS = interpolate(TestExpression, TestFunctionSpaceS)
        TestXV = interpolate(TestExpression, TestFunctionSpaceV)
        SurfaceToVolumeDof = numpy.empty(len(TestXS.vector()), dtype=int)
        SurfaceToVolumeDof.fill(-1)
        for i in range(len(TestXS.vector())):
            value1 = TestXS.vector()[i]
            for j in range(len(TestXV.vector())):
                value2 = TestXV.vector()[j]
                if value1 == value2:
                    SurfaceToVolumeDof[i] = j
                    break
        return SurfaceToVolumeDof

    #new version but only for CG1
    #does not work in parallel
    def MakeSurfaceToVolumeDofMap(self, mesh, gamma, SpaceType, SpaceOrder):
        if not SpaceType == "CG":
            print "ONLY WORKS FOR CG1 FUNCTIONS"
            exit()
        if not SpaceOrder == 1:
            print "ONLY WORKS FOR CG1 FUNCTIONS"
            exit()
        MoveSpaceV = FunctionSpace(mesh, SpaceType, SpaceOrder)
        MoveSpaceS = FunctionSpace(gamma, SpaceType, SpaceOrder)
        #surface dof to vertex
        SDtoV = dof_to_vertex_map(MoveSpaceS)
        #volume vertex to dof
        VVtoD = vertex_to_dof_map(MoveSpaceV)
        SurfaceToVolumeDof = numpy.empty(len(SDtoV), dtype=int)
        mapa = gamma.entity_map(0)
        #for every local surface dof i
        for i in range(len(SDtoV)):
            #get local ID of surface vertex for ith surface dof
            SVertex = int(SDtoV[i])
            #transform to volume ID for same vertex
            VVertex = mapa[SVertex]
            #get volume dof for this volume vertex
            VDof = VVtoD[VVertex]
            #set value
            SurfaceToVolumeDof[i] = VDof
        return SurfaceToVolumeDof
    
    def ReverseBoundaryMap(self, mesh, gamma):
        mapa = gamma.entity_map(0)
        ReverseMap = MeshFunction('int',mesh,0)
        for i in range(len(ReverseMap.array())):
            ReverseMap[i] = -1
        #self.dprint("size: %d"%(len(mapa.array())))
        for i in range(len(mapa.array())):
            ReverseMap[mapa[i]] = i
        return ReverseMap

    def ExtentVectorSurfaceFunctionToVolume(self, mesh, V, boundary_parts = [], SymmX = [], SymmY = [], SymmZ = []):
        gamma = BoundaryMesh(mesh, "exterior")
        MaxDim = self.mesh.topology().dim()
        SpaceType = "CG"
        SpaceOrder = 1
        #Globalize and order V by global indices
        VGlobal = numpy.zeros(len(V.vector()))
        SurfVert = dof_to_vertex_map(FunctionSpace(gamma, SpaceType, SpaceOrder))
        (sdmin, sdmax) = V.vector().local_range()
        sdmin = int(sdmin/MaxDim)
        sdmax = int(sdmax/MaxDim)
        for i in range(sdmax-sdmin):
            Vert = MeshEntity(gamma, 0, SurfVert[i])
            IndexGlobal = Vert.global_index()
            for j in range(MaxDim):
                value = V.vector()[MaxDim*(i+sdmin)+j]
                VGlobal[MaxDim*IndexGlobal+j] = value
        VGlobal = self.SyncSum(VGlobal)
        #MyCorrectBoundary = self.VertexOnBoundary(self.VariableBoundary)
        MyReverseMap = self.ReverseBoundaryMap(mesh, gamma)
        mapa = gamma.entity_map(0)
        SurfDof = vertex_to_dof_map(FunctionSpace(gamma, SpaceType, SpaceOrder))
        VolDof = vertex_to_dof_map(FunctionSpace(mesh, SpaceType, SpaceOrder))
        VolVert = dof_to_vertex_map(FunctionSpace(mesh, SpaceType, SpaceOrder))
        SpaceV = VectorFunctionSpace(mesh, SpaceType, SpaceOrder, MaxDim)
        VVolume = Function(SpaceV)
        (vdmin, vdmax) = VVolume.vector().local_range()
        vdmin = int(vdmin/MaxDim)
        vdmax = int(vdmax/MaxDim)
        MyData = numpy.zeros(MaxDim*(vdmax-vdmin))
        MyRange = FunctionSpace(mesh, SpaceType, SpaceOrder).dofmap().ownership_range()
        for i in range(vdmin, vdmax):
            GlobalDof = i
            LocalDof = i-vdmin
            LocVert = int(VolVert[LocalDof])
            SurfVert = MyReverseMap[LocVert]
            boundary = 0.0
            if SurfVert != -1:
                Vert = MeshEntity(gamma, 0, SurfVert)
                SDof = SurfDof[SurfVert]
                GlobalIndex = Vert.global_index()
                #if MyCorrectBoundary[GlobalIndex] > 0.0:
                for j in range(MaxDim):
                    value = VGlobal[MaxDim*GlobalIndex+j]
                    MyData[MaxDim*LocalDof + j] = value
        VVolume.vector().set_local(MyData)
        VVolume.vector().apply('')
        if SymmX != [] or SymmY != [] or SymmZ != []:
            VVolume = self.FixSymmetryPlane(boundary_parts, VVolume, SymmX, SymmY, SymmZ)
        return VVolume

    def ComponentLaplaceDefo(self, mesh, boundary_parts, V, Stiffness = Constant(0.1), closed = False, VariableIDs = [], FixedIDs = [], FixX = [], FixY = [], FixZ = []):
        MaxDim = self.mesh.topology().dim()
        #gamma = BoundaryMesh(mesh, "exterior")
        #dss=ds[boundary_parts]
        #dx=dx[boundary_parts]
        N = VectorFunctionSpace(mesh, 'CG', 1, MaxDim)
        #interpolate from surface to volume sometimes misses components...
        #V = interpolate(V, N)
        if V.function_space().mesh().geometry().dim() == mesh.geometry().dim()-1:
            self.pprint("ComponentLaplaceDefo: Extending Function from Surface to Volume")
            V = self.ExtentVectorSurfaceFunctionToVolume(mesh, V, boundary_parts, FixX, FixY, FixZ)
        #File("VTest.pvd") << V
        v = TestFunctions(N)
        w = TrialFunctions(N)
        a = 0.0
        l = 0.0
        for i in range(MaxDim):
            a += Stiffness*inner(grad(v[i]),grad(w[i]))*dx
            l += Constant(0.0)*v[i]*dx
        if closed == True:
            for i in range(MaxDim):
                a += v[i]*w[i]*ds
                l += v[i]*V[i]*ds
        A = assemble(a)
        b = assemble(l)
        if MaxDim == 3:
            Zero = (0.0, 0.0, 0.0)
        elif MaxDim == 2:
            Zero = (0.0, 0.0)
        bc = []
        if boundary_parts != []:
            if closed == False:
                for i in VariableIDs:
                    bc.append(DirichletBC(N, V, boundary_parts, i))
                for i in FixedIDs:
                    bc.append(DirichletBC(N, Zero, boundary_parts, i))
                for i in FixX:
                    bc.append(DirichletBC(N.sub(0), 0.0, boundary_parts, i))
                for i in FixY:
                    bc.append(DirichletBC(N.sub(1), 0.0, boundary_parts, i))
                for i in FixZ:
                    bc.append(DirichletBC(N.sub(2), 0.0, boundary_parts, i))
        else:
            bc.append(DirichletBC(N, V, "on_boundary"))
        for MyBC in bc:
            MyBC.apply(A,b)
        if True:
            PreconditionerType = "default"
            solver=KrylovSolver(A, "gmres")
            solver.parameters['nonzero_initial_guess'] = False#True
            solver.parameters['relative_tolerance'] = 1e-8
            solver.parameters['absolute_tolerance'] = 1e-15
            #solver.parameters['preconditioner']['structure'] = 'same'
            solver.parameters['preconditioner']['structure'] = 'different_nonzero_pattern'
            solver.parameters["monitor_convergence"] = False#True
        else:
            solver=LUSolver(A, "default")
            solver.parameters["reuse_factorization"]=False#True
        Sol = Function(N)
        solver.solve(Sol.vector(), b)
        return Sol
    
    def ConvectionDeformation(self, V, Diffusion = 1e-2, VariableIDs = [], FixedIDs = []):
        import ConvectionDiffusion as CDS
        MaxDim = self.mesh.topology().dim()
        Deg = 1
        CDSolver = CDS.ConvectionDiffusion(self.mesh, self.boundary_parts, "./CDDeformation")
        b1 = self.EikonalDistance(VariableIDs, self.mesh, self.boundary_parts, 10)
        b2 = self.EikonalDistance(FixedIDs, self.mesh, self.boundary_parts, 10)
        XDMFFile(mpi_comm_world(), "./output/b1.xdmf") << b1
        XDMFFile(mpi_comm_world(), "./output/b2.xdmf") << b2
        CDSolver.Wind = b2*grad(b1)
        CDSolver.HeatConduct = b1*b1*Diffusion#b1*5e-2
        CDSolver.DeltaT = 0.0#1e-2
        CDSolver.NumTimeSteps = 1
        CDSolver.StoreHDD = True
        CDSolver.StoreScalars = False
        CDSolver.DegT = Deg
        CDSolver.StrongBoundaryConditions = True
        Move = Function(VectorFunctionSpace(self.mesh, "CG", Deg))
        (dmin, dmax) = Move.vector().local_range()
        dmin = int(dmin/MaxDim)
        dmax = int(dmax/MaxDim)
        LocValues = numpy.zeros(MaxDim*(dmax-dmin))
        for j in range(MaxDim):
            CDSolver.BoundaryID = []
            CDSolver.BValue = []
            CDSolver.DWeight = []
            CDSolver.NWeight = []
            for i in VariableIDs:
                CDSolver.BoundaryID.append(i)
                CDSolver.BValue.append(V[j])
                CDSolver.DWeight.append(Constant(1.0))
                CDSolver.NWeight.append(Constant(0.0))
            for i in FixedIDs:
                CDSolver.BoundaryID.append(i)
                CDSolver.BValue.append(Constant(0.0))
                CDSolver.DWeight.append(Constant(1.0))
                CDSolver.NWeight.append(Constant(0.0))
            CDSolver.Solve()
            #Modification for Dolfin 1.5 (with Ghost Layers)
            for i in range(CDSolver.t0.vector().local_size()/MaxDim):
            #for i in range(dmax-dmin):
                #LocValues[MaxDim*i+j] = CDSolver.t0.vector()[i+dmin]
                LocValues[MaxDim*i+j] = CDSolver.t0.vector()[i]
                #Move.vector()[MaxDim*i+j] = CDSolver.t0.vector()[i]
        Move.vector().set_local(LocValues)
        Move.vector().apply("")
        return Move

    def StokesDefo(self, mesh, boundary_parts, V):
        import Navier as DefoNavier
        FlowSolver = DefoNavier.Navier(mesh, boundary_parts, "Defo")
        FlowSolver.UseLU = True
        FlowSolver.UseCustomSchur = False#not FlowSolver.UseLU
        FlowSolver.LinearizationType = 1 #Newton: 1, Picard: 0
        FlowSolver.NonLinResidualMomentum = 1.0e-5
        FlowSolver.NonLinResidualMass = 1.0e-5
        FlowSolver.LinResU = 1.0e-5
        FlowSolver.LinResP = 1.0e-5
        FlowSolver.LinMaxiter = 100
        FlowSolver.DeltaT = 0.0#0.1#0.025
        FlowSolver.NumTimeSteps = 1
        FlowSolver.theta=0.5 #1.0: Implicit Euler, 0.0: Explicit Euler
        FlowSolver.Transient = False
        FlowSolver.PrimalStorage = "None"#"HDD"
        FlowSolver.StoreHDD = False
        FlowSolver.rho = 0.0
        FlowSolver.homotopy = 0.0
        FlowSolver.mu = 1.0/1.0#1e-2#1e-2
        FlowSolver.G2Stabilization = False
        FlowSolver.G2Newton = False
        FlowSolver.kappa1 = 5.0e-1
        FlowSolver.kappa2 = 5.0e-1
        FlowSolver.NoslipID = [1,2,3,5,6]
        FlowSolver.InletID = self.VariableBoundary
        FlowSolver.PressureID = [2]
        FlowSolver.FixPressure = True
        FlowSolver.PrimalDual = "Primal"
        FlowSolver.inflow = project(V, VectorFunctionSpace(mesh, "CG", 2))
        FlowSolver.InitStorage()
        FlowSolver.MakeInitialState()
        FlowSolver.solve()
        return FlowSolver.u0
    
    def VolDefo(self, mesh, boundary_parts, V, Stiffness=0.1, rhs=1.0, MaxBoundary=6):
        Stiffness = Constant(Stiffness)
        #dss=ds[boundary_parts]
        #dx=dx[boundary_parts]
        N = VectorFunctionSpace(mesh, 'CG', self.MaxOrder)
        
        v = TestFunctions(N)
        w = TrialFunctions(N)
        a = Stiffness*(inner(grad(v[0]),grad(w[0])) + inner(grad(v[1]),grad(w[1])) + inner(grad(v[2]),grad(w[2])))*dx
        a += (inner(v[0],w[0])+inner(v[1],w[1])+inner(v[2],w[2]))*dx
        l = Constant(rhs)*(V[0]*v[0]+V[1]*v[1]+V[2]*v[2])*dx
        
        Sol = Function(N)
        
        if not boundary_parts == []:
            bc = []
            for i in self.VariableBoundary:
                bc.append(DirichletBC(N, V, boundary_parts, i))
            for i in range(1, MaxBoundary+1):
                if not i == self.VariableBoundary:
                    bc.append(DirichletBC(N, (0.0,0.0,0.0), boundary_parts, i))
            solve(a==l, Sol, bc)
        else:
            print "ComponentLaplaceDefo: APPLYING BCs Typ 2"
            bc0 = DirichletBC(N, V, "on_boundary")
            solve(a==l, Sol, bc0)
        return Sol

    #assume input is a boundary mesh gamma already! Otherwise MaxDim wrong...
    #Computes kappa by kappa*n = SurfLaplace X, with X surface point coordinates
    def ComputeMeanCurvature(self, mesh, gamma, SmoothEps = 0.0):
        deg = 1
        MaxDim = gamma.topology().dim()+1
        SmoothEps = Constant(SmoothEps)
        S = FunctionSpace(gamma, "CG", deg)
        V = VectorFunctionSpace(gamma, "CG", deg, MaxDim)
        #V = FunctionSpace(gamma, "CG", deg)
        if MaxDim == 1:
            X = interpolate(Expression("x[0]", degree=ExprDegree), V)
            Z = Constant(0.0)
        elif MaxDim == 2:
            X = interpolate(Expression(("x[0]", "x[1]"), degree=ExprDegree), V)
            Z = Constant((0.0, 0.0))
        elif MaxDim == 3:
            X = interpolate(Expression(("x[0]", "x[1]", "x[2]"), degree=ExprDegree), V)
            Z = Constant((0.0, 0.0, 0.0))
        #make point normal
        #Face normal
        N = self.SurfaceFaceNormal(mesh)
        ##point normal
        N = self.AverageFaceQuantity(N)
        self.NormalizeVectorField(N)
        
        h = TrialFunction(V)
        v = TestFunction(V)
        a = (SmoothEps*inner(grad(v), grad(h)) + Constant(2.0)*inner(h,v))*dx
        l = inner(grad(v), grad(X))*dx
        HN = Function(V)
        #Enforce Dirichlet Zero on open boundaries...
        bc0 = DirichletBC(V, Z, "on_boundary")
        solve(a==l, HN, bc0)
        #compute norm <--- WRONG! Removes SIGN!
        #multiply with transpose(n) <--- RIGHT!
        H = Function(S)
        (dmin, dmax) = H.vector().local_range()
        LocData = numpy.zeros(dmax-dmin)
        for i in range(0, dmax-dmin):
            value = 0.0
            for j in range(MaxDim):
                data1 = HN.vector()[MaxDim*(i+dmin)+j]
                data2 = N.vector()[MaxDim*(i+dmin)+j]
                value += data1*data2
            #value = sqrt(float(value))
            LocData[i] = value
        H.vector().set_local(LocData)
        H.vector().apply("")
        H.rename("curvature", "dummy")
        return H

    #compute the mean curvature by calculating tangent_div n
    def ComputeDivN(self, mesh, SmoothEps = 0.0, boundary_parts = [], markers = []):
        mesh.init()
        gamma = BoundaryMesh(mesh, "exterior")
        gamma.init()
        if markers != []:
            bp = self.ReduceMeshFunctionToSurface(mesh, boundary_parts)
        else:
            bp = []
        
        N = self.SurfaceFaceNormal(mesh)
        N = self.AverageFaceQuantity(N, bp, markers)
        self.NormalizeVectorField(N)
        
        if markers != []:
            (gamma_sub, dummy) = self.ExtractSubsurface_old(markers, mesh, boundary_parts)
            N = self.ReduceVectorFunctionToSubsurface(N, mesh, gamma_sub, "CG", 1)
        else:
            gamma_sub = gamma
        N = project(N, VectorFunctionSpace(gamma_sub, "CG", 1, 3))
        
        S = FunctionSpace(gamma_sub, "CG", 1)
        u = TrialFunction(S)
        v = TestFunction(S)
        a = Constant(1.0)*dot(v,u)*dx + Constant(SmoothEps)*inner(grad(v), grad(u))*dx
        l = inner(v, Constant(0.0))*dx + inner(v, div(N))*dx
        kappa = Function(S)
        solve(a==l, kappa)

        if markers != []:
            kappa = self.ExpandFunctionFromSubsurface(kappa, gamma, gamma_sub, "CG", 1)
        kappa.rename("Curvature", "dummy")
        return kappa

    def ComputeDivNVolume(self, mesh, SmoothEps, N = None, boundary_parts=None, InteriorEdges=None):
        if N == None:
            N = self.VolumeNormal(mesh, [])
        #plot(N, interactive=True)
        Space = FunctionSpace(mesh, "CG", 1)
        w = TrialFunction(Space)
        v = TestFunction(Space)
        l = v*(div(N)-dot(grad(N)*N,N))*ds
        a = inner(v,w)*dx
        a += (inner(v,w) + SmoothEps*inner(grad(v)-dot(grad(v),N)*N, grad(w)-dot(grad(w),N)*N))*ds
        if boundary_parts != None:
            dS = dolfin.dS(domain=mesh, subdomain_data=boundary_parts)
            for i in InteriorEdges:
                l += avg((v)*((div(N))-dot((grad(N))*N,N)))*dS(i)
                a += avg(inner((v),(w)))*dS(i)
        kappa = Function(Space)
        solve(a==l, kappa)
        #plot(kappa, interactive=True)
        #File("kappa.pvd") << kappa
        kappa.rename("Curvature", "dummy")
        return kappa

    """
    def CurvatureFromSignedDistance(self, SignedDistance, boundary_parts=None, InteriorEdges=None):
        Space = SignedDistance.function_space()
        w = TrialFunction(Space)
        v = TestFunction(Space)
        kappa = Function(Space)
        kappa.rename("kappa", "")
        a = (inner(v, w)+0.001*inner(grad(v), grad(w)))*(dx+ds)
        l = inner(grad(v), grad(SignedDistance))*(dx+ds)
        
        if boundary_parts != None:
            mesh = Space.mesh()
            dS = dolfin.dS(domain=mesh, subdomain_data=boundary_parts)
            for i in InteriorEdges:
                a += jump((inner(v, w)+0.001*inner(grad(v), grad(w))))*dS(i)
                l += jump(inner(grad(v), grad(SignedDistance)))*dS(i)
        
        solve(a==l,kappa)
        plot(kappa, interactive=True)
        return kappa
    """
    
    def DiscreteMeshLaplaceSmooth(self, mesh, gamma, Codim, lam, mu=0.0, FixedBoundary = [], Weights = [], Tangential = False):
        MaxDim = mesh.topology().dim()
        #gamma = BoundaryMesh(mesh, "exterior")
        if Codim == 0:
            MeshToFix = mesh
        if Codim == 1:
            OnCorrectBoundary = self.VertexOnBoundary(FixedBoundary)
            MeshToFix = gamma
        MoveSpaceV = VectorFunctionSpace(MeshToFix, "CG", 1, MaxDim)
        
        MoveNew = Function(MoveSpaceV)
        SizeGlobal = len(MoveNew.vector())
        (dmin, dmax) = MoveNew.vector().local_range()
        dmin = int(dmin/MaxDim)
        dmax = int(dmax/MaxDim)
        LocValues = numpy.zeros(MaxDim*(dmax-dmin))
        #GlobalMove = numpy.zeros(MaxDim*SizeGlobal)
        #for i in range(dmax-dmin):
        #    for j in range(MaxDim):
        #        #LocValues[MaxDim*i+j] = Move.vector()[MaxDim*(i+dmin)+j]
        #        GlobalMove[MaxDim*(i+dmin)+j] = Move.vector()[MaxDim*(i+dmin)+j]
        #GlobalMove = self.SyncSum(GlobalMove)
        d2v = dof_to_vertex_map(FunctionSpace(MeshToFix, "CG", 1))
        NumNeighbour = numpy.zeros(SizeGlobal)
        if Weights != []:
            if len(Weights.vector()) != SizeGlobal/MaxDim:
                self.pprint("Not enough weights for every node...")
                exit()
            GlobalWeights = numpy.zeros(int(SizeGlobal/MaxDim))
            for i in range(dmin, dmax):
                GlobalWeights[i] = Weights.vector()[i]
            self.SyncSum(GlobalWeights)
        Coords = numpy.zeros(MaxDim*SizeGlobal)
        if Tangential == True:
            #face normal
            #gamma2 = BoundaryMesh(mesh, "exterior")
            N = self.SurfaceFaceNormal(mesh)
            #point normal
            N = self.AverageFaceQuantity(N)
            self.NormalizeVectorField(N)
            #Extent to Volume: stay n at boundary and become zero in volume...
            if Codim == 0:
                N = self.ExtentVectorSurfaceFunctionToVolume(mesh, N)
            #GlobalNormal = numpy.zeros(MaxDim*SizeGlobal)
            #for i in range(dmax-dmin):
            #    for j in range(MaxDim):
            #        GlobalNormal[MaxDim*(i+dmin)+j] = N.vector()[MaxDim*(i+dmin)+j]
            #GlobalNormal = self.SyncSum(GlobalNormal)
        #Communicate data
        for v1 in vertices(MeshToFix):
            Index = v1.index()
            IndexGlobal = v1.global_index()
            if Codim == 1:
                if OnCorrectBoundary[IndexGlobal] < 1.0:
                    continue
            neighbors = [];
            if Weights != []:
                WeightSum = 0.0
            for e in edges(v1):
                for j in range(2):
                    if not e.entities(0)[j] == Index:
                        neighbors.append(e.entities(0)[j])
                        if Weights != []:
                            Vert = Vertex(MeshToFix, e.entities(0)[j])
                            MyIndex = Vert.global_index()
                            WeightSum += (1.0 + GlobalWeights[MyIndex])
            NumConnections = len(neighbors)
            NumNeighbour[IndexGlobal] = NumConnections
            if Weights != []:
                NumNeighbour[IndexGlobal] = WeightSum
            for i in range(NumConnections):
                VPartner = Vertex(MeshToFix, neighbors[i])
                PartnerIndex = VPartner.global_index()
                for j in range(MaxDim):
                    x = VPartner.x(j)# + GlobalMove[MaxDim*PartnerIndex+j]
                    if Weights != []:
                        x *= (1.0 + GlobalWeights[PartnerIndex])
                    Coords[MaxDim*IndexGlobal+j] += x
        NumNeighbour = self.SyncSum(NumNeighbour)
        Coords = self.SyncSum(Coords)
        #standard discrete mesh smoothing
        for i in range(0, dmax-dmin):
            v1 = Vertex(MeshToFix, d2v[i])
            Index = v1.index()
            IndexGlobal = v1.global_index()
            if Codim == 1:
                if OnCorrectBoundary[IndexGlobal] < 1.0:
                    continue
            #compute projection factor
            if Tangential == True:
                value = 0.0
                for j in range(MaxDim):
                    x = v1.x(j)
                    value += (Coords[MaxDim*IndexGlobal + j]/NumNeighbour[IndexGlobal] - x)*N.vector()[MaxDim*(i+dmin)+j]
            XNew = [0.0]*MaxDim
            for j in range(MaxDim):
                x = v1.x(j)
                XNew[j] = Coords[MaxDim*IndexGlobal + j]/NumNeighbour[IndexGlobal] - x
                if Tangential == True:
                    XNew[j] -= value*N.vector()[MaxDim*(i+dmin)+j]
                LocValues[MaxDim*i + j] += lam*XNew[j]
        #forward/backward smoothing, aka "Taubin" smoothing
        if mu > 0.0:
            for i in range(0, dmax-dmin):
                v1 = Vertex(MeshToFix, d2v[i])
                Index = v1.index()
                IndexGlobal = v1.global_index()
                if Codim == 1:
                    if OnCorrectBoundary[IndexGlobal] < 1.0:
                        continue
                #compute projection factor
                if Tangential == True:
                    value = 0.0
                    for j in range(MaxDim):
                        x = v1.x(j)# + GlobalMove[MaxDim*IndexGlobal + j]
                        value += (Coords[MaxDim*IndexGlobal + j] - NumNeighbour[IndexGlobal]*x)*N.vector()[MaxDim*(i+dmin)+j]
                XNew = [0.0]*MaxDim
                for j in range(MaxDim):
                    XNew[j] = Coords[MaxDim*IndexGlobal + j]/NumNeighbour[IndexGlobal]
                    if Tangential == True:
                        XNew[j] -= value*N.vector()[MaxDim*(i+dmin)+j]/NumNeighbour[IndexGlobal]
                    Xlam = LocValues[MaxDim*i + j]
                    x = v1.x(j)# + GlobalMove[MaxDim*IndexGlobal + j]
                    LocValues[MaxDim*i + j] -= mu*(XNew[j]-(x + Xlam))
                    #LocValues[MaxDim*i + j] -= mu*(XNew[j]-Xlam)
        elif mu < 0.0:
            self.pprint("Please use mu > 0.0 for backward step")
            exit(1)
        if Tangential == True:
            for i in range(0, dmax-dmin):
                value = 0.0
                for j in range(MaxDim):
                    NX = N.vector()[MaxDim*(i+dmin) + j]
                    MoveX = LocValues[MaxDim*i + j]
                    value = value + NX*MoveX
                for j in range(MaxDim):
                    NX = N.vector()[MaxDim*(i+dmin) + j]
                    MoveX = LocValues[MaxDim*i + j]
                    LocValues[MaxDim*i + j] = MoveX - value*NX
        MoveNew.vector().set_local(LocValues)
        MoveNew.vector().apply("")
        if MaxDim == 1:
            zero = 0.0
        if MaxDim == 2:
            zero = (0.0,0.0)
        if MaxDim == 3:
            zero = (0.0,0.0,0.0)
        for i in FixedBoundary:
            if Codim == 0:
                bc = DirichletBC(MoveSpaceV, zero, self.boundary_parts, i)
                bc.apply(MoveNew.vector())
        #L1B = norm(MoveNew.vector()-Move.vector(), 'linf')
        ##L2B = norm(MoveNew.vector()-Move.vector())
        L1B = norm(MoveNew.vector(), 'linf')
        L2B = norm(MoveNew)
        return (L1B, L2B, MoveNew)
    
    def MakeProcessorMap(self, mesh):
        MaxDim = mesh.topology().dim()
        MyRank = MPI.rank(mpi_comm_world())
        Map = MeshFunction("int", mesh, MaxDim)
        for c in cells(mesh):
            Map.array()[c.index()] = MyRank
        return Map

    #Centroid Voronoi Tessellation (CVT)
    def CVTSmooth(self, mesh, boundary_parts, Codim, Tangential = False, FixPlanes = [], VariableVertices = [], VariableMarker = [], SymmX = [], SymmY = [], SymmZ = []):
        MaxDim = mesh.topology().dim()
        Dim = mesh.topology().dim()-Codim
        MoveSpaceV = VectorFunctionSpace(mesh, "CG", 1, MaxDim)
        MoveNew = Function(MoveSpaceV)
        SizeGlobal = len(MoveNew.vector())
        GlobalCentroids = numpy.zeros(MaxDim*mesh.size_global(0))
        GlobalAreas = numpy.zeros(mesh.size_global(0))
        (dmin, dmax) = MoveNew.vector().local_range()
        dmin = int(dmin/MaxDim)
        dmax = int(dmax/MaxDim)
        if Tangential == True:
            #face normal
            N = self.VolumeNormal(mesh, FixPlanes)
        #interior
        if Codim == 0:
            for v1 in vertices(mesh):
                IndexGlobal = v1.global_index()
                for e in cells(v1):
                    if e.is_ghost() == False:
                        Centroid = numpy.zeros(MaxDim)
                        for v2 in vertices(e):
                            for j in range(MaxDim):
                                Centroid[j] += (1.0/(Dim+1))*v2.x(j)
                        Area = e.volume()
                        GlobalAreas[IndexGlobal] += Area
                        for j in range(MaxDim):
                            GlobalCentroids[MaxDim*IndexGlobal + j] += Area*Centroid[j]
        #boundary
        elif Codim == 1:
            for v1 in vertices(mesh):
                IndexGlobal = v1.global_index()
                if VariableVertices[IndexGlobal] > 0:
                    for e in facets(v1):
                        if e.exterior() == True and e.is_ghost()==False and boundary_parts[e.index()] in VariableMarker:
                            if MPI.size(mpi_comm_world()) == 1:
                                ID = e.index()
                            else:
                                ID = e.global_index()
                            Centroid = numpy.zeros(MaxDim)
                            for v2 in vertices(e):
                                for j in range(MaxDim):
                                    Centroid[j] += (1.0/(Dim+1))*v2.x(j)
                            if MaxDim == 3:
                                Area = Face(mesh, e.index()).area()
                            else:
                                Area = Cell(mesh, e.index()).area()
                            GlobalAreas[IndexGlobal] += Area
                            for j in range(MaxDim):
                                GlobalCentroids[MaxDim*IndexGlobal + j] += Area*Centroid[j]
        self.SyncSum(GlobalCentroids)
        self.SyncSum(GlobalAreas)
        LocMove = numpy.zeros(MaxDim*(dmax-dmin))
        d2v = dof_to_vertex_map(FunctionSpace(mesh, "CG", 1))
        for i in range(0, dmax-dmin):
            v1 = Vertex(mesh, d2v[i])
            if Codim == 0 or VariableVertices[v1.global_index()] > 0:
                IndexGlobal = v1.global_index()
                MyArea = GlobalAreas[IndexGlobal]
                if MyArea > 0.0:
                    for j in range(MaxDim):
                        LocMove[MaxDim*i+j] = (1.0/MyArea)*GlobalCentroids[MaxDim*IndexGlobal + j]
                        LocMove[MaxDim*i+j] -= v1.x(j)
                    if Tangential == True:
                        value = 0.0
                        for j in range(MaxDim):
                            value += LocMove[MaxDim*i+j]*N.vector()[MaxDim*(i)+j]
                        for j in range(MaxDim):
                            LocMove[MaxDim*i+j] -= value*N.vector()[MaxDim*(i)+j]
                elif MyArea == 0.0:
                    self.dprint("Codim %d: AREA PROBLEM!"%Codim)
        MoveNew.vector().set_local(LocMove)
        MoveNew.vector().apply("")
        if MaxDim == 1:
            zero = 0.0
        if MaxDim == 2:
            zero = (0.0,0.0)
        if MaxDim == 3:
            zero = (0.0,0.0,0.0)
        #keep boundaries constant if repairing the interior
        if Codim == 0:
            if SymmX != [] or SymmY != [] or SymmZ != []:
                FixedMarker = list(set(range(1,numpy.max(boundary_parts.array()))) - set(SymmX + SymmY + SymmZ))
                #print FixedMarker
                #print "MAxDIM", MaxDim
                bc = []
                for i in FixedMarker:
                    bc.append(DirichletBC(MoveSpaceV, zero, boundary_parts, i))
                for i in SymmX:
                    bc.append(DirichletBC(MoveSpaceV.sub(0), 0.0, boundary_parts, i))
                for i in SymmY:
                    bc.append(DirichletBC(MoveSpaceV.sub(1), 0.0, boundary_parts, i))
                for i in SymmZ:
                    bc.append(DirichletBC(MoveSpaceV.sub(2), 0.0, boundary_parts, i))
                for MyBC in bc:
                        MyBC.apply(MoveNew.vector())
            else:
                bc = DirichletBC(MoveSpaceV, zero, "on_boundary")
                bc.apply(MoveNew.vector())
        #fix planes:
        for i in FixPlanes:
            def Plane(x, on_boundary):
                return abs(x[i]) < DOLFIN_EPS
            bc = DirichletBC(MoveSpaceV.sub(i), 0.0, Plane)
            bc.apply(MoveNew.vector())
        L1B = norm(MoveNew.vector(), 'linf')
        L2B = norm(MoveNew)
        #for i in range(MoveNew.vector().local_size()):
        #    self.dprint("Component %e"%MoveNew.vector()[i])
        #plot(MoveNew, interactive=True)
        return (L1B, L2B, MoveNew)

    #same as curvature flow with implicit euler timestepping
    def ContinuousMeshLaplaceSmooth(self, gamma, lam):
        V = VectorFunctionSpace(gamma, "CG", 1, 3)
        X = interpolate(Expression(("x[0]", "x[1]", "x[2]"), element=V.ufl_element()), V)
        X_next = TrialFunction(V)
        eta = TestFunction(V)
        a = inner(X_next, eta)*dx + lam*inner(grad(X_next), grad(eta))*dx
        l = inner(X,eta)*dx
        bc0 = DirichletBC(V, X, "on_boundary")
        X2 = Function(V)
        solve(a==l, X2, bc0)
        return project(X2-X,V)
    
    def DiscreteMeshRepair(self, mesh, boundary_parts, SmoothVolume, SmoothBoundary, Tangential = True, MaxIter = 99999, Step = 1.0, Stop = 1e-6, FixPlanes = [], VariableBoundary = [[1,2,3,4,5,6,7,8,9,10]], SymmX = [], SymmY = [], SymmZ = [], Vis = False, InitField = [], Curvature = []):
        #self.pprint("Start Repair")
        #Determine indices of fixes vertices
        #this will preserve edges between regions of different VariableBoundary values in boundary_parts
        OnVariable = []
        for i in range(len(VariableBoundary)):
            OnVariable.append(self.VertexOnBoundary(mesh, boundary_parts, VariableBoundary[i], True, SymmX+SymmY+SymmZ))
        #OnSymmetry = self.VertexOnBoundary(mesh, boundary_parts, SymmX+SymmY+SymmZ, False)
        
        CopyMesh = Mesh(mesh)
        CopyBP = MeshFunction("size_t", CopyMesh, CopyMesh.geometry().dim()-1)
        for i in range(len(boundary_parts.array())):
            CopyBP.array()[i] = boundary_parts.array()[i]
        if InitField != []:
            CopyMesh.move(InitField)
        Move = Function(VectorFunctionSpace(mesh, "CG", 1))
        DeltaMove = Function(VectorFunctionSpace(CopyMesh, "CG", 1))
        L1B = 0.0; L2B = 0.0
        if SmoothVolume == True:
            (L1Bv, L2Bv, DeltaMovev) = self.CVTSmooth(CopyMesh, CopyBP, 0, Tangential, FixPlanes, [], [])#, SymmX, SymmY, SymmZ)
            DeltaMove = self.AddVectors(DeltaMove, DeltaMovev, 1.0, Step)
            L1B = L1B + L1Bv
            L2B = L2B + L2Bv
        if SmoothBoundary == True:
            for j in range(len(VariableBoundary)):
                (L1Bb, L2Bb, DeltaMoveb) = self.CVTSmooth(CopyMesh, CopyBP, 1, Tangential, FixPlanes, OnVariable[j], VariableBoundary[j])
                DeltaMove = self.AddVectors(DeltaMove, DeltaMoveb, 1.0, Step)
                L1B = L1B + L1Bb
                L2B = L2B + L2Bb
        i = 0
        if Vis:
            self.pprint("Repair Visalization on")
            #VisFile = XDMFFile(mpi_comm_world(), "tmp/ConvertDefo.xdmf")
            #VisFile.parameters["rewrite_function_mesh"] = True
            #VisFile.parameters["flush_output"] = True
            VisFile = File("tmp/ConvertDefo.pvd", "compressed")
            VisFile << (DeltaMove, float(i))
            #exit(0)
        self.pprint("Mesh Smooth: %4d, L1B = %e, L2B = %e"%(i, L1B, L2B))
        CopyMesh.move(DeltaMove)
        Move = self.AddVectors(Move, DeltaMove)
        while L1B > Stop and i < MaxIter:
            i+=1
            L1B = 0.0; L2B = 0.0
            DeltaMove = Function(VectorFunctionSpace(CopyMesh, "CG", 1))
            if SmoothVolume == True:
                (L1Bv, L2Bv, DeltaMovev) = self.CVTSmooth(CopyMesh, CopyBP, 0, Tangential, FixPlanes, [], [])#, SymmX, SymmY, SymmZ)
                DeltaMove = self.AddVectors(DeltaMove, DeltaMovev, 1.0, Step)
                L1B = L1B + L1Bv
                L2B = L2B + L2Bv
            if SmoothBoundary == True:
                for j in range(len(VariableBoundary)):
                    (L1Bb, L2Bb, DeltaMoveb) = self.CVTSmooth(CopyMesh, CopyBP, 1, Tangential, FixPlanes, OnVariable[j], VariableBoundary[j])
                    DeltaMove = self.AddVectors(DeltaMove, DeltaMoveb, 1.0, Step)
                    L1B = L1B + L1Bb
                    L2B = L2B + L2Bb
            self.pprint("Mesh Smooth: %4d, L1B = %e, L2B = %e"%(i, L1B, L2B))
            if Vis:
                VisFile << (DeltaMove, float(i))
            CopyMesh.move(DeltaMove)
            Move = self.AddVectors(Move, DeltaMove)
            Move.rename("Repair", "dummy")
        Move.rename("Repair", "dummy")
        return (L1B, L2B, Move)

    def MyDiscreteMeshRepair(self, mesh, gamma, lam, tol, MaxIter=100):
        MaxDim = 3
        MoveSpaceS = FunctionSpace(gamma, "CG", 1)
        MoveSpaceV = VectorFunctionSpace(gamma, "CG", 1, MaxDim)
        #return value
        Move = Function(MoveSpaceV)
        meshCopy = Mesh(mesh)
        meshCopy.init()
        meshCopy.order()
        gammaCopy = BoundaryMesh(meshCopy, "exterior")
        gammaCopy.init()
        gammaCopy.order()

        #face normal
        n = self.SurfaceFaceNormal(mesh)
        #point normal
        n = self.AverageFaceQuantity(n)
        self.NormalizeVectorField(n)
        DOFs = vertex_to_dof_map(MoveSpaceS)
        Offset = Function(MoveSpaceV)

        #turn all coordinates into a vector and calculate N (=patch size)
        GT= gamma.topology()
        NumVertex = GT.size_global(0)
        MyCoordinates = numpy.zeros([NumVertex, MaxDim])
        MyCopyCoordiantes = numpy.zeros([NumVertex, MaxDim])
        MyNeighbours = [[] for k in range(NumVertex)]
        for v1 in vertices(gamma):
            Index = v1.index()
            for j in range(MaxDim):
                MyCoordinates[Index, j] = v1.x(j)
                MyCopyCoordiantes[Index, j] = v1.x(j)
            for e in edges(v1):
                for j in range(2):
                    if not e.entities(0)[j] == Index:
                        MyNeighbours[Index].append(e.entities(0)[j])

        #calculate initial badness:
        #Offset = numpy.zeros([NumVertex, MaxDim])
        x_c = numpy.zeros([NumVertex, MaxDim])
        MyInnerDerivative = numpy.zeros([NumVertex, MaxDim, MaxDim])
        for k in range(NumVertex):
            #compute average in tangent plane
            N = len(MyNeighbours[k])
            #calculate center in tangent plane
            x_k = MyCoordinates[k]
            n_k = numpy.empty(MaxDim)
            #calcuate tangent center
            for j in range(MaxDim):
                n_k[j] = n.vector()[MaxDim*int(DOFs[k]) + j]
            for i in range(N):
                x_i = MyCoordinates[MyNeighbours[k][i]]
                x_c[k] = x_c[k] + (1.0/N)*(x_i - numpy.dot(x_i-x_k,n_k)*n_k)
            #tangent center has been calculated
            for j in range(MaxDim):
                Offset.vector()[MaxDim*DOFs[k]+j] = x_k[j]-x_c[k][j]
            #print "x_k", x_k, "n_k", n_k, "N", N, "x_c", x_c
            #compute gradient
            for j in range(MaxDim):
                e = numpy.zeros(MaxDim)
                e[j] = 1.0
                #create inner derivative of center node
                MyInnerDerivative[k][j] = MyInnerDerivative[k][j] + e - numpy.dot(e, n_k)*n_k
                #create inner derivative of halo nodes
                for i in range(N):
                    MyInnerDerivative[MyNeighbours[k][i]][j] = MyInnerDerivative[MyNeighbours[k][i]][j] + (1.0/N)*(e - numpy.dot(e, n_k)*n_k)
            #inner derivative computed

        #remove fixed nodes from norm
        Offset = self.InjectOnSubSurface(Offset, mesh, self.VariableBoundary)
        #bc = []
        #for j in range(1, self.NumBoundaries):
        #  if not j == self.VariableBoundary:
                #bc = DirichletBC(MoveSpaceV, (0.0,0.0,0.0), self.boundary_parts, j)
                #bc.apply(Offset.vector())

        Badness = 0.0
        for k in range(NumVertex):
            for j in range(MaxDim):
                Badness = Badness + 0.5*numpy.dot(Offset.vector()[MaxDim*k + j], Offset.vector()[MaxDim*k + j])
        #start projection loop
        count = 0
        print "Repair:", count, "Step:", lam, "L2-Badness:", Badness, "L1-Badness:", max(Offset.vector())
        #L2-stopping
        #while Badness > tol and count < MaxIter:
        #L1-stopping
        while max(Offset.vector()) > tol and count < MaxIter:
            count = count + 1
            #compute gradient
            MyGradient = Function(MoveSpaceV)
            for k in range(NumVertex):
                x_k = MyCoordinates[k]
                OuterDerivative = numpy.zeros(MaxDim)
                for j in range(MaxDim):
                    OuterDerivative[j] = Offset.vector()[MaxDim*DOFs[k] + j]
                for j in range(MaxDim):
                    MyGradient.vector()[MaxDim*DOFs[k]+j] = numpy.dot(OuterDerivative,MyInnerDerivative[k])[j]

            #remove fixed nodes
            MyGradient = self.InjectOnSubSurface(MyGradient, mesh, self.VariableBoundary)
            #for j in range(1, self.NumBoundaries):
                #if not j == self.VariableBoundary:
                    #bc = DirichletBC(MoveSpaceV, (0.0,0.0,0.0), self.boundary_parts, j)
                    #bc.apply(MyGradient.vector())

            #update coordinates / gradient descent
            for k in range(NumVertex):
                for j in range(MaxDim):
                    MyCoordinates[k][j] = MyCoordinates[k][j] - lam*MyGradient.vector()[MaxDim*DOFs[k]+j]

            #caluclate new badness and derivatives
            x_c = numpy.zeros([NumVertex, MaxDim])
            MyInnerDerivative = numpy.zeros([NumVertex, MaxDim, MaxDim])
            #interate over all center nodes
            for k in range(NumVertex):
                #compute average in tangent plane
                N = len(MyNeighbours[k])
                #calculate center in tangent plane
                x_k = MyCoordinates[k]
                n_k = numpy.empty(MaxDim)
                #calcuate tangent center
                for j in range(MaxDim):
                    n_k[j] = n.vector()[MaxDim*int(DOFs[k]) + j]
                for i in range(N):
                    x_i = MyCoordinates[MyNeighbours[k][i]]
                    x_c[k] = x_c[k] + (1.0/N)*(x_i - numpy.dot(x_i-x_k,n_k)*n_k)
                #tangent center has been calculated
                for j in range(MaxDim):
                    Offset.vector()[MaxDim*DOFs[k]+j] = x_k[j]-x_c[k][j]
                #print "x_k", x_k, "n_k", n_k, "N", N, "x_c", x_c
                #compute gradient
                for j in range(MaxDim):
                    e = numpy.zeros(MaxDim)
                    e[j] = 1.0
                    #create inner derivative of center node
                    MyInnerDerivative[k][j] = MyInnerDerivative[k][j] + e - numpy.dot(e, n_k)*n_k
                    #create inner derivative of halo nodes
                    for i in range(N):
                        MyInnerDerivative[MyNeighbours[k][i]][j] = MyInnerDerivative[MyNeighbours[k][i]][j] + (1.0/N)*(e - numpy.dot(e, n_k)*n_k)
                #inner derivative computed
            #end loop all vertices

            #remove fixed nodes from norm
            Offset = self.InjectOnSubSurface(Offset, mesh, self.VariableBoundary)
            #bc.apply(Offset.vector())
            BadnessNew = 0.0
            for k in range(NumVertex):
                for j in range(MaxDim):
                    BadnessNew = BadnessNew + 0.5*numpy.dot(Offset.vector()[MaxDim*k + j], Offset.vector()[MaxDim*k + j])
            if BadnessNew > Badness:
                print "MESH REPAIR:", count, "REDUCING STEP"
                print "BadnessOld:", Badness, "BadnessNew", BadnessNew
                lam = 0.5*lam
            else:
                Badness = BadnessNew
                print "Repair:", count, "Step:", lam, "L2-Badness:", Badness, "L1-Badness:", max(Offset.vector())
                #calculate new normals
                #easiest by using gammaCopy...
                for v1 in vertices(gammaCopy):
                    Index = v1.index()
                    for j in range(MaxDim):
                        gammaCopy.coordinates()[Index][j] = MyCoordinates[Index, j]
                gammaCopy.init()

                #update normals
                #print "Step Accepted"
                #make new point normal
                #n = self.SurfaceFaceNormal(mesh, gammaCopy)
                n = self.SurfaceFaceNormal(meshCopy)
                n = self.AverageFaceQuantity(n)
                self.NormalizeVectorField(n)
        #while loop terminated
        #mesh repaired
        for k in range(NumVertex):
            for j in range(MaxDim):
                Move.vector()[MaxDim*DOFs[k]+j] = MyCoordinates[k, j] - MyCopyCoordiantes[k, j]
        return (Badness, max(Offset.vector()), Move)

    def FixMeshFunctionParallel(self, mf):
        self.pprint("ShapeOpt: FixMeshFunctionParallel")
        Max = mf.array().max()
        Min = mf.array().min()
        ErrorLow = -1
        ErrorHigh = 1000
        if Max > ErrorHigh or Min < ErrorLow:
            mf.mesh().init()
            for i in range(len(mf.array())):
                value = mf.array()[i]
                if value > ErrorHigh or value < ErrorLow:
                    #Ent = MeshEntity(mf.mesh(), mf.dim(), i)
                    Ent = Facet(mf.mesh(), i)
                    if(Ent.exterior() == False):
                        mf.array()[i] = 0
                        #print "Parallel Dolfin DG Hack: Iterior Facet set to zero"
                    else:
                        nvalues = mf.array()[Ent.entities(mf.dim())]
                        GoodValues = []
                        for j in nvalues:
                            if j > ErrorLow and j < ErrorHigh and j != 0:
                                GoodValues.append(j)
                        if len(GoodValues) > 0:
                            def most_common(L):
                                import itertools
                                groups = itertools.groupby(sorted(L))
                                def _auxfun((item, iterable)):
                                    return len(list(iterable)), -L.index(item)
                                return max(groups, key=_auxfun)[0]
                            OkValue = most_common(GoodValues)
                            #print "Parallel Dolfin DG Hack: MeshFunction Entry: ",i, "to", OkValue
                            mf.array()[i] = OkValue
                        else:
                            mf.array()[i] = 0
