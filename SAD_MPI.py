from dolfin import *
import numpy

#parallel output:
from mpi4py import MPI as MPI4PY

#print from rank 0
def pprint(string, only=0):
    rank = MPI.rank(mpi_comm_world())
    #MPISize = MPI.size(mpi_comm_world())
    if rank == only:
        print string

#print all ranks unsorted
def dprint(string):
    comm = MPI4PY.COMM_WORLD
    rank = comm.Get_rank()
    print ("Rank %d:"%rank)+" "+string

#print all ranks sorted
def sprint(string):
    comm = MPI4PY.COMM_WORLD
    data = comm.gather(string)
    if comm.Get_rank() == 0:
        print data

def PrintVectorInGlobalSorting(t):
    comm = MPI4PY.COMM_WORLD
    for i in range(comm.Get_size()):
        if comm.Get_rank() == i:
            print "Indices from Rank", i
            for j in range(t.vector().local_size()):
                print t.vector()[j]
        comm.Barrier()

def PrintArrayInGlobalSorting(t):
    comm = MPI4PY.COMM_WORLD
    for i in range(comm.Get_size()):
        if comm.Get_rank() == i:
            print "Indices from Rank", i
            print t
        comm.Barrier()

def SyncAllGather(values):
    MySize = numpy.shape(values)[0]
    NewShape = [MPI.size(mpi_comm_world())]
    for i in range(len(numpy.shape(values))):
        NewShape.append(numpy.shape(values)[i])
    if MPI.size(mpi_comm_world()) > 1:
        from mpi4py import MPI as MPI4PY
        comm = MPI4PY.COMM_WORLD
        valueAllProc = numpy.zeros(NewShape)
        vcopy = numpy.array(values)
        comm.Allgather(vcopy, valueAllProc)
        return valueAllProc
    else:
        valueAllProc = values.reshape(NewShape)
        return valueAllProc

def SyncSum(LocValues):
    if MPI.size(mpi_comm_world()) > 1:
        from mpi4py import MPI as MPI4PY
        comm = MPI4PY.COMM_WORLD
        MPI4PYRank = comm.Get_rank()
        MPI4PYSize = comm.Get_size()
        Shape = LocValues.shape
        if len(Shape) > 1:
            LocValues = LocValues.reshape(LocValues.size)
        MySize = long(len(LocValues))
        NormalsAllProcs = numpy.zeros(MPI4PYSize*MySize, dtype=LocValues.dtype)
        comm.Allgather(LocValues, NormalsAllProcs)
        #MPI.gather(mpi_comm_world(), LocValues, NormalsAllProcs)
        for i in range(MySize):
            LocValues[i] = 0.0
            for j in range(MPI4PYSize):
                LocValues[i] = LocValues[i] + NormalsAllProcs[MySize*j + i]
        if len(Shape) > 1:
            LocValues = LocValues.reshape(Shape)
        return LocValues
    else:
        return LocValues

def SyncMin(LocValues):
    if MPI.size(mpi_comm_world()) > 1:
        from mpi4py import MPI as MPI4PY
        comm = MPI4PY.COMM_WORLD
        MPI4PYRank = comm.Get_rank()
        MPI4PYSize = comm.Get_size()
        Shape = LocValues.shape
        if len(Shape) > 1:
            LocValues = LocValues.reshape(LocValues.size)
        MySize = long(len(LocValues))
        NormalsAllProcs = numpy.zeros(MPI4PYSize*MySize, dtype=LocValues.dtype)
        comm.Allgather(LocValues, NormalsAllProcs)
        #MPI.gather(mpi_comm_world(), LocValues, NormalsAllProcs)
        for i in range(MySize):
            LocValues[i] = 0.0
            for j in range(MPI4PYSize):
                MyValue = NormalsAllProcs[MySize*j + i]
                if MyValue < LocValues[i]:
                    LocValues[i] = MyValue
        if len(Shape) > 1:
            LocValues = LocValues.reshape(Shape)
        return LocValues
    else:
        return LocValues

def GlobalizeMesh(mesh):
    mesh.init()
    #plot(mesh, interactive=True)
    TopDim = mesh.topology().dim()
    GeoDim = mesh.geometry().dim()
    NumVertexGlobal = mesh.size_global(0)
    NumCellsGlobal = mesh.size_global(TopDim)
    coords = mesh.coordinates()
    CoordsGlobal = numpy.zeros([NumVertexGlobal, GeoDim])
    NumProcGlobal = numpy.zeros(NumVertexGlobal)
    for v in vertices(mesh):
        x = numpy.array(coords[v.index()])
        CoordsGlobal[v.global_index(),:] = x
        NumProcGlobal[v.global_index()] = 1.0
    SyncSum(CoordsGlobal)
    SyncSum(NumProcGlobal)
    if min(NumProcGlobal==0):
        self.dprint("ERROR: THERE IS A VERTEX NOT BELONGING TO ANY PROCESSOR")
        exit(1)
    for IndexGlobal in range(NumVertexGlobal):
        CoordsGlobal[IndexGlobal,:] = CoordsGlobal[IndexGlobal,:]/NumProcGlobal[IndexGlobal]
    CellsGlobal = numpy.zeros([NumCellsGlobal, TopDim+1], dtype=numpy.uintp)
    for c in cells(mesh):
        LocVertexID = 0
        for v in vertices(c):
            CellsGlobal[c.global_index(), LocVertexID] = v.global_index()
            LocVertexID += 1
    SyncSum(CellsGlobal)
    #New: Also globalize Facets:
    mesh.init_global(TopDim-1)
    NumFacetsGlobal = mesh.size_global(TopDim-1)
    NumProcFacets = numpy.zeros(NumFacetsGlobal)
    FacetsGlobal = numpy.zeros([NumFacetsGlobal, TopDim], dtype=numpy.uintp)
    for f in facets(mesh):
        LocVertexID = 0
        #For some VERY strange reason, faces in SymmetricMesh segfault upon global_index() call
        #And ONLY on 1 processor...
        if MPI.size(mpi_comm_world()) == 1:
            f_index = f.index()
        else:
            f_index = f.global_index()
        for v in vertices(f):
            FacetsGlobal[f_index, LocVertexID] = v.global_index()
            LocVertexID += 1
        NumProcFacets[f_index] = 1.0
    SyncSum(FacetsGlobal)
    SyncSum(NumProcFacets)
    for IndexGlobal in range(NumFacetsGlobal):
        FacetsGlobal[IndexGlobal,:] = FacetsGlobal[IndexGlobal,:]/NumProcFacets[IndexGlobal]
    return (CoordsGlobal, CellsGlobal, FacetsGlobal)

def GlobalizeMeshFunction(mf):
    MaxDim = mf.dim()
    mesh = mf.mesh()
    GlobalValues = numpy.zeros(mesh.size_global(MaxDim), dtype=numpy.uintp)
    GlobalNumProc = numpy.zeros(mesh.size_global(MaxDim), dtype=numpy.uintp)
    for e in entities(mesh, MaxDim):
        IDL = e.index()
        #strange bug in mesh generator meshes...
        if MPI.size(mpi_comm_world()) == 1 and MaxDim == 2:
            IDG = IDL
        else:
            IDG = e.global_index()
        GlobalValues[IDG] = mf.array()[IDL]
        GlobalNumProc[IDG] += 1
    SyncSum(GlobalValues)
    SyncSum(GlobalNumProc)
    return GlobalValues/GlobalNumProc
