# -*- coding: utf-8 -*-
from dolfin import *
from ufl import differentiation

from ufl.assertions import ufl_assert
from ufl.log import error

from ufl.core.terminal import Terminal
from ufl.core.multiindex import MultiIndex, Index, FixedIndex, indices

from ufl.tensors import as_tensor, as_scalar, as_scalars, unit_indexed_tensor, unwrap_list_tensor

from ufl.classes import ConstantValue, Identity, Zero, FloatValue
from ufl.classes import Coefficient, FormArgument, ReferenceValue
from ufl.classes import Grad, ReferenceGrad, Variable
from ufl.classes import Indexed, ListTensor, ComponentTensor
from ufl.classes import ExprList, ExprMapping
from ufl.classes import Product, Sum, IndexSum
from ufl.classes import Jacobian, JacobianInverse
from ufl.classes import SpatialCoordinate

from ufl.constantvalue import is_true_ufl_scalar, is_ufl_scalar
from ufl.operators import (dot, inner, outer, lt, eq, conditional, sign,
    sqrt, exp, ln, cos, sin, tan, cosh, sinh, tanh, acos, asin, atan, atan_2,
    erf, bessel_J, bessel_Y, bessel_I, bessel_K,
    cell_avg, facet_avg)

from math import pi

from ufl.corealg.multifunction import MultiFunction
from ufl.corealg.map_dag import map_expr_dag
from ufl.algorithms.map_integrands import map_integrand_dags

from ufl import checks# import is_cellwise_constant

import SAD_derivatives
from SAD_tangent_differentiation import div_tan

class TestOperator(MultiFunction):
    def __init__(self):
        MultiFunction.__init__(self)
    
    #expr = MultiFunction.reuse_if_untouched
    def expr(self, o):
        return None
    
    def terminal(self, o):
        return "SAD_TERMINAL"
    
    #def coefficient(self, o):
    #    return "SAD_COEFFICIENT"
    
    def list_tensor(self, o):#, *dops):
        return "SAD_LIST_TENSOR"
    
    #def derivative(self, o):
    #    return "SAD_DERIVATIVE"
    
    def variable_derivative(self, o):
        return "SAD_VARIABLE_DERIVATIVE"
    
    #def coefficient_derivative(self, o):
    #    return "SAD_COEFFICIENT_DERIVATIVE"
    
    def grad(self, o):
        return "SAD_GRADIENT"
    
    def div(self, o):
        return "SAD_DIVERGENCE"
    
    def transposed(self, o):
        return "SAD_TRANSPOSED"
    
    def component_tensor(self, o):
        return "SAD_COMPONENT_TENSOR"

    def sum(self, o):
        return "SAD_SUM"
    
    def indexed(self, o):
        return "SAD_INDEXED"
    
    def index_sum(self, o):
        return "SAD_INDEX_SUM"
    
    def product(self, o):
        return "SAD_PRODUCT"

    def inner(self, o):
        return "SAD_INNER"
    
    def dot(self, o):
        return "SAD_DOT"

    def outer(self, o):
        return "SAD_OUTER"

    def trace(self, o):
        return "SAD_TRACE"
"""
    def div_tan(self, o):
        return "SAD_DIV_TAN"
"""

def GetMyOp(MyTest):
    if MyTest == "SAD_PRODUCT":
        return Product
    if MyTest == "SAD_INNER":
        return inner
    if MyTest == "SAD_DOT":
        return dot

#Turn
#[{ A | A_{i_{14}} = (grad(q))[0, i_{14}] }, { A | A_{i_{15}} = (grad(q))[1, i_{15}] }]
#into grad([q[0], q[1]])
def RecombineListTensor(o):
    #print ""
    #print "Recombine List Tensor:"
    #print o
    if len(o.ufl_operands) == 0:
        return o
    OperatorTest = TestOperator()
    MyArgument = []
    MyIndex = []
    for a in o.ufl_operands:
        AType = OperatorTest(a)
        #print "Recombine: Processing", a
        #print "AType", AType
        if AType != "SAD_COMPONENT_TENSOR":
            #print "Recombine List Tensor: Return:", o
            return o
        b = a.ufl_operands[0]
        BType = OperatorTest(b)
        print "BType", BType
        if BType != "SAD_INDEXED":
            return o
        c, IndexC = b.ufl_operands
        CType = OperatorTest(c)
        print "CType", CType
        if CType != "SAD_GRADIENT":
            return o
        IndexA = a.ufl_operands[1]
        IndexB = b.ufl_operands[1]
        if len(IndexB) != 2:
            return o
        if str(IndexA) == str(IndexB[1]):
            MyArgument.append(c.ufl_operands[0])
            MyIndex.append(IndexB[0])
    MyReturn = []
    for i in range(len(MyArgument)):
        Arg = MyArgument[i]
        ind = MyIndex[i]
        MyReturn.append(Arg[ind])
    MyReturn = grad(as_vector(MyReturn))
    #print "Recombine List Tensor: Return:", MyReturn
    #print ""
    return MyReturn

#analyze componente tensor a and pull out scalars
def TensorPullOutScalars(a):
    #print "TensorPullOutScalars"
    OperatorTest = TestOperator()
    a0, A0Index = a.ufl_operands
    #print "Tensor:", a0, A0Index
    TensorType = OperatorTest(a0)
    #print "Tensor Type", TensorType
    if TensorType == "SAD_PRODUCT":
        a1, b1 = a0.ufl_operands
        a1_Type = OperatorTest(a1)
        b1_Type = OperatorTest(b1)
        #print "Tensor Operand 0", a1, a1_Type
        #print "Tensor Operand 1", b1, b1_Type
        #Rausziehen von Scalaren:
        #WARNING: UFL Indexed has lengh one and can be confused with scalars
        if a1_Type == "SAD_INDEXED":# and b1_Type != "SAD_INDEXED":
            a10, IndexA10 = a1.ufl_operands
            #print "A: len1, len2:", len(b1.ufl_shape), len(a10.ufl_shape)
            #print "a10:", a10
            #print "IndexA10:", IndexA10, len(IndexA10)
            if len(b1.ufl_shape) == 0 and str(IndexA10) == str(A0Index):
                return [b1, a10]
        if b1_Type == "SAD_INDEXED":# and a1_Type != "SAD_INDEXED":
            b10, IndexB10 = b1.ufl_operands
            #print "B: len1, len2:", len(a1.ufl_shape), len(b10.ufl_shape)
            #if len(b10.ufl_shape) != 0:
            if len(a1.ufl_shape) == 0 and str(IndexB10) == str(A0Index):
                return [a1, b10]
    return [as_ufl(1.0), a]

#analyze componente tensor a and check if the distributive rule can be applied
def TensorDistributiveLaw(a):
    #print "TensorDistributiveLaw"
    OperatorTest = TestOperator()
    a0, A0Index = a.ufl_operands
    #print "Tensor:", a0, A0Index
    TensorType = OperatorTest(a0)
    #print "Tensor Type", TensorType
    if TensorType == "SAD_PRODUCT":
        a1, b1 = a0.ufl_operands
        a1_Type = OperatorTest(a1)
        b1_Type = OperatorTest(b1)
        #print "Tensor Operand 0", a1, a1_Type
        #print "Tensor Operand 1", b1, b1_Type
        #actual tensor treatment
        if a1_Type == "SAD_INDEXED":
            #print "TEST TEST 2"
            a10, a1Index = a1.ufl_operands
            #print "a10:", a10
            #print "a1Index:", a1Index
            a10_Type = OperatorTest(a10)
            #print "a10_Type:", a10_Type
            if a10_Type == "SAD_SUM":
                aO1, aO2 = a10.ufl_operands
                #print "Sum1", aO1
                #print "Sum2", aO2
                return aO1*b1 + aO2*b1
        if b1_Type == "SAD_INDEXED":
            #print "TEST TEST 3"
            b10, b1Index = b1.ufl_operands
            #print "b10:", b10
            #print "b1Index:", b1Index
            b10_Type = OperatorTest(b10)
            #print "b10_Type:", b10_Type
            if b10_Type == "SAD_SUM":
                aSum1, aSum2 = b10.ufl_operands
                #print "Sum1", aSum1
                #print "Sum2", aSum2
                return a1*aSum1 + a1*aSum2
    #print "TensorDistributiveLaw: No Change"
    return a

#test if tensor serves as a transposition (of Matrix) and then flip dot product order
def TensorHiddenTransposedDot(o):
    OperatorTest = TestOperator()
    MyType = OperatorTest(o)
    if MyType == "SAD_COMPONENT_TENSOR" and len(o.ufl_shape) == 2:
        a, AIndex = o.ufl_operands
        #print "HiddenTranspose", a, a.ufl_shape
        #MyType = OperatorTest(o)
        #if MyType == "SAD_COMPONENT_TENSOR":
        #    a, IndexA = o.ufl_operands
        TypeA = OperatorTest(a)
        if TypeA == "SAD_INDEXED":
            a0, A0Index = a.ufl_operands
            #print a0, a0.ufl_shape
            if len(a0.ufl_shape) == 2:
                if AIndex[0] == A0Index[1] and AIndex[1] == A0Index[0]:
                    return transpose(a0)
    return None

def MySwap(a,b):
    tmp = a
    a = b
    b = tmp
    return (a,b)

#forward simplification:
def SimplifyForm(o):
    OperatorTest = TestOperator()
    #print ""
    #print "SIMPLIFY: START ON:", o
    MyTest = OperatorTest(o)
    #print "SIMPLIFY Type:", MyTest
    if MyTest == "SAD_SUM":
        a, b = o.ufl_operands
        return SimplifyForm(a) + SimplifyForm(b)
    
    if MyTest == "SAD_GRADIENT":
        a = o.ufl_operands[0]
        InnerType = OperatorTest(a)
        #print "Gradient Operand Type:", InnerType, "Expr:", a
        #grad(inner(a,b)) = dot(grad(a),b) + dot(grad(b),a)
        if InnerType == "SAD_INNER" or InnerType == "SAD_DOT":
            a, b = a.ufl_operands
            #dot product of vectors... same as inner product
            if len(a.ufl_shape) <= 1 and len(b.ufl_shape) <= 1:
                MyReturn = dot(b,Grad(a)) + dot(a,Grad(b))
                #print ""
                #print "Gradient: Grad of Inner/Dot: Return:", MyReturn
                #Symp = SimplifyForm(dot(b,Grad(a))) + SimplifyForm(dot(a,Grad(b)))
                #print "After Simplification:"
                #print "Symp:", Symp
                #print ""
                return SimplifyForm(dot(b,Grad(a))) + SimplifyForm(dot(a,Grad(b)))
            return o
        #grad(a*b) = grad(a)*b + a*grad(b)
        if InnerType == "SAD_PRODUCT": #1D
            a0, a1 = a.ufl_operands
            #print "Gradient of Product:"
            #print a0, checks.is_globally_constant(a0)
            #print a1, checks.is_globally_constant(a1)
            if checks.is_globally_constant(a0) and not checks.is_globally_constant(a1):
                MyReturn = a0*grad(a1)
                #print "Return 1:", MyReturn
                #print ""
                return a0*SimplifyForm(grad(a1))
            if checks.is_globally_constant(a1) and not checks.is_globally_constant(a0):
                MyReturn = a1*grad(a0)
                #print "Return 2:", MyReturn
                #print ""
                return a1*SimplifyForm(grad(a0))
            if checks.is_globally_constant(a0) == False and checks.is_globally_constant(a1) == False:
                #MyReturn = Grad(a0)*a1 + Grad(a1)*a0
                #print "Return 3:", MyReturn
                #print ""
                return SimplifyForm(Grad(a0))*a1 + SimplifyForm(Grad(a1))*a0
            #print "Gradient: Default Return", o
            #print ""
            return o
        #grad(a+b) = grad(a)+grad(b)
        if InnerType == "SAD_SUM":
            #print "SUM!! IN GRAD!"
            a0, a1 = a.ufl_operands
            return SimplifyForm(Grad(a0)) + SimplifyForm(Grad(a1))
        #grad(a*V) = a*grad(V) + V*grad(a)^T = a*grad(V) + outer(V,grad(a))
        #Scalar*Vector appears as component tensor in UFL
        if InnerType == "SAD_COMPONENT_TENSOR":
            #print "Component Tensor in Grad"
            #print o
            a1, IndexA = a.ufl_operands
            #print "A:", a1
            #print "B:", IndexA
            TensorType = OperatorTest(a1)
            #print TensorType
            if TensorType == "SAD_PRODUCT":
                a2, b2 = a1.ufl_operands
                A2Type = OperatorTest(a2)
                B2Type = OperatorTest(b2)
                #print "A2:", A2Type, a2
                if B2Type == "SAD_INDEXED":
                    tmp1 = a2
                    a2 = b2
                    b2 = tmp1
                    tmp2 = A2Type
                    A2Type = B2Type
                    B2Type = tmp2
                if A2Type == "SAD_INDEXED":
                    a3, IndexA3 = a2.ufl_operands
                    #print "a3", a3
                    #print IndexA3
                    if IndexA == IndexA3: #case: grad(b*V) = DV*b + V*grad(b)^T <<<=== CHECK!
                        #print "b2:", b2
                        #print "a3:", a3
                        #print "b2:", b2
                        isconstant = checks.is_globally_constant(b2)
                        #print "isconstant", isconstant
                        if isconstant:
                            MyReturn = b2*grad(a3)
                        else:
                            MyReturn = b2*grad(a3) + outer(a3, grad(b2))
                        #print "GRADIENT OF SCALAR*VECTOR"
                        #print MyReturn
                        return MyReturn
        return Grad(SimplifyForm(a))

    #Product rule for divergence
    if MyTest == "SAD_DIVERGENCE":
        #print "Divergence: Processing", o
        a = o.ufl_operands[0]
        InnerType = OperatorTest(a)
        #print "DIV_PROCESS:", InnerType, a
        if InnerType == "SAD_COMPONENT_TENSOR":
            a1, IndexA1 = a.ufl_operands
            #print a1
            #print IndexA1
            A1Type = OperatorTest(a1)
            #print "A1Type", A1Type
            if A1Type == "SAD_PRODUCT":
                a2, b2 = a1.ufl_operands
                A2Type = OperatorTest(a2)
                B2Type = OperatorTest(b2)
                #print "a2:", A2Type, a2
                #print "b2:", B2Type, b2
                if A2Type == "SAD_INDEXED":
                    a3, IndexA3 = a2.ufl_operands
                    if IndexA1 == IndexA3:
                        if checks.is_globally_constant(b2):
                            return SimplifyForm(b2*div(a3))
                        return SimplifyForm(b2*div(a3)) + SimplifyForm(inner(grad(b2), a3))
                if B2Type == "SAD_INDEXED":
                    b3, IndexB3 = b2.ufl_operands
                    #print "o =", o
                    #print "a =", a
                    #print "a1 = ", a1
                    #print "a2 = ", a2
                    #print "b2 = ", b2
                    if IndexA1 == IndexB3:
                        if checks.is_globally_constant(a2):
                            return SimplifyForm(a2*div(b3))
                        return SimplifyForm(a2*div(b3)) + SimplifyForm(inner(grad(a2), b3))
        #print "Divergence: Return Standard", o
        return o

    if MyTest == "SAD_TRACE":
        #print "Simplify Trace!"
        a = o.ufl_operands[0]
        #print o
        #print a
        InnerType = OperatorTest(a)
        #print "A-InnerType:", InnerType
        a_symp = SimplifyForm(a)
        #print "a_symp", a_symp
        SympInnerType = OperatorTest(a_symp)
        #print "A-Symp Inner:", SympInnerType
        if SympInnerType == "SAD_SUM":
            #print "SUM IN TRACE"
            a1, a2 = a_symp.ufl_operands
            return SimplifyForm(tr(a1)) + SimplifyForm(tr(a2))
        if SympInnerType == "SAD_COMPONENT_TENSOR":
            #print "COMPONENT_TENSOR IN TRACE"
            [MyS, MyV] = TensorPullOutScalars(a)
            #print MyS
            #print MyV
            if MyS != as_ufl(1.0):
                MyReturn = MyS*SimplifyForm(tr(MyV))
                return MyReturn
        #print ""
        #print ""
        return o

    """
    if MyTest == "SAD_VARIABLE_DERIVATIVE":
        a, da = o.ufl_operands
        AType = OperatorTest(a)
        #print "DERIV"
        #print "Type:", AType, a
        #print "da", da
        if AType == "SAD_PRODUCT":
            a0, a1 = a.ufl_operands
            #print "A0", a0
            #print "A1", a1
            #print "RETURN:", differentiation.VariableDerivative(a0, da)*a1 + a0*differentiation.VariableDerivative(a1, da)
            return SimplifyForm(differentiation.VariableDerivative(a0, da)*a1) + SimplifyForm(a0*differentiation.VariableDerivative(a1, da))
    """
            
    #turn index sums back into inner products
    if MyTest == "SAD_INDEX_SUM":
        #print ""
        #print "INDEX INDEX INDEX!! SUM!"
        #print o
        #print "Shape 1:", len(o.ufl_shape)
        a, IndexA = o.ufl_operands
        #print "A:", a
        #print "IndexA:", IndexA
        AType = OperatorTest(a)
        #print "AType:", AType
        #print "Test for linearity"
        #IndexSum(a_i + b_i) = IndexSum(a_i) + IndexSum(b_i)
        if AType == "SAD_COMPONENT_TENSOR":
            a0, IndexA0 = a.ufl_operands
            #print "CT A0:", a0
            #print "CT IndexA0:", IndexA0
            A0Type = OperatorTest(a0)
            #print "Component Tensor: A0Type:", A0Type
            if A0Type == "SAD_SUM":
                #print "IndexSum -> Component Tensor -> Sum"
                a1, b1 = a0.ufl_operands
                #print "a1:", a1
                #print "b1:", b1
                A1Type = OperatorTest(a1)
                B1Type = OperatorTest(b1)
                #print "A1Type:", A1Type
                #print "B1Type:", B1Type
                CT1 = ComponentTensor(a1, IndexA0)
                CT2 = ComponentTensor(b1, IndexA0)
                #print "INDEX_SUM: RETURN:", IndexSum(CT1, IndexA) + IndexSum(CT2, IndexA)
                return SimplifyForm(IndexSum(CT1, IndexA)) + SimplifyForm(IndexSum(CT2, IndexA))
        #print "Test for inner product"
        #len == 0: Answer is a scalar => test for inner product
        if AType == "SAD_PRODUCT" and len(o.ufl_shape) == 0:
            a0, b0 = a.ufl_operands
            A0Type = OperatorTest(a0)
            B0Type = OperatorTest(b0)
            #print "A0P:", a0, A0Type
            #print "B0P:", b0, B0Type
            if A0Type == "SAD_INDEXED" and B0Type == "SAD_INDEXED":
                A0Op, A0Index = a0.ufl_operands
                B0Op, B0Index = b0.ufl_operands
                #print A0Op, A0Index
                #print B0Op, B0Index
                if A0Index == B0Index:
                    MyReturn = inner(A0Op, B0Op)
                    #print "Index Sum/Product: Return:", MyReturn
                    return SimplifyForm(MyReturn)
        #print "Test for dot product"
        #len == 1: Answer is a vector => test for dot product
        if AType == "SAD_COMPONENT_TENSOR" and len(o.ufl_shape) > 0:
            if A0Type == "SAD_PRODUCT":
                a00, a01 = a0.ufl_operands
                A00Type = OperatorTest(a00)
                A01Type = OperatorTest(a01)
                #print "A00:", A00Type, a00
                #print "A01:", A01Type, a01
                if A00Type == "SAD_INDEXED" and A01Type == "SAD_INDEXED":
                    #print "2x Indexed!"
                    a1, IndexA1 = a00.ufl_operands
                    b1, IndexB1 = a01.ufl_operands
                    #print "a1:", a1
                    #print "IndexA1:", IndexA1
                    #print "b1:", b1
                    #print "IndexB1:", IndexB1
                    #print len(IndexA1), len(IndexB1)
                    A1Type = OperatorTest(a1)
                    B1Type = OperatorTest(b1)
                    #print "A1Type", A1Type
                    #print "B1Type", B1Type
                    #print "len(a1)", len(a1)
                    #print "len(b1)", len(b1)
                    #print "shape(b1)", b1.ufl_shape
                    if A1Type == "SAD_COMPONENT_TENSOR":
                        [a1,b1] = MySwap(a1,b1)
                        [IndexA1, IndexB1] = MySwap(IndexA1, IndexB1)
                        [A1Type, B1Type] = MySwap(A1Type, B1Type)
                    if B1Type == "SAD_COMPONENT_TENSOR":
                        a2, A2Index = b1.ufl_operands
                        #print "a2:", a2
                        #print "A2Index:", A2Index
                        A2Type = OperatorTest(a2)
                        #print "A2Type:", A2Type
                        if A2Type == "SAD_INDEXED":
                            a3, IndexA3 = a2.ufl_operands
                            #print "a3:", a3
                            #print "IndexA3[0]", IndexA3[0]
                            #print "IndexA3[1]", IndexA3[1]
                            #print "IndexA", IndexA
                            #print len(a3)
                            if len(IndexA3) == 2:
                                #need str comparison here?!
                                if str(IndexA3[0]) == str(IndexA):
                                    #print "Test 1"
                                    #MyReturn = dot(SimplifyForm(a1),SimplifyForm(a3))
                                    #print "RETURN:", MyReturn
                                    return dot(a1,a3)
                                if str(IndexA3[1]) == str(IndexA):
                                    #print "Test 2"
                                    return dot(a3, a1)
        return o
    #turn component tensors into dot products if possible
    if MyTest == "SAD_COMPONENT_TENSOR":
        a, ResultIndex = o.ufl_operands
        #check for transposed:
        MyReturn = TensorHiddenTransposedDot(o)
        if MyReturn != None:
            #print "C_TENSOR SWAP TRANSPOSE"
            #print MyReturn
            return SimplifyForm(MyReturn)
        
        AType = OperatorTest(a)
        #print ""
        #print "C_TENSOR:", o
        #print "C_TENSOR: Tensor Index", ResultIndex
        #print "C_TENSOR: AType:", AType
        #print "C_TENSOR:"
        #print a
        if AType == "SAD_SUM":
            #print "COMPONENT TENSOR OF SUM"
            a1, b1 = a.ufl_operands
            #print "a1:", a1
            #print "b1:", b1
            return SimplifyForm(ComponentTensor(a1, ResultIndex)) + SimplifyForm(ComponentTensor(b1, ResultIndex))
        if AType == "SAD_PRODUCT":
            #print "C_Tensor Product"
            a1, b1 = a.ufl_operands
            A1Type = OperatorTest(a1)
            B1Type = OperatorTest(b1)
            #test for possibility of outer product
            if len(ResultIndex) == 2:
                if A1Type == "SAD_INDEXED" and B1Type == "SAD_INDEXED":
                    a2, A2Index = a1.ufl_operands
                    b2, B2Index = b1.ufl_operands
                    if len(A2Index) == 1 and len(B2Index) == 1:
                        #print "POSSIBILITY FOR OUTER"
                        #print ResultIndex[0], str(ResultIndex[0]) == str(A2Index)
                        #print ResultIndex[1], str(ResultIndex[1]) == str(B2Index)
                        #print A2Index
                        #print B2Index
                        #print "a2", a2
                        #print "b2", b2
                        if str(ResultIndex[0]) == str(A2Index) and str(ResultIndex[1]) == str(B2Index):
                            MyReturn = outer(a2, b2)
                        if str(ResultIndex[0]) == str(B2Index) and str(ResultIndex[1]) == str(A2Index):
                            MyReturn = outer(b2, a2)
                        #print "Created outer:", MyReturn
                        return SimplifyForm(MyReturn)
        if AType == "SAD_INDEX_SUM":
            a1, SumIndex = a.ufl_operands
            A1Type = OperatorTest(a1)
            #print "C_TENSOR: INDEX_SUM: a1", A1Type, a1
            #print "SumIndex", SumIndex
            if A1Type == "SAD_PRODUCT":
                a2, b2 = a1.ufl_operands
                A2Type = OperatorTest(a2)
                B2Type = OperatorTest(b2)
                #print "Product:"
                #print "a2", A2Type, a2
                #print "b2", B2Type, b2
                if A2Type == "SAD_INDEXED" and B2Type == "SAD_INDEXED":
                    #print "Both are indexed!"
                    a3, IndexA3 = a2.ufl_operands
                    b3, IndexB3 = b2.ufl_operands
                    #print "a3", a3
                    #print "IndexA3", IndexA3
                    #print "b3", b3
                    #print "IndexB3", IndexB3
                    #print len(IndexA3)
                    #print len(IndexB3)
                    if len(IndexA3) == 1 and len(IndexB3) == 2:
                        MyMatrix = b3
                        MyMatrixIndex = IndexB3
                        MyVector = a3
                        MyVectorIndex = IndexA3
                    elif len(IndexB3) == 1 and len(IndexA3) == 2:
                        MyMatrix = a3
                        MyMatrixIndex = IndexA3
                        MyVector = b3
                        MyVectorIndex = IndexB3
                    else:
                        print "Shape Derivative: Non-Matrix-Vector Tensor detected"
                        print "Shape Derivative: Some features might not work correctly, double check"
                        return o
                    #print "Matrix:", MyMatrix
                    #print "Vector:", MyVector
                    #need to do string comparisons here for some reason...
                    if str(ResultIndex) == str(MyMatrixIndex[0]) and str(SumIndex) == str(MyMatrixIndex[1]) and str(SumIndex) == str(MyVectorIndex):
                        MyResult = dot(MyMatrix, MyVector)
                        #print "MATRIX VECTOR PRODUCT:", MyResult
                        return SimplifyForm(MyResult)
                    if str(ResultIndex) == str(MyMatrixIndex[1]) and str(SumIndex) == str(MyMatrixIndex[0]) and str(SumIndex) == str(MyVectorIndex):
                        MyResult = dot(MyVector, MyMatrix)
                        #print "VECTOR Matrix PRODUCT:", MyResult
                        return SimplifyForm(MyResult)
                    return o
            #pull-out IndexSums of Sums...
            if A1Type == "SAD_SUM":
                a2, b2 = a1.ufl_operands
                A2Type = OperatorTest(a2)
                B2Type = OperatorTest(b2)
                #print "C_Tensor -> Index Sum -> Sum"
                #print "a2", A2Type, a2
                #print "b2", B2Type, b2
                ISum1 = IndexSum(a2, SumIndex)
                ISum2 = IndexSum(b2, SumIndex)
                MyReturn =  SimplifyForm(ComponentTensor(ISum1, ResultIndex)) + SimplifyForm(ComponentTensor(ISum2, ResultIndex))
                return MyReturn
        #Process general tensors
        if AType == "SAD_INDEXED":
            a1, IndexA1 = a.ufl_operands
            A1Type = OperatorTest(a1)
            #print "Tensor Interior:", a1
            #print "Tensor InINdex:", IndexA1
            #turn row of gradient to gradient of component
            if A1Type == "SAD_GRADIENT":
                #print "C_TENSOR: Test: turn row of gradient to gradient of component"
                a2 = a1.ufl_operands[0]
                if len(IndexA1) == 2:
                    if str(IndexA1[1]) == str(ResultIndex):
                        MyReturn = grad(a2[IndexA1[0]])
                        #print "MyReturn:", MyReturn
                        return SimplifyForm(MyReturn)
            #check for linearity in tensor
            if A1Type == "SAD_SUM":
                #print "Sum in Indexed over", IndexA1
                a2, b2 = a1.ufl_operands
                A2Type = OperatorTest(a2)
                B2Type = OperatorTest(b2)
                #print "a2", A2Type, a2
                #print "b2", B2Type, b2
                sum1 = Indexed(a2, IndexA1)
                sum2 = Indexed(b2, IndexA1)
                return SimplifyForm(ComponentTensor(sum1, ResultIndex)) + SimplifyForm(ComponentTensor(sum2, ResultIndex))
            """
            #pull scalars out from double nested tensors
            if A1Type == "SAD_COMPONENT_TENSOR":
                print "Tensor of Tensor!"
                a2, IndexA2 = a1.ufl_operands
                A2Type = OperatorTest(a2)
                print "a2:", A2Type, a2
                print "IndexA2:", IndexA2
                if A2Type == "SAD_PRODUCT":
                    print "pull out scalar"
                    #exit()
            """
        #print "C_TENSOR: DO NOTHING: RETURN:", o
        return o#ComponentTensor(SimplifyForm(a), ResultIndex)

    if MyTest == "SAD_LIST_TENSOR":
        #print "LIST TENSOR:", o, o.ufl_shape
        MyReturn = RecombineListTensor(o)
        #print "LIST TENSOR: RETURN:", MyReturn
        return MyReturn
    """
    #remove index from scalar values...
    if MyTest == "SAD_INDEXED":
        a, IndexA = o.ufl_operands
        print "SIMPLIFY: INDEXED:"
        print a, len(a.ufl_shape)
        print IndexA
        if len(a.ufl_shape) == 0:
            return SimplifyForm(a)
    """

    if MyTest == "SAD_INNER":
        a,b = o.ufl_operands
        TypeA = OperatorTest(a)
        TypeB = OperatorTest(b)
        #print "SAD_INNER", a
        #print "SAD_INNER: TYPE_A:", TypeA
        #print "SAD_INNER", b
        #print "SAD_INNER: TYPE_B:", TypeB
        #print ""
        if TypeB == "SAD_DOT":
            [a, b] = MySwap(a, b)
            [TypeA, TypeB] = MySwap(TypeA, TypeB)
        if TypeA == "SAD_DOT":
            a0,b0 = a.ufl_operands
            TypeA0 = OperatorTest(a0)
            TypeB0 = OperatorTest(b0)
            #remove nested outer products
            #inner(dot(outer(a1,b1),b0),b) = inner(a1,b)*inner(b1,b0)
            if TypeA0 == "SAD_OUTER":
                a1, b1 = a0.ufl_operands
                MyReturn = inner(a1,b)*inner(b1,b0)
                #print "SAD_INNER 1: Return:", MyReturn
                return SimplifyForm(inner(a1,b))*SimplifyForm(inner(b1,b0))
            #remove transposed!
            #inner(dot(v,A),w) = inner(v, dot(A,w)) ( = inner(dot(A,w),v) )
            #v = a0, A = b0, w = b
            if len(b0.ufl_shape) == 2 and len(a0.ufl_shape) < len(b0.ufl_shape):
                #print "TRANSPOSED DOT!"#, o
                MyReturn = inner(dot(b0,b), a0)
                #print "SAD_INNER 2: Return:", MyReturn
                return SimplifyForm(inner(dot(b0,b), a0))
    """
    if MyTest == "SAD_TRANSPOSED":
        a = o.ufl_operands
        print "TRANSPOSED:"
        print str(a)
        AType = OperatorTest(a)
        if AType == "SAD_SUM":
            a1, b1 = a.ufl_operands
            return SimplifyForm(transposed(a1)) + SimplifyForm(transposed(b1))
    """
    #remove transposed from dot products by flipping the order
    if MyTest == "SAD_DOT":
        a1, a2 = o.ufl_operands
        #print "SAD_DOT:", o
        #print "a1:", a1
        #print "a2:", a2
        if len(a1.ufl_shape) == 2 and len(a2.ufl_shape) == 2:
            #print "SAD_DOT: dot(MAT, MAT)"
            A1Type = OperatorTest(a1)
            A2Type = OperatorTest(a2)
            if A1Type == "SAD_TRANSPOSED":
                return SimplifyForm(dot(a2,a1))
            if A2Type == "SAD_TRANSPOSED":
                return SimplifyForm(outer(a1,a2))
        #dot(a, outer(b,c)) = inner(a,b)*c
        if len(a1.ufl_shape) == 1 and len(a2.ufl_shape) == 2:
            #print "SAD_DOT: dot(VEC, MAT)"
            A2Type = OperatorTest(a2)
            if A2Type == "SAD_OUTER":
                #print "MATCH!"
                #print "o", o
                #print "a1", a1
                #print "a2", a2
                b, c = a2.ufl_operands
                MyReturn = inner(a1,b)*c
                #print MyReturn
                return SimplifyForm(MyReturn)
        if len(a1.ufl_shape) == 2 and len(a2.ufl_shape) == 1:
            #print "SAD_DOT: dot(MAT, VEC)"
            A1Type = OperatorTest(a1)
            if A1Type == "SAD_OUTER":
                b, c = a1.ufl_operands
                MyReturn = inner(a2,c)*b
                return SimplifyForm(MyReturn)
        #turn dots into inners
        if len(a1.ufl_shape) == 1 and len(a2.ufl_shape) == 1:
            MyReturn = inner(a1, a2)
            return SimplifyForm(MyReturn)
        #print "SAT_DOT: PASS!"

    if MyTest != "SAD_PRODUCT" and MyTest != "SAD_INNER" and MyTest != "SAD_DOT":
        #print "NOTHING TO DO! Returning:", o
        return o
    MyOp = GetMyOp(MyTest)
    a, b = o.ufl_operands
    """
    a = SimplifyForm(a)
    b = SimplifyForm(b)
    """
    dta = OperatorTest(a)
    dtb = OperatorTest(b)
    #print "General Product: Operand Type A:", dta, "Expr:", a
    #print "General Product: Operand Type B:", dtb, "Expr:", b
    # indexed is always scalar...
    if dtb == "SAD_INDEXED":
        [dta, dtb] = MySwap(dta, dtb)
        [a, b] = MySwap(a, b)
    if dta == "SAD_INDEXED":
        a1, IndexA1 = a.ufl_operands
        A1Type = OperatorTest(a1)
        if A1Type == "SAD_SUM":
            a2, b2 = a1.ufl_operands
            #print "Test Ind", a2
            #print "Test Ind", b2
            #print "MyReturn"
            #print b*Indexed(a2, IndexA1) + b*Indexed(b2, IndexA1)
            MyReturn = SimplifyForm((b*Indexed(a2, IndexA1))) + SimplifyForm((b*Indexed(b2, IndexA1)))
            return MyReturn
    
    if dta == "SAD_COMPONENT_TENSOR":
        #print "SIMPLIFY: Pull out Scalars of:", a
        [MyS, MyV] = TensorPullOutScalars(a)
        #print "SIMPLIFY: SCALARS:", MyS
        #print "SIMPLIFY: VECTORS:", MyV
        MyS = SimplifyForm(MyS)
        MyV = SimplifyForm(MyV)
        if MyS != as_ufl(1.0):
            #print "Pulling out Scalars:"
            #print "MyS:", MyS
            #print "MyV:", MyV
            #print "b:", b
            #print "MyOp:", MyOp
            MyReturn = SimplifyForm(MyS*MyOp(MyV, b))
            return MyReturn
        MyA = TensorDistributiveLaw(a)
        MyA = SimplifyForm(MyA)
        return MyOp(MyA, b)
    if dtb == "SAD_COMPONENT_TENSOR":
        [MyS, MyV] = TensorPullOutScalars(b)
        MyS = SimplifyForm(MyS)
        MyV = SimplifyForm(MyV)
        if MyS != as_ufl(1.0):
            MyReturn = SimplifyForm(MyS*MyOp(a, MyV))
            return MyReturn
        MyB = TensorDistributiveLaw(b)
        MyB = SimplifyForm(MyB)
        return MyOp(a, MyB)
    
    #Don't do both at once...
    #if dta == "SAD_SUM" and dtb == "SAD_SUM":
    #    dta0 = a.ufl_operands[0]
    #    dta1 = a.ufl_operands[1]
    #    dtb0 = b.ufl_operands[0]
    #    dtb1 = b.ufl_operands[1]
    #    MyReturn = SimplifyForm(MyOp(dta0,dtb0), MyOp) + SimplifyForm(MyOp(dta0,dtb1), MyOp) + SimplifyForm(MyOp(dta1,dtb0), MyOp) + SimplifyForm(MyOp(dta1,dtb1), MyOp)
    #    return MyReturn
    
    #if dta == "SAD_INDEXED" and dtb == "SAD_INDEXED":
    #    a0 = a.ufl_operands[0]
    #    IndexA = a.ufl_operands[1].indices()#[0]
    #    b0 = b.ufl_operands[0]
    #    IndexB = b.ufl_operands[1].indices()#[0]
    #    print "INDEXED: AOp1:", a0, ", AOp2:", IndexA
    #    print "INDEXED: BOp1:", b0, ", BOp2:", IndexB
    #    if IndexA != IndexB:
    #        print "Shape Derivative: Unequal Indices in Distributive Rule"
    #        exit(1)
    #    else:
    #        MyReturn = a0[IndexA]#*b0[IndexB]
    #        print "EQUAL INDEX RETURN VALUE:", MyReturn
    #        return MyReturn
    
    if dta == "SAD_SUM":
        #print "REPLACE RIGHT:", dtb
        #print "ADLaw_S Op:", MyOp
        dta0 = a.ufl_operands[0]
        dta1 = a.ufl_operands[1]
        return SimplifyForm(MyOp(dta0,b)) + SimplifyForm(MyOp(dta1,b))
    if dtb == "SAD_SUM":
        #print "REPLACE RIGHT:", dtb
        #print "ADLaw_S Op:"#, MyOp
        b0 = b.ufl_operands[0]
        b1 = b.ufl_operands[1]
        #print b0
        #print b1
        return SimplifyForm(MyOp(a,b0)) + SimplifyForm(MyOp(a,b1))
    #print "REACHED END:"
    a_new = SimplifyForm(a)
    b_new = SimplifyForm(b)
    #print "a_old:", a, a.ufl_shape
    #print "b_old:", b, b.ufl_shape
    #print "a_new:", a_new, a_new.ufl_shape
    #print "b_new:", b_new, b_new.ufl_shape
    #print "RETURNING:", MyOp(a_new,b_new)
    return MyOp(a_new,b_new)

def CreateStandardExpression(o):
    #print "SIMPLIFICATION: CREATE STANDARD: INPUT:"
    #print o
    #print ""
    #print "SIMPLIFICATION: PRE_SIMPLIFICATION:"
    o_new = SimplifyForm(o)
    #print "DONE"
    #print o_new
    i = 0
    while o_new != o and i < 50:
        #print "SIMPLIFICATION: CREATE STANDARD: A-LOOP!", i
        o = o_new
        o_new = SimplifyForm(o)
        #print o_new
        i = i+1
    #print ""
    #print "SIMPLIFICATION: CREATE STANDARD: STEP 1:"
    #print o_new
    #print ""
    #print "SIMPLIFICATION: APPLY DERIVATIVES on", o_new
    o_new = SAD_derivatives.SAD_apply_derivatives(o_new)
    #print ""
    #print "SIMPLIFICATION: AFTER APPLY DERIVATIVES:"
    #print o_new
    #print ""
    i = 0
    #print "SIMPLIFICATION: POST_SIMPLIFICATION"
    while o_new != o and i < 50:
        #print ""
        #print "SIMPLIFICATION: CREATE STANDARD: B-LOOP!", i
        #print ""
        o = o_new
        o_new = SimplifyForm(o)
        #print o_new
        i = i + 1
    return o_new

def TransposeInner(SADForm):
    OperatorTest = TestOperator()
    MyType = OperatorTest(SADForm)
    if MyType == "SAD_INNER":# or MyType == "SAD_DOT":
        a, b = SADForm.ufl_operands
        AType = OperatorTest(a)
        BType = OperatorTest(b)
        if AType == "SAD_DOT":
            a1, a2 = a.ufl_operands
            return inner(a2, dot(b, a1))
        if BType == "SAD_DOT":
            b1, b2 = b.ufl_operands
            return inner(dot(a, b1), b2)
    return SADForm

#traverse UFL tree and remove all sum-branches involving grad(n)
#used to get the outer spatial derivative of <grad_x(h(x,N), n> in surface shape derivative
def RemoveFromSForm(o, Expr2Rmv1, Expr2Rmv2):
    #print "RemoveFromSForm:", Expr2Rmv1, Expr2Rmv2
    #print o
    OperatorTest = TestOperator()
    MyType = OperatorTest(o)
    Products = ["SAD_PRODUCT", "SAD_INNER", "SAD_DOT"]
    #if MyType == "SAD_INNER":
    #    MyOp = inner
    #if MyType == "SAD_DOT":
    #    MyOp = dot
    #if MyType == "SAD_PRODUCT":
    #    MyOp = Product
    if MyType in Products:
        #print "REMOVER IN PRODUCT-EQUIV:", o
        MyOp = GetMyOp(MyType)
        a, b = o.ufl_operands
        def MyTestForRemoval(a, b, Expr2Rmv1, Expr2Rmv2):
            #print "TEST!", a, b
            #a, b = o.ufl_operands
            #print a, b, Expr2Rmv1, Expr2Rmv2, MyType
            Logic1 = (a == Expr2Rmv1 and b == Expr2Rmv2)
            #Logic2 = (a == Expr2Rmv2 and b == Expr2Rmv1)
            if MyType == "SAD_PRODUCT" or MyType == "SAD_INNER":
                Logic2 = (a == Expr2Rmv2 and b == Expr2Rmv1)
            else:
                Logic2 = False
            #print "Logic", Logic1, Logic2
            if Logic1 or Logic2:
                #print "REMOVE FROM", o, MyType
                #print "OpA:", a
                #print "OpB:", b
                #print "Logic", Logic1, Logic2
                return True#as_ufl(0.0)
        if MyTestForRemoval(a, b, Expr2Rmv1, Expr2Rmv2):
            return as_ufl(0.0)
        #Transpose check potentially slow...
        ot = TransposeInner(o)
        if ot != o:
            at, bt = ot.ufl_operands
            at1 = RemoveFromSForm(at, Expr2Rmv1, Expr2Rmv2)
            bt1 = RemoveFromSForm(bt, Expr2Rmv1, Expr2Rmv2)
            if at1 == as_ufl(0.0) or bt1 == as_ufl(0.0):
                return as_ufl(0.0)
        a1 = RemoveFromSForm(a, Expr2Rmv1, Expr2Rmv2)
        b1 = RemoveFromSForm(b, Expr2Rmv1, Expr2Rmv2)
        if a1 == as_ufl(0.0) or b1 == as_ufl(0.0):
            return as_ufl(0.0)
        return MyOp(a1,b1)
    if MyType == "SAD_SUM":
        #print "REMOVER IN SUM:"
        a, b = o.ufl_operands
        #print a
        #print b
        a1 = RemoveFromSForm(a, Expr2Rmv1, Expr2Rmv2)
        b1 = RemoveFromSForm(b, Expr2Rmv1, Expr2Rmv2)
        if a1 == as_ufl(0.0) and b1 == as_ufl(0.0):
            return as_ufl(0.0)
        if a1 == as_ufl(0.0):
            return b1
        if b1 == as_ufl(0.0):
            return a1
        return a1 + b1
    return o

#Fills CList with all product inputs of o
#Usefull for operations utilizing the permutation rule:
#all products could be on the same UFL-Tree level
def CollectProduct(o, CList):
    OperatorTest = TestOperator()
    Type = OperatorTest(o)
    #print "Collecting:", Type, o
    if Type == "SAD_PRODUCT":
        a, b = o.ufl_operands
        CollectProduct(a, CList)
        CollectProduct(b, CList)
    else:
        CList.append(o)
    NumM1 = 0
    #remove -1*-1*-1*-1
    while as_ufl(-1) in CList:
        CList.remove(as_ufl(-1))
        NumM1 += 1
    if NumM1%2 != 0:
        CList.append(as_ufl(-1))
    #return CList

def _ApplyTangentStokesGetType(Expr, MyMarker, MyScalars, MyVectors):
    #print "TANGENT STOKES: Expr", Expr
    OperatorTest = TestOperator()
    Type = OperatorTest(Expr)
    if Type == "SAD_INNER" or Type == "SAD_DOT":
        a, b = Expr.ufl_operands
        if a == MyMarker:
            #print "TANGENT STOKES: FOUND MARKER IN a"
            MyVectors.append(b)
        elif b == MyMarker:
            #print "TANGENT STOKES: FOUND MARKER IN b"
            MyVectors.append(a)
        else:
            #check if marker can be identified by transposition
            AType = OperatorTest(a)
            BType = OperatorTest(b)
            FoundA = False
            FoundB = False
            if AType == "SAD_DOT":
                a1, a2 = a.ufl_operands
                #inner(dot(a1,a2), b) = inner(a2, dot(b, a1)) = inner(a1, dot(a2, b))
                if a2 == MyMarker:
                    MyVectors.append(dot(b, a1))
                    FoundA = True
                if a1 == MyMarker:
                    MyVectors.append(dot(a2, b))
                    FoundA = True
            if BType == "SAD_DOT":
                b1, b2 = b.ufl_operands
                if b2 == MyMarker:
                    MyVectors.append(dot(a, b1))
                    FoundB = True
                if b1 == MyMarker:
                    MyVectors.append(dot(b2, a))
                    FoundB = True
            if FoundA == False and FoundB == False:
                MyScalars.append(Expr)
    else:
        MyScalars.append(Expr)

#Automatically descends into sums...
def ApplyTangentStokes(o, MyMarker, ReplaceWith, n, kappa, Is_Normal=[]):
    OperatorTest = TestOperator()
    Type = OperatorTest(o)
    #print "TANGENT STOKES: PROCESSING", Type, o
    #print "MARKER:", MyMarker
    if Type == "SAD_SUM":
        a,b = o.ufl_operands
        #print "TANGENT STOKES: FORM IS A SUM:"
        #print "TANGENT STOKES: RE-START on a:"
        #print a
        #print "TANGENT STOKES: RE-START on b:"
        #print b
        s1 = ApplyTangentStokes(a, MyMarker, ReplaceWith, n, kappa, Is_Normal)
        s2 = ApplyTangentStokes(b, MyMarker, ReplaceWith, n, kappa, Is_Normal)
        return s1 + s2
    if Type == "SAD_PRODUCT" or Type == "SAD_DOT" or Type == "SAD_INNER":
        CList = []
        CollectProduct(o, CList)
        if len(CList) == 0:
            print "TANGENT STOKES: ERROR: Expecting a list"
            exit(1)
        Expr = CList[0]
        MyScalars = []
        MyVectors = []
        _ApplyTangentStokesGetType(Expr, MyMarker, MyScalars, MyVectors)
        for i in range(1,len(CList)):
            Expr = CList[i]
            _ApplyTangentStokesGetType(Expr, MyMarker, MyScalars, MyVectors)
        #print ""
        #print ""
        #print "TANGENT STOKES: VECTORS"
        #for Vec in MyVectors:
        #    print Vec
        #print "TANGENT STOKES: SCALARS"
        #for Scar in MyScalars:
        #    print "Scar:", Scar
        #print ""
        #print ""
        if len(MyVectors) != 1:
            print "TANGENT STOKES: EXPECTING EXACTLY ONE RELEVANT VECTOR, BUT GOT", len(MyVectors)
            exit(1)
        if MyVectors[0] in Is_Normal:
            #print "TANGENT STOKES: REMOVING NORMAL PART"#, MyVectors[0], Is_Normal
            #print "Old Form:", o
            #print "Removing:", MyMarker, MyVectors[0]
            MyReturn = RemoveFromSForm(o, MyMarker, MyVectors[0])
            #print "New Form:", MyReturn
            return MyReturn
        else:
            #print "TANGENT STOKES: NOT REMOVING / NOT Is_Normal!"
            #ScalarIsNormal = False
            #for V in Is_Normal:
            #    if inner(V,n) in MyScalars or inner(n,V) in MyScalars:
            #        ScalarIsNormal = True
            #print "TAN_STOKES:, ScalarIsNormal:", ScalarIsNormal
            if len(MyScalars) == 0:
                Expr2 =  inner(ReplaceWith,n)*div_tan(MyVectors[0], n)
                Expr3 = inner(ReplaceWith,n)*kappa*inner(MyVectors[0], n)
                NewExpr = Expr2 - Expr3
            else:
                Scar = MyScalars[0]
                for k in range(1, len(MyScalars)):
                    Scar = Scar*MyScalars[k]
                NewExpr = -inner(ReplaceWith,n)*kappa*Scar*inner(MyVectors[0], n)
                #if ScalarIsNormal == False:
                NewExpr = NewExpr + inner(ReplaceWith, n)*div_tan(MyVectors[0]*Scar,n)
        #print "TANGENT STOKES: NEW EXPRESSION", NewExpr
        return NewExpr
    else:
        print "APPLY TANGENT STOKES: EXPECTING SUM OR PRODUCT"
        print o
        return o

#remove inner(n,n)
def RemoveNN(o, n):
    OperatorTest = TestOperator()
    MyType = OperatorTest(o)
    if MyType == "SAD_SUM":
        a, b = o.ufl_operands
        Expr1 = RemoveNN(a, n)
        Expr2 = RemoveNN(b, n)
        return Expr1 + Expr2
    else:
        CList = []
        CollectProduct(o, CList)
        while inner(n,n) in CList:
            CList.remove(inner(n,n))
        o_new = CList[0]
        for i in range(1, len(CList)):
            o_new = o_new*CList[i]
        return o_new

#apply simplifications to o, assumes o is in SAD standard form
def ApplySimplifications(o, n, kappa, Is_Normal = [], Constant_In_Normal = []):
    #remove inner(n,n)
    o_new = RemoveNN(o, n)
    o_new = RemoveFromSForm(o_new, n, grad(n))
    for Vec in Is_Normal:
        o_new = RemoveFromSForm(o_new, Vec, grad(n))
        """
        print ""
        print "1) Expression after using", Vec, "is multiple of normal"
        print "1) Remove:", grad(n), Vec
        print o_new
        print ""
        """
    #always assume curvature and normal(?) is extended constantly in normal direction:
    for Vec in Constant_In_Normal + [n, kappa]:
        o_new = RemoveFromSForm(o_new, grad(Vec), n)
        """
        print ""
        print "2) Expression after using", Vec, "is constant in normal direction"
        print "2) Remove:", grad(Vec), n
        print o_new
        print ""
        """
    return o_new

#add assumption (DV)^T W = 0
def IncorporateSymmetry(o, V, W, N):
    OperatorTest = TestOperator()
    #print "processing:", o
    MyTest = OperatorTest(o)
    if MyTest == "SAD_SUM":
        a, b = o.ufl_operands
        NewFormA = IncorporateSymmetry(a, V, W, N)
        NewFormB = IncorporateSymmetry(b, V, W, N)
        if NewFormA == as_ufl(0.0) and NewFormB == as_ufl(0.0):
            return as_ufl(0.0)
        if NewFormA != as_ufl(0.0):
            if NewFormB == as_ufl(0.0):
                #print "ReturningA", NewFormA
                return NewFormA
        if NewFormB != as_ufl(0.0):
            if NewFormA == as_ufl(0.0):
                #print "ReturningB", NewFormB
                return NewFormB
        #print "Returning", NewFormA + NewFormB
        return NewFormA + NewFormB
    elif MyTest == "SAD_PRODUCT":
        CList = []
        CollectProduct(o, CList)
        k = 0
        DVIn = False
        for k in range(len(CList)):
            InnerForm = CList[k]
            #print k, InnerForm
            InnerType = OperatorTest(InnerForm)
            if InnerType == "SAD_INNER":
                c, d = InnerForm.ufl_operands
                CType = OperatorTest(c)
                DType = OperatorTest(d)
                #print "CType", CType, "DType", DType
                if CType == "SAD_DOT":
                    c1, c2 = c.ufl_operands
                    if c2 == grad(V) and c1 == N:
                        DVIn = True
                    if c1 == grad(V) and d == N:
                        DVIn = True
                if DType == "SAD_DOT":
                    d1, d2 = d.ufl_operands
                    if d2 == grad(V) and d1 == N:
                        DVIn = True
                    if d1 == Grad(V) and c == N:
                        DVIn = True
        #print "W", W
        #print "V", V
        #print "N", N
        if W != None and V != None:
            WIn = inner(W, N) in CList or inner(N, W) in CList or dot(W, N) in CList or dot(N, W) in CList
            #print "WIn", WIn
            if not (DVIn and WIn):
                NewForm = CList[0]
                for i in range(1,len(CList)):
                    NewForm = NewForm*CList[i]
                return NewForm
            else:
                return as_ufl(0.0)
        return o
    else:
        return o

#Create a list of all summands of a form and append
def MySumAppend(SADForm, Measure, TermList):
    OperatorTest = TestOperator()
    Type = OperatorTest(SADForm)
    if Type == "SAD_SUM":
        a, b = SADForm.ufl_operands
        MySumAppend(a, Measure, TermList)
        MySumAppend(b, Measure, TermList)
    else:
        if SADForm != as_ufl(0.0):
            TermList.append((SADForm, Measure))

#Build the final form out of the SumAppendList
def MyBuildSADForm(TermList, V, SymmetryDirection=None, N=None):
    #print "BUILDING FINAL EXPRESSION: START"
    #for (i, j) in TermList:
    #    print i
    CancelList = [False]*len(TermList)
    if len(TermList) > 1:
        for i in range(len(TermList)):
            (MyFormA, MyMeasureA) = TermList[i]
            #print "PROCESSING:", i, MyFormA
            for j in range(i+1,len(TermList)):
                if i != j and CancelList[i] == False:
                    AList = []
                    CollectProduct(MyFormA, AList)
                    (MyFormB, MyMeasureB) = TermList[j]
                    #print "Form A:", MyFormA
                    #print "    Test against Form B:", MyFormB
                    BList = []
                    CollectProduct(MyFormB, BList)
                    for a in range(len(AList)):
                        AExpr = AList[a]
                        for b in range(len(BList)):
                            BExpr = BList[b]
                            if AExpr == BExpr:
                                AList[a] = None
                                BList[b] = None
                                break
                    #for a in range(len(AList)):
                    #    print "    A:", AList[a]
                    #for b in range(len(BList)):
                    #    print "    B:", BList[b]
                    AllNoneA = True
                    AllNoneB = True
                    NumM1A = 0
                    NumM1B = 0
                    for a in range(len(AList)):
                        if AList[a] != None and AList[a] != as_ufl(-1):
                            AllNoneA = False
                        if AList[a] == as_ufl(-1):
                            NumM1A = NumM1A + 1
                    for b in range(len(BList)):
                        if BList[b] != None and BList[b] != as_ufl(-1):
                            AllNoneB = False
                        if BList[b] == as_ufl(-1):
                            NumM1B = NumM1B + 1
                    #print AllNoneA, AllNoneB, NumM1A, NumM1B
                    if AllNoneA == True and AllNoneB == True and NumM1A%2 == 0 and NumM1B%2 != 0 and MyMeasureA == MyMeasureB:
                        CancelList[i] = True
                        CancelList[j] = True
                        #print "i:", i, "cancels with j:", j
                        break
    #print "CancelList:", CancelList
    #for k in range(len(CancelList)):
    #    print "k:", k, CancelList[k]
    IndexStart = -1
    for i in range(len(TermList)):
        if CancelList[i] == False:
            MyFormA, MyMeasureA = TermList[i]
            if SymmetryDirection != None and N != None:
                MyFormA = IncorporateSymmetry(MyFormA, SymmetryDirection, V, N)
            SADForm = MyFormA*MyMeasureA
            IndexStart = i
            break
    if IndexStart == -1:
        return as_ufl(0.0)*dx
    for i in range(IndexStart+1, len(TermList)):
        if CancelList[i] == False:
            MyFormA, MyMeasureA = TermList[i]
            if MyFormA != as_ufl(0.0):
                if SymmetryDirection != None and N != None:
                    MyFormA = IncorporateSymmetry(MyFormA, SymmetryDirection, V, N)
                SADForm = SADForm + MyFormA*MyMeasureA
    return SADForm
