Merge "OldMesh.msh";
Coherence;
CreateTopology;

Delete Physicals;

ll[] = Line "*";
For j In {0 : #ll[]-1}
//new id for lines is the old id + 1000
Compound Line(ll[j]+1000) = ll[j];
Physical Line(ll[j]) = ll[j]+1000;
EndFor

ss[] = Surface "*";
For i In {0 : #ss[]-1}
Compound Surface(ss[i]+1000) = ss[i];
Physical Surface(ss[i]) = ss[i]+1000;
//Delete {Surface{ss[i]};}
EndFor

//mesh resolution
Field[7] = MathEval;
Field[7].F = "0.05";
Background Field = 7;