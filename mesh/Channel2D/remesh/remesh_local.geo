Merge "OldMesh.msh";
Coherence;
CreateTopology;

Delete Physicals;

ll[] = Line "*";
For j In {0 : #ll[]-1}
//Printf("LINE: %f %f", j, ss[j]);
//new id for lines is the old id + 1000
Compound Line(ll[j]+1000) = ll[j];
Physical Line(ll[j]) = ll[j]+1000;
EndFor

ss[] = Surface "*";
For i In {0 : #ss[]-1}
//Printf("SURFACE: %f %f", i, ss[i]);
Compound Surface(ss[i]+1000) = ss[i];
Physical Surface(ss[i]) = ss[i]+1000;
//Delete {Surface{ss[i]};}
EndFor

//mesh resolution
Field[1] = Attractor;
Field[1].NNodesByEdge = 100;
Field[1].EdgesList = {1041, 1042};
Field[2] = Threshold;
Field[2].IField = 1;
Field[2].LcMin = 0.025;
Field[2].LcMax = 0.1;
Field[2].DistMin = 0.15;
Field[2].DistMax = 0.5;
Background Field = 2;