from dolfin import *
from ShapeOpt import *
from SAD_IO import *
from subprocess import call

# SAD_Navier_Reduced
# Author: Stephan Schmidt
#
# This demo finds the flow obstacle shape with minimum drag (shear) in an incompressible Navier-Stokes fluid
#
# This demo automatically generates a "nested" optimization loop, where primal, adjoint and optimality
# are solved sugessively. The optimization strategy is approximative Newton on the reduced system
#
# All (shape-) derivatives are generated automatically
#
# If you want to change state PDE or objective function, just change the Lagrangian. Everything else should
# adapt automatically.
#
# The default Reynolds Number is 100, see other material parameters below
#
# If starting from the supplied meshes, some resmeshing is problably necessary during optimization.
# This is done via gmsh.
# Please check if the strings below match your call to gmsh:
import platform
if platform.system() == "Darwin":
    Gmsh_str = "/Applications/Gmsh.app/Contents/MacOS/gmsh"
else:
    Gmsh_str = "gmsh"
dolfin_convert_str = "dolfin-convert"

#parameters['allow_extrapolation'] = True
#set_log_active(False)
#set_log_level(PROGRESS)

OutputFolder = "./output/SAD_Navier_Reduced/" #where to store the results
RestartFolder = OutputFolder+"Restart/" #where to store restart data
MeshFolder = "./mesh/Channel2D/" #where to get the inital mesh and flow geometry from

MeshName = "Channel2_Symm" #Name of the mesh
RemeshLogic = "remesh_local" #Name of the temporary mesh needed for remeshing

VolumeConstraint = True
BarycenterConstraint = True

Mu = Constant(1.0/100.0)
Mu.rename("Mu", "")
Rho = Constant(1.0)
Rho.rename("Rho", "")
GenerateSurface = True

#Step lengths for the optimization and when to switch from StepLength1 to Steplength2
StepLength1 = Constant(2.5e-1) #for Re = 100
StepLength2 = Constant(2.5e-1) #for Re = 100
StepSwap = 5

#Remesh control
RemeshEvery = 5 #When to remesh
RemeshStop = 20 #When to stop remeshing
OptEvery = 1 #Update shape after every PDE solve

#Geometry aux variables
CurvSmooth = 1e-1 #Smoothing when computing curvature from normals

#Optimization Control Variables
ResStop = 1e-4 #Optimization stop criterion: Residual
MaxIter = 1000 #Optimization stop criterion: Maximum number of iterations



#Begin:
Shape1 = ShapeOpt()
#Load the initial mesh
#Load serial Dolfin .xml meshes
mesh = Mesh(MeshFolder + "/" + MeshName+".xml")
boundary_parts = MeshFunction("size_t", mesh, MeshFolder + "/" + MeshName+"_facet_region.xml")
#VolumeParts nessessary for remeshing
VolumeParts = MeshFunction("size_t", mesh, MeshFolder + "/" + MeshName + "_physical_region.xml")
#Load a h5 mesh for parallel i/o
#(mesh, boundary_parts, VolumeParts) = Shape1.LoadMeshH5("./output/SAD_Stokes/Restart_Stokes/Mesh_%05d.h5"%44, True, True)

#Markers to denote boundary conditions matching the given mesh
Inlet = [11,12,31,32]
Noslip = []
Outlet = [21,22]
Design = [41,42]
Symm = [71,72]
AllMarkers = Inlet + Noslip + Outlet + Design + Symm

Shape1.mesh = mesh
Shape1.boundary_parts = boundary_parts
MaxDim = Shape1.mesh.topology().dim()
if MaxDim == 2:
    MyZeroV = Constant((0.0,0.0))
    MyIn = Constant((1.0,0.0))
else:
    MyZeroV = Constant((0.0,0.0,0.0))
    MyIn = Constant((1.0,0.0,0.0))
#MyInflow = Expression("1.5*(x[1]+y0)*(x[1]-y0)/(-y0*y0)", y0=1.5)*MyIn
MyInflow = MyIn

#Real-valued function space for scalar geometry constraints
R0 = FiniteElement("R", Shape1.mesh.ufl_cell(), 0)
OneR = project(Constant(1.0), FunctionSpace(Shape1.mesh, R0))
OneR.rename("One", "")

#Declare Function Spaces
def InitSpaces(mesh):
    #Space for velocity and adjoints
    V2 = VectorElement("CG", mesh.ufl_cell(), 2)
    #Space for pressure
    S1 = FiniteElement("CG", mesh.ufl_cell(), 1)
    #Space for deformation field
    V1 = VectorElement("CG", mesh.ufl_cell(), 1)
    #Space for scalar constraints
    R0 = FiniteElement("R", mesh.ufl_cell(), 0)
    Elements1 = [V2, S1]
    StateSpace = FunctionSpace(mesh, MixedElement(Elements1))
    AdjointSpace = FunctionSpace(mesh, MixedElement(Elements1))
    Elements2 = [V1]
    if VolumeConstraint:
        Elements2.append(R0)
        if BarycenterConstraint:
            for k in range(MaxDim):
                Elements2.append(R0)
    if VolumeConstraint == False:
        OptSpace = FunctionSpace(mesh, V1)
    else:
        OptSpace = FunctionSpace(mesh, MixedElement(Elements2))
    q_prim = Function(StateSpace)
    q_prim.rename("q_prim", "")
    q_adj = Function(AdjointSpace)
    q_adj.rename("q_adj", "")
    q_opt = Function(OptSpace)
    q_opt.rename("q_opt", "")
    return (q_prim, q_adj, q_opt)

def SplitState(q_prim, q_adj, q_opt):
    (u0, p0) = split(q_prim)
    (lu0, lp0) = split(q_adj)
    if VolumeConstraint:
        if BarycenterConstraint:
            if MaxDim == 2:
                (V0, lVol, lBary0, lBary1) = split(q_opt)
                lBary = (lBary0, lBary1)
            if MaxDim == 3:
                (V0, lVol, lBary0, lBary1, lBary2) = split(q_opt)
                lBary = (lBary0, lBary1, lBary2)
        else:
            (V0, lVol) = split(q_opt)
            lBary = None
    else:
        V0 = q_opt
        lVol = None
        lBary = None
    return (u0, p0, V0, lu0, lp0, lVol, lBary)

def MyMeshDefo(V,W):
    MeshSmooth = Constant(0.1)
    MeshSmooth.rename("MeshSmooth", "")
    MyDefo = inner(V,W)*dx + MeshSmooth*inner(grad(V),grad(W))*dx + inner(V,W)*ds
    MyDefo += MeshSmooth*inner(grad(V), grad(W))*ds
    return MyDefo

def NavierStokesResidual(u0, p0, lu0, lp0):
    global Mu
    global Rho
    Mu_eff = Mu#*(pow((inner(grad(u0), grad(u0))), 3.0)+1e0)
    NSR = (Mu_eff*inner(grad(u0), grad(lu0)) + lp0*div(u0) - p0*div(lu0))*dx
    NSR += Rho*inner(lu0, dot(grad(u0),u0))*dx
    return NSR

#Define Lagrangian
def MakeLagrangian(q_prim, q_adj, q_opt):
    global Mu
    global Rho
    (u0, p0, V0, lu0, lp0, lVol, lBary) = SplitState(q_prim, q_adj, q_opt)
    
    print "Defining Lagrangian:"
    Mu_eff = Mu#*(pow((inner(grad(u0), grad(u0))), 3.0)+1e0)
    L_Objective = Mu_eff*inner(grad(u0), grad(u0))*dx
    L_Functional = NavierStokesResidual(u0, p0, lu0, lp0)
    
    #Scalar Constraints
    if lVol != None:
        L_Scalar = (lVol*OneR)*dx
        return (L_Objective, L_Functional, L_Scalar)
    #Barycenter-constraint cannot be postulated here, because the non-integral root node
    #would fail UFL and the S-AD differentiation
    #Add the derivative component of that directly to the KKT-RHS
    return (L_Objective, L_Functional)

def InitProblem(q_prim, q_adj, q_opt):
    PrimalSpace = q_prim.function_space()
    AdjointSpace = q_adj.function_space()
    if VolumeConstraint:
        (L_Objective, L_Functional, L_Scalar) = MakeLagrangian(q_prim, q_adj, q_opt)
    else:
        (L_Objective, L_Functional) = MakeLagrangian(q_prim, q_adj, q_opt)
    L = L_Objective + L_Functional
    if VolumeConstraint:
        L = L + L_Scalar
    #use dolfin standard differentiation
    State = derivative(L, q_adj, TestFunction(PrimalSpace))
    Adj = derivative(L, q_prim, TestFunction(AdjointSpace))
    #use Shape1 differentiation
    #State = ProcessState(L, q_adj, TestFunction(PrimalSpace))
    #Adj = ProcessState(L, q_prim, TestFunction(AdjointSpace))
    return (State, Adj)

def SolveState(state, q_prim):
    PrimalSpace = q_prim.function_space()
    MyIn = project(MyInflow, PrimalSpace.sub(0).collapse())
    PrimalBC = []
    for i in Noslip+Design:
        PrimalBC.append(DirichletBC(PrimalSpace.sub(0), MyZeroV, Shape1.boundary_parts, i))
    for i in Inlet:
        PrimalBC.append(DirichletBC(PrimalSpace.sub(0), MyIn, Shape1.boundary_parts, i))
    solve(state == 0, q_prim, bcs=PrimalBC)

def SolveAdjoint(NavierAdjoint, q_adj):
    AdjointSpace = q_adj.function_space()
    MyBC = []
    for k in Inlet+Noslip+Design:
        MyBC.append(DirichletBC(AdjointSpace.sub(0), MyZeroV, Shape1.boundary_parts, k))
    solve(NavierAdjoint == 0, q_adj, bcs=MyBC)

def SolveOptimality(StepLength, q_prim, q_adj, q_opt, InitVol = None, InitBary = None):
    global Vol_offset, BaryOff
    N = Shape1.VolumeNormal(Shape1.mesh)
    N.rename("n", "")
    kappa = Shape1.ComputeDivNVolume(Shape1.mesh, CurvSmooth)
    kappa.rename("Curvature", "")

    (u0, p0, V0, lu0, lp0, lVol, lBary) = SplitState(q_prim, q_adj, q_opt)
    OptSpace = q_opt.function_space()
    if VolumeConstraint:
        CurVol = assemble(OneR*dx)
        (L_Objective, L_Functional, L_Scalar) = MakeLagrangian(q_prim, q_adj, q_opt)
        L = L_Objective + L_Functional + L_Scalar
        if BarycenterConstraint:
            CurBary = Shape1.ComputeBarycenter(Shape1.mesh)
            if MaxDim == 2:
                (V, dlVol, dlBary0, dlBary1) = TestFunctions(OptSpace)
                (W, DlVol, DlBary0, DlBary1) = TrialFunctions(OptSpace)
                dlBary = (dlBary0, dlBary1)
                DlBary = (DlBary0, DlBary1)
            if MaxDim == 3:
                (V, dlVol, dlBary0, dlBary1, dlBary2) = TestFunctions(OptSpace)
                (W, DlVol, DlBary0, DlBary1, DlBary2) = TrialFunctions(OptSpace)
                dlBary = (dlBary0, dlBary1, dlBary2)
                DlBary = (DlBary0, DlBary1, DlBary2)
        else:
            (V, dlVol) = TestFunctions(OptSpace)
            (W, DlVol) = TrialFunctions(OptSpace)
    else:
        (L_Objective, L_Functional) = MakeLagrangian(q_prim, q_adj, q_opt)
        V = TestFunction(OptSpace)
        W = TrialFunction(OptSpace)
    
    dL_Objective = ShapeDerivative(L_Objective, Shape1.mesh, V, N, kappa=kappa, boundary_parts=Shape1.boundary_parts, Is_Normal=[V], Constant_In_Normal=[], SymmetryDirection=V, GenerateSurface=GenerateSurface)
    dL_Functional = ShapeDerivative(L_Functional, Shape1.mesh, V, N, kappa=kappa, boundary_parts=Shape1.boundary_parts, Is_Normal=[V], Constant_In_Normal=[], SymmetryDirection=V, GenerateSurface=GenerateSurface)
    dL = dL_Objective + dL_Functional

    KKT = MyMeshDefo(V, W)

    if VolumeConstraint:
        dL_Scalar = ShapeDerivative(L_Scalar, Shape1.mesh, V, N, kappa=kappa, boundary_parts=Shape1.boundary_parts, Is_Normal=[V], Constant_In_Normal=[], SymmetryDirection=V, State=[lVol], StateDirection=[dlVol], GenerateSurface=GenerateSurface)
        KKT_Scalar = ShapeDerivative(dL_Scalar, Shape1.mesh, W, N, kappa=kappa, boundary_parts=Shape1.boundary_parts, Is_Normal=[V,W], Constant_In_Normal=[], SymmetryDirection=V, State=[lVol], StateDirection=[DlVol], GenerateSurface=GenerateSurface)
        KKT += KKT_Scalar
        if BarycenterConstraint:
            SDB = Shape1.SD_Bary(Shape1.mesh, V, N)
            for k in range(MaxDim):
                dL_Scalar += lBary[k]*SDB[k]*ds
                KKT += DlBary[k]*SDB[k]*ds + adjoint(DlBary[k]*SDB[k]*ds)
        dL += dL_Scalar
    bc = []
    if VolumeConstraint:
        for i in Inlet+Noslip+Outlet:
            bc.append(DirichletBC(OptSpace.sub(0), MyZeroV, Shape1.boundary_parts, i))
    else:
        for i in Inlet+Noslip+Outlet:
            bc.append(DirichletBC(OptSpace, MyZeroV, Shape1.boundary_parts, i))

    #created reduced KKT system
    (A,b) = assemble_system(KKT, dL, bc)

    if VolumeConstraint:
        Vol_offset = CurVol - InitVol
        if BarycenterConstraint:
            VolID = len(b) - (1+MaxDim)
            b[VolID] = -Vol_offset
            BaryOff = MaxDim*[0.0]
            for k in range(MaxDim):
                BaryOff[k] = float(CurBary[k]) - float(InitBary[k])
                b[VolID + k + 1] = -BaryOff[k]
            print "BaryOff", BaryOff
        else:
            VolID = len(b) - 1
            b[VolID] = -Vol_offset

    # Compute L2-Norm of RHS / Optimality
    NormRHS = Function(OptSpace)
    NormRHS.vector()[:] = b
    NormRHS = norm(NormRHS, "L2")
    
    Grad = Function(OptSpace)
    Grad.rename("ReducedGradient", "")
    solve(A, Grad.vector(), -b)
    #update!
    q_opt.assign(project(q_opt + Grad, OptSpace))
    if VolumeConstraint:
        MyMove = project(-StepLength*Grad.split()[0], OptSpace.sub(0).collapse())
        MyMove.rename("Move", "")
        assign(q_opt.sub(0), project(MyZeroV, q_opt.function_space().sub(0).collapse()))
    else:
        MyMove = project(-StepLength*Grad, OptSpace)
        MyMove.rename("Move", "")
        assign(q_opt, project(MyZeroV, q_opt.function_space()))
    return (MyMove, NormRHS)

InitVol = assemble(OneR*dx)
print "Initial Volume:", InitVol
if BarycenterConstraint:
    InitBary = Shape1.ComputeBarycenter(Shape1.mesh)
    print "Initial Barycenter:", InitBary
else:
    InitBary = None

(q_prim, q_adj, q_opt) = InitSpaces(Shape1.mesh)
(NavierState, NavierAdjoint) = InitProblem(q_prim, q_adj, q_opt)
SolveState(NavierState, q_prim)
SolveAdjoint(NavierAdjoint, q_adj)
(MyMove, NormRHS) = SolveOptimality(StepLength1, q_prim, q_adj, q_opt, InitVol, InitBary)

#declare output files for plotting
FU = File(OutputFolder+"/Velocity.pvd", "compressed")
FP = File(OutputFolder+"/Pressure.pvd", "compressed")
FW = File(OutputFolder+"/Move.pvd", "compressed")
#FWB = File(OutputFolder+"/MoveBary.pvd", "compressed")
FLu = File(OutputFolder+"/Adj_Velo.pvd", "compressed")
FLp = File(OutputFolder+"/Adj_Pres.pvd", "compressed")
FKappa = File(OutputFolder+"/Kappa.pvd", "compressed")
FN = File(OutputFolder+"/Normal.pvd", "compressed")

#convergence history
if MPI.rank(mpi_comm_world()) == 0:
    fHistory = open(OutputFolder+"/History.txt", "w")
    if GenerateSurface == True:
        fHistory.write("Iter\tObjective\tResMove_Vol\tRHS_Surface")
    else:
        fHistory.write("Iter\tObjective\tResMove_Vol\tRHS_Volume")
    if VolumeConstraint:
        fHistory.write("\tResidualVol")
    if BarycenterConstraint:
        for k in range(MaxDim):
            fHistory.write("\tResidualBary%d"%k)
        fHistory.write("\n")

Res1 = 1e+10
i = 0
while Res1 > ResStop and i < MaxIter:
    if RestartFolder != None:
        StoreMesh(RestartFolder+"Mesh_%05d.h5"%i, Shape1.mesh, boundary_parts, VolumeParts)
    up = project(q_prim.sub(0), q_prim.function_space().sub(0).collapse())
    up.rename("u", "")
    FU << up
    pp = project(q_prim.sub(1), q_prim.function_space().sub(1).collapse())
    pp.rename("p", "")
    FP << pp
    lup = project(q_adj.sub(0), q_adj.function_space().sub(0).collapse())
    lup.rename("lu", "")
    FLu << lup
    lpp = project(q_adj.sub(1), q_adj.function_space().sub(1).collapse())
    lpp.rename("lpp", "")
    FLp << lpp
    i = i+1
    if i < StepSwap:
        StepLength = StepLength1
    else:
        StepLength = StepLength2

    if VolumeConstraint:
        (L_Objective, L_Functional, L_Scalar) = MakeLagrangian(q_prim, q_adj, q_opt)
    else:
        (L_Objective, L_Functional) = MakeLagrangian(q_prim, q_adj, q_opt)
    obj = assemble(L_Objective)
    Res1 = norm(MyMove, "L2")
    print "Objective:", obj, "Residual:", Res1
    if MPI.rank(mpi_comm_world()) == 0:
        fHistory.write("%4d\t%e\t%e\t%e"%(i, obj, Res1, NormRHS))
        if VolumeConstraint:
            fHistory.write("\t%e"%(Vol_offset))
            if BarycenterConstraint:
                for k in range(MaxDim):
                    fHistory.write("\t%e"%BaryOff[k])
        fHistory.write("\n")
        fHistory.flush()
    FW << MyMove
    ALE.move(Shape1.mesh, MyMove)
    SolveState(NavierState, q_prim)
    SolveAdjoint(NavierAdjoint, q_adj)
    (MyMove, NormRHS) = SolveOptimality(StepLength1, q_prim, q_adj, q_opt, InitVol, InitBary)
    #remesh
    if i%RemeshEvery == 0 and i < RemeshStop:
        print "Remeshing"
        OptSpace = q_opt.function_space()
        if VolumeConstraint:
            VolStore = float(project(q_opt.split()[1], OptSpace.sub(1).collapse()).vector()[0])
            if BarycenterConstraint:
                BaryStore = MaxDim*[0.0]
                for k in range(MaxDim):
                    BaryStore[k] = float(project(q_opt.split()[k+1], OptSpace.sub(k+1).collapse()).vector()[0])
        RemeshFolder = MeshFolder + "/remesh/"
        #write the mesh back out as a .msh file, which gmsh can read in again
        WriteMSH(Shape1.mesh, Shape1.boundary_parts, AllMarkers, MeshFolder + "/remesh/OldMesh.msh", VolumeParts)
        #external call to gmsh
        if MPI.rank(mpi_comm_world()) == 0:
            call([Gmsh_str, "-v", "0", "-2", RemeshFolder+"/"+RemeshLogic+".geo"])
            call([dolfin_convert_str, RemeshFolder+RemeshLogic+".msh", RemeshFolder+MeshName+".xml"])
        #Read the new mesh
        mesh = Mesh(RemeshFolder + MeshName+".xml")
        boundary_parts = MeshFunction("size_t", mesh, RemeshFolder + MeshName+"_facet_region.xml")
        VolumeParts = MeshFunction("size_t", mesh, RemeshFolder + MeshName + "_physical_region.xml")
        Shape1.mesh = mesh
        Shape1.boundary_parts = boundary_parts
        
        #re-declare Function Spaces...
        (q_prim, q_adj, q_opt) = InitSpaces(Shape1.mesh)
        OptSpace = q_opt.function_space()
        R0 = FiniteElement("R", Shape1.mesh.ufl_cell(), 0)
        OneR = project(Constant(1.0), FunctionSpace(Shape1.mesh, R0))
        OneR.rename("One", "")
        (NavierState, NavierAdjoint) = InitProblem(q_prim, q_adj, q_opt)
        VZero = project(MyZeroV, OptSpace.sub(0).collapse())
        if VolumeConstraint:
            tmpVol = Function(OptSpace.sub(1).collapse())
            tmpVol.vector()[0] = VolStore
            if BarycenterConstraint == True:
                tmpBary = MaxDim*[Function(OptSpace.sub(k+1).collapse())]
                for k in range(MaxDim):
                    tmpBary[k].vector()[0] = BaryStore[k]
                if MaxDim == 2:
                    assign(q_opt, [VZero, tmpVol, tmpBary[0], tmpBary[1]])
                else:
                    assign(q_opt, [VZero, tmpVol, tmpBary[0], tmpBary[1], tmpBary[2]])
            else:
                assign(q_opt, [VZero, tmpVol])

        SolveState(NavierState, q_prim)
        SolveAdjoint(NavierAdjoint, q_adj)
        (MyMove, NormRHS) = SolveOptimality(StepLength1, q_prim, q_adj, q_opt, InitVol, InitBary)
fHistory.close()
