from ufl.log import warning, error
from ufl.assertions import ufl_assert
from ufl.utils.dicts import subdict, mergedicts, EmptyDict

from ufl.core.expr import Expr
from ufl.core.terminal import Terminal
from ufl.core.operator import Operator
from ufl.core.ufl_type import ufl_type
from ufl.core.multiindex import Index, FixedIndex, MultiIndex

from ufl.exprcontainers import ExprList, ExprMapping
from ufl.constantvalue import Zero
from ufl.coefficient import Coefficient
from ufl.indexed import Indexed
from ufl.variable import Variable
from ufl.precedence import parstr
from ufl.domain import find_geometric_dimension


#connect with remaining ufl
from ufl.differentiation import CompoundDerivative
from ufl.operators import div, grad, inner, outer, dot
from ufl import checks
from ufl import constantvalue
"""

@ufl_type(num_ops=1, inherit_indices_from_operand=0, is_terminal_modifier=True)
class Div_Tan(CompoundDerivative):
    __slots__ = ()
    
    def __new__(cls, f):
        ufl_assert(not f.ufl_free_indices,"Free indices in the divergence argument is not allowed.")
        # Return zero if expression is trivially constants
        if is_cellwise_constant(f):
            return Zero(f.ufl_shape[:-1]) # No free indices asserted above
        return CompoundDerivative.__new__(cls)

    def __init__(self, f):
        CompoundDerivative.__init__(self, (f,))
    
    @property
    def ufl_shape(self):
        return self.ufl_operands[0].ufl_shape[:-1]
    
    def __str__(self):
        return "div_tan(%s)" % self.ufl_operands[0]
    
    def __repr__(self):
        return "Div_Tan(%r)" % self.ufl_operands[0]

"""

#simple definition:
def div_tan(v, n, kappa=None):
    if v == n:
        if kappa == None:
            print "Tangential Divergence of normal is curvature. Please specify"
            exit(1)
        else:
            return kappa
    return (div(v) - inner(dot(grad(v),n),n))

def grad_tan(f, n):
    #return grad(f) - inner(grad(f),n)*n
    return grad(f) - outer(dot(grad(f),n),n)

def jacobi_tan(f, n):
    return grad(f) - outer(dot(grad(f),n),n)

def laplace_tan(f, n, kappa):
    isconstant = checks.is_globally_constant(f)
    if not isconstant:
        return div(grad(f)) - kappa*inner(grad(f), n) - dot(n, dot(grad(grad(f)), n))
    return constantvalue.as_ufl(0.0)
#return (grad(f) - dot(n,inner(grad(f),n)))

__all__ = ['div_tan', 'grad_tan', 'jacobi_tan', 'laplace_tan']
